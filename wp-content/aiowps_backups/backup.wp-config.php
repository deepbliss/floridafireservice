<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'floridfs_newsite');

/** MySQL database username */
define('DB_USER', 'floridfs_newuser');

/** MySQL database password */
define('DB_PASSWORD', 'om{u[sclXpPT');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'WP_MEMORY_LIMIT', '256M' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gT&SZFqHaTA ua#It}FJ&edrq_>)T|(q,Q#6lskxV(2o?tr?X>YO=zuDA|sAk3HF');
define('SECURE_AUTH_KEY',  'qRF?47@+&b3a`;lq]Gw1Ia-DU[VjWsK4 V.Kps!z[kvg*}DBgKtn>++M!3;f@L-1');
define('LOGGED_IN_KEY',    ',Wp&UDT&NzW=J.i4*85P8.8N]@13fC4zNsbV4<eyydm+3?mL)/L1kbC%FdEvA!hK');
define('NONCE_KEY',        'iN:Qm4LY.0[+E+YL0;bB$NOFhWZE;<2^}ZRB8(UPRdD>A{#T?&8:65$jKJYj8)E$');
define('AUTH_SALT',        'pf&w_0Q<AmTRO0o{#y-S+_opV0y-g0J2BMCqjkL?P q,Fi{7DCoXH4x=-#oTh!b&');
define('SECURE_AUTH_SALT', 'jSCg5s!Gg&Q.,0aXpTx3/:DDwi1_qvkj72O)Q(JBc%`*r,UT0[AxxKr~E)=K6[gv');
define('LOGGED_IN_SALT',   'GW BXWW?H,S##;]ll0|{(DcA9-P|(<HIaXQ<3G:oXHbb]&:uiZwinWwxtE);du#8');
define('NONCE_SALT',       '}+N:^RjtZSWybR{`?{L%F?(zlA4mRA,8jL2ebVU}/xCE@W#Zuj*I$a~Ul%2oR<uA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
