<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * ManageMore Vantiv Payment Gateway
 *
 * Provides a ManageMore Payment Gateway through Vantiv
 *
 * @class 		WC_Gateway_ManageMore_Vantiv
 * @extends		WC_Payment_Gateway_CC
 * @version		2.17
 * @package		WooCommerce/Classes/Payment
 * @author 		Intellisoft Solutions, Inc.
 */
class WC_Gateway_ManageMore_Vantiv extends WC_Payment_Gateway_CC {

  /**
  * Constructor for the gateway.
  */
  public function __construct() {
		$this->id                 = 'managemore_vantiv';
		$this->icon               = null;
		$this->has_fields         = true;
 	  $this->supports = array( 'product',
		                         'tokenization' );
		$this->method_title       = __( 'ManageMore eConnect with Vantiv', 'woocommerce' );
		$this->method_description = __( '<img src="' . plugins_url( 'images/mmpill.png', __FILE__ ) . '" >&nbsp;&nbsp;&nbsp;<img src="' . plugins_url( 'images/vantiv.png', __FILE__ ) . '" ><br>Allows payments via ManageMore\'s unique IntelliCharge Payment Gateway and automatically transmits orders to ManageMore via eConnect.<br><br>
		                                 <a href="http://www.managemore.com/intellicharge/application.htm" target="_blank">Sign-Up For a FREE Merchant Account</a>
		                                 <a href="' . admin_url( 'admin.php?page=econnect-settings' ) . '">Plug-In Settings</a>', 'woocommerce' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();


      // Turn these settings into variables we can use
		foreach ( $this->settings as $setting_key => $value ) {
			$this->$setting_key = $value;
		}
		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );

    //Gateway URLs
    //Iframe URL
    $this->Mercury_HC_IFrame_URL="https://hc.mercurypay.com/CheckoutiFrame.aspx";
    $this->Mercury_HC_IFrame_Test_URL="https://hc.mercurycert.net/CheckoutiFrame.aspx";  
    // Hosted Checkout API WSDL URL
    $this->Mercury_HC_API_URL= "https://hc.mercurypay.com/hcws/HCService.asmx?WSDL";
    $this->Mercury_HC_Test_API_URL= "https://hc.mercurycert.net/hcws/HCService.asmx?WSDL"; 
    // Hosted Checkout API WSDL URL
    $this->Mercury_TWS_API_URL= "https://hc.mercurypay.com/tws/transactionservice.asmx?WSDL";
    $this->Mercury_TWS_Test_API_URL= "https://hc.mercurycert.net/tws/transactionservice.asmx?WSDL"; 


  	//Add Actions
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
    add_action( 'woocommerce_api_wc_managemore_vantiv', array( $this, 'check_vantiv_response' ) );
    add_action( 'woocommerce_receipt_managemore_vantiv', array($this, 'receipt_page'));

    
    
  }

  

  /**
  * Initialise Gateway Settings Form Fields
  */
  public function init_form_fields() {

    	$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable ManageMore eConnect with Vantiv', 'woocommerce' ),
				'default' => 'no'
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'Credit Card Payment', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'merchantid' => array(
				'title'       => __( 'Merchant Id', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'The Merchant Id assigned by Vantiv', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'password' => array(
				'title'       => __( 'Password', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'The Hosted Checkout password assigned by Vantiv', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'authtype' => array(
				'title'       => __( 'Authorization Type', 'woocommerce' ),
				'type'        => 'select',
				'description' => __( 'Type of authorization to perform', 'woocommerce' ),
				'default'     => 'Capture',
				'options' => array(
                              'preauth' => __( 'PreAuth', 'woocommerce' ),
                              'capture' => __( 'Capture', 'woocommerce' )
                               ),
				'desc_tip'    => true,
			),
/*
			'cardonfile' => array(
				'title'   => __( 'Allow Card on File', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Allow customer to save token of credit card on file for future checkout', 'woocommerce' ),
				'default' => 'no'
			),
*/			
        'autovoid' => array(
				'title'   => __( 'Auto Void on Failure', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Automatically cancel order and void credit card if there is an eConnect failure', 'woocommerce' ),
				'default' => 'yes'
			),
		);
  }

  /**
  * Process Payment function - call payment page
  */
  function process_payment( $order_id ) {
     global $woocommerce;
        
     $order = new WC_Order( $order_id );

     //Update Post Meta with Card on File Preference
     $save_token = ( isset( $_POST['wc-'.$this->id.'-payment-token'] ) && 'new' == $_POST['wc-'.$this->id.'-payment-token'] ) ? 'yes' : 'no' ;
     update_post_meta( $order_id, '_econnect_save_token', $save_token, true );
         
     // Redirect to payment page
     return array(
       'result'    => 'success',
       'redirect'  => $order->get_checkout_payment_url( true )
      );
  }

  /**
  * Check Vantiv server callback response
  **/
  function check_vantiv_response(){
   
   /**Note on response url:
   
      Since Vantiv's iframe posts its response by calling the response url within the iframe,
      this would cause a "page within a page".  So we have their system call a middle-man php script,
      which "breaks out" of the iframe and posts to the standard url for call back of our plugin
      (e.g. www.mywebsite.com/wc-api/WC_managemore_vantiv), or the value returned by:
      home_url( '/wc-api/WC_managemore_vantiv' )
      We re-pass the order id and posted values to our callback within the url (e.g. ?order_id=xxx&ResponseCode=xxx, etc.)
      
   **/

   global $woocommerce;

   //Get Order
   $order_id = $_GET['order_id'];  
   $order = new WC_Order( $order_id );

   //get values posted by Vantiv
   $cardid = $_GET['PaymentID'];  
   $return_code=$_GET['ReturnCode'];  
   $return_message=$_GET['ReturnMessage'];

     
   $success = false;
   $cc_success = false;

   if($return_code == 0 || $return_code == 101) { 
      //If success or decline
    
      //Per Mercury Development, we should call VerifyPayment even on a decline, to get additional info for user.
      //Although in our testing the message from VerifyPayment was the same as the ReturnMessage POSTed
      //if the card was declined.
   
      // Instantiate MercuryHCClient Object
      include_once("mercury_client.php");
      $wsdlURL=  ($this->merchantid == "778825001") ? $this->Mercury_HC_Test_API_URL : $this->Mercury_HC_API_URL ;
      $hc = new MercuryHCClient($wsdlURL);
   
      //Call Mercury VerifyPayment
      $verifyPaymentRequest = array(
               "MerchantID"   => $this->merchantid,
               "Password"  => $this->password,
               "PaymentID" => $cardid
               );
      
      $verifyPaymentResponse = $hc->sendVerifyPayment($verifyPaymentRequest);
      $response_code =  $verifyPaymentResponse->VerifyPaymentResult->ResponseCode;
      $response_status = $verifyPaymentResponse->VerifyPaymentResult->Status;
      if($response_code !=0 || $response_status != 'Approved'){
         $response_msg_to_customer='<b>Credit Card Error:</b> '. $verifyPaymentResponse->VerifyPaymentResult->DisplayMessage . " (" .$response_status .")";
      }
      else {
          //Check CVV2 Error
          $CvvResult=$verifyPaymentResponse->VerifyPaymentResult->CvvResult;
          if ($CvvResult != "M" && $CvvResult != ""){
             //AVS Result Returned, did not match.  Treat as error, do not process card.
             $response_msg_to_customer='<b>Credit Card Error:</b> Card Security Code Error ('. $CvvResult .")";
          }
          else {
             $cc_success=true;
             $vantiv_cc_response->auth_code=$verifyPaymentResponse->VerifyPaymentResult->AuthCode;
             $vantiv_cc_response->gateway_refid=$verifyPaymentResponse->VerifyPaymentResult->Token;
          }
      }
   }       
   else {
       $response_msg_to_customer='<b>Credit Card Error:</b> '. $return_message . " (" .$return_code . ")";
   }
   
   if ($cc_success == true) {
   
          //Set PreAuth
          if (get_option('econnect_transtype') =='invoice'){
             $this->trantype = 1 ; //invoice
             $this->preauth = 0 ; //cannot have preauth on invoice
          }
          else {
             $this->trantype = 7 ;
             if ($this->authtype=='preauth') {
                 $this->preauth='1';
             }
             else {
                 $this->preauth='0';
             }
          }

          //Set Card Type
          $CardType=$verifyPaymentResponse->VerifyPaymentResult->CardType;
          switch ($CardType) {
          case "AMEX":
            $this->card_desc='American Express';
            $this->card_type = 'amex';
            break;
          case "M/C":
            $this->card_desc='MasterCard';
            $this->card_type = 'mastercard';
            break;
          case "VISA":
            $this->card_desc='Visa';
            $this->card_type = 'visa';
            break;
          case "DCVR":
            $this->card_desc='Discover';
            $this->card_type = 'discover';
            break;
          default:
            $this->card_desc=$verifyPaymentResponse->VerifyPaymentResult->CardType;  //Other; use CardType
          }
          $this->card_expdate = str_replace( array( '/', ' '), '', $verifyPaymentResponse->VerifyPaymentResult->ExpDate );

       //Send Order to eConnect
       $this->_econnect_sendpacket($order,$verifyPaymentResponse);
       if ($this->eConnectError == true){

       		// _econnect_sendpacket would have added notice to the cart if error.
     
          //Set redirect back to payment page
          $redirect_url = $order->get_checkout_payment_url( $on_checkout = false );

       }
       else {

          //All OK, approved and sent to eConnect
          
         //Update Post Meta with Gateway Ref Id and other payment info (for REST-based importing into MM)
         update_post_meta( $order->id, '_econnect_card_desc', $this->card_desc );
         update_post_meta( $order->id, '_econnect_card_num', substr($verifyPaymentResponse->VerifyPaymentResult->MaskedAccount,-4) );
         update_post_meta( $order->id, '_econnect_auth_code', substr($this->auth_code,10)  );  //note: substring fixes rare cases (esp with test account) that auth_code is null
         update_post_meta( $order->id, '_econnect_auth_type', $this->preauth );
         update_post_meta( $order->id, '_econnect_gateway_refid', $verifyPaymentResponse->VerifyPaymentResult->RefNo . '&#124;' . $verifyPaymentResponse->VerifyPaymentResult->Invoice . '&#124;' . $verifyPaymentResponse->VerifyPaymentResult->Token );
         update_post_meta( $order->id, '_econnect_acq_ref_data', $verifyPaymentResponse->VerifyPaymentResult->AcqRefData );
         update_post_meta( $order->id, '_econnect_process_data', $verifyPaymentResponse->VerifyPaymentResult->ProcessData );
         update_post_meta( $order->id, '_econnect_card_expdate', substr($this->card_expdate,0,2) . '/' . substr($this->card_expdate,2,2) );

          //Save Account Number, other info on Customer Record
          $user = wp_get_current_user();
          $user_id = $user->ID ;
          if ($user_id) {
            $value = $this->econnect_response_array['CID'];
            update_user_meta( $user_id, '_econnect_field_CID' , $value);
            $value = $this->econnect_response_array['B'];
            update_user_meta( $user_id, '_econnect_field_B' , $value);
            $value = $this->econnect_response_array['LPD'];
            update_user_meta( $user_id, '_econnect_field_LPD' , $value);
            $value = $this->econnect_response_array['LAR'];
            update_user_meta( $user_id, '_econnect_field_LAR' , $value);
            $value = $this->econnect_response_array['LSIA'];
            update_user_meta( $user_id, '_econnect_field_LSIA' , $value);
            $value = $this->econnect_response_array['LSID'];
            update_user_meta( $user_id, '_econnect_field_LSID' , $value);
          }

 	        // Save Token, if necessary
 	        $save_token = get_post_meta( $order_id, '_econnect_save_token', true );
	       	if ($save_token=="yes") {
	       		
            $ccexp = str_replace( array( '/', ' '), '', $verifyPaymentResponse->VerifyPaymentResult->ExpDate );
	       		$ccmonth = substr($ccexp,0,2);
	       		$ccyear = '20'.substr($ccexp,2,2); // this will break in the 22nd century :(
            $token = new WC_Payment_Token_CC();
            $token_string = $verifyPaymentResponse->VerifyPaymentResult->Token;
            $token->set_token( $token_string );
            $token->set_gateway_id( $this->id ); // `$this->id` references the gateway ID set in `__construct`
            $token->set_card_type( $this->card_type );
            $token->set_last4( substr($verifyPaymentResponse->VerifyPaymentResult->MaskedAccount,-4) );
            $token->set_expiry_month( $ccmonth );
            $token->set_expiry_year( $ccyear );
            $token->set_user_id( get_current_user_id() );
            $token->save();

            //Save note to order about token being stored
            $tokeninfo = 'Token stored.';
    		    $order->add_order_note( $tokeninfo );


          }


		      // Mark order as Paid
		      $order->payment_complete();

		      // Empty the cart (Very important step)
		      $woocommerce->cart->empty_cart();

          // Set redirect to Thank you page
 	        $redirect_url = $order->get_checkout_order_received_url();



 	     }

   }
   else {


		  // Add notice to the cart
      wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
     
      //Set redirect back to payment page
      $redirect_url = $order->get_checkout_payment_url( $on_checkout = false );
  }
     
  wp_redirect( $redirect_url );  //perform the redirect
  
  exit;  //obligatory exit at end of callback
     
  }


  /**
  * Display payment form (no credit card fields).
  */
  function payment_fields(){

     $this->cardonfile='no';  //override card on file setting, until logic is finished

     if ( is_checkout() ) {
       	echo "Your card information will be entered on the next page. ";
        if ($this->cardonfile=='yes'){
 	         $this->tokenization_script();
           $this->saved_payment_methods();
           $this->save_payment_method_checkbox();
           echo '<br>';
           echo '<br>';
        }
     }
     else {
        if ($this->cardonfile=='no'){
        	echo "We're sorry, but you cannot save cards on file at this time.";
        }
        else {
      	  echo "We're sorry, but you cannot save new cards on file except during purchase.";
      	}
     }
  	 return;
  	
  }

  /**
   * Custom "Receipt Page" that displays the payment page with iframe
   */
  public function receipt_page($order_id){

    
    //Instantiate WC_Order object with current order
    $order = new WC_Order( $order_id );

	 //See if we should send a Check Stock Packet
	 if (get_option('econnect_checkstock')=='yes' || get_option('econnect_checkstock')=='all'){
	     $this->_econnect_checkstock($order);
        if ($this->eConnectError ==true){
           //Set redirect back to payment page
           $redirect_url = $order->get_checkout_payment_url( false );
           wp_redirect( $redirect_url );  //perform the redirect
           exit;
        }
	 }




    //Instantiate MercuryHCClient Object
    include_once("mercury_client.php");
    $wsdlURL=  ($this->merchantid = "778825001") ? $this->Mercury_HC_Test_API_URL : $this->Mercury_HC_API_URL ;
    $hc = new MercuryHCClient($wsdlURL);

    //Initialize Mercury Payment Request
    $initPaymentRequest = array(
            "MerchantID"   => $this->merchantid,
            "Password"  => $this->password,
            "TranType" => "Sale",
            "Invoice" => $order_id,
            "TotalAmount" => $order->get_total(),  
            "TaxAmount" => $order->get_total_tax(),  
            "OrderTotal" => "on",
            "DisplayStyle" => "Custom",
            "SubmitButtonText" => "Submit",
            "CancelButton" => "on",
            "FontSize" => "Medium",
            "PageTimeoutDuration" => "10",  //minutes (0=unlimited)
            "CardHolderName"  => $order->billing_first_name . ' ' . $order->billing_last_name,
            "Frequency" => "Recurring",
            "OperatorID" =>"test",
            "Memo"   => "ManageMore eConnect Version 9.0",
            "ProcessCompleteUrl" => plugins_url( '/process-response-vantiv.php?order_id=', __FILE__).$order_id,
            "ReturnUrl" => plugins_url( '/process-response-vantiv.php?order_id='.$order_id, __FILE__ )
            
            ); 
            
            /**Note on response url:
            
               Since Vantiv's iframe posts its response by calling the response url within the iframe,
               this would cause a "page within a page".  So we have their system call a middle-man php script,
               which "breaks out" of the iframe and posts to the standard url for call back of our plugin
               (e.g. www.mywebsite.com/wc-api/WC_managemore_vantiv), or the value returned by:
               home_url( '/wc-api/WC_managemore_vantiv' )
               We re-pass the order id and posted values to our callback within the url (e.g. ?order_id=xxx&ResponseCode=xxx, etc.)
               
            **/
    

    $initPaymentResponse = $hc->sendInitializePayment($initPaymentRequest);

    $iframe_url=  ($this->merchantid = "778825001") ? $this->Mercury_HC_IFrame_Test_URL : $this->Mercury_HC_IFrame_URL ;

    $response_code =  $initPaymentResponse->InitializePaymentResult->ResponseCode;
    if($response_code !=0){
       $init_error=$initPaymentResponse->InitializePaymentResult->Message;
       echo "We're sorry, but Credit Cards cannot be processed at this time.  Initialization Error: " .  $init_error;
    }
    else {
       $init_error='';
       $this->cardid = $initPaymentResponse->InitializePaymentResult->PaymentID;
       echo '<iframe src="'. $iframe_url .'?pid=' . $this->cardid.'" 
                        width="600px" height="450px" scrolling="auto" frameborder="0" style="text-align: center; display: none;" onload="this.style.display=\'block\';">
                        Your browser does not support iFrames. To view this content, please download and use the latest version of one of the following browsers: Internet Explorer, Firefox, Google Chrome or Safari.</iframe>';

    }


  }



  /**
   * Send Credit Card communication request
   *
   * Note: For this gateway, this is only used for void
   */
  public function _sendCCRequest($submit_type,$order,$verifyPaymentResponse) {

    //Instantiate MercuryTransClient Object
    include_once("mercury_client.php");
    $wsdlURL=  ($this->merchantid == "778825001") ? $this->Mercury_TWS_Test_API_URL : $this->Mercury_TWS_API_URL ;
    $tc = new MercuryTransClient($wsdlURL);


    if($submit_type == 'Void'){
        //Build array to send for voiding
       $submit_data = array(
                      'MerchantID' => $this->merchantid,
                      'Invoice' => $verifyPaymentResponse->VerifyPaymentResult->Invoice,
                      'Token' => $verifyPaymentResponse->VerifyPaymentResult->Token,
                      'Frequency' => "Recurring",
                      'AuthCode' => $verifyPaymentResponse->VerifyPaymentResult->AuthCode,
                      'PurchaseAmount' => $verifyPaymentResponse->VerifyPaymentResult->Amount,
                      'RefNo' => $verifyPaymentResponse->VerifyPaymentResult->RefNo,
                      'AcqRefData' => $verifyPaymentResponse->VerifyPaymentResult->AcqRefData,
                      'ProcessData' => $verifyPaymentResponse->VerifyPaymentResult->ProcessData,
                      'Memo'   => "ManageMore eConnect Version 9.0");
    }
    else {
       //Build array to send for Sale, PreAuth (N/A)
       $submit_data=array();
    }
    $PaymentResponse = $tc->CreditReversalToken($submit_data,$this->password);
    return $PaymentResponse;
  }



  /**
   * Send transaction packet to eConnect Server.
   */

  function _econnect_sendpacket($order,$verifyPaymentResponse) {


      global $woocommerce,$econnect_order_packet_array;
     

      $econnect_disable = esc_attr( get_option('econnect_disable') );
      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );
      $connect_timeout = get_option('econnect_connect_timeout');
      $packet_timeout = get_option('econnect_packet_timeout');
      //make sure timeouts not less than minimum
      if ($connect_timeout < 5) $connect_timeout = 5;
      if ($packet_timeout < 10) $packet_timeout = 10;

      //Check if ManageMore processing should be skipped via
      //econnect_disable flag
      if ($econnect_disable=='1') {
         $note_text = "eConnect Processing Disabled.  Order not transmitted.";
         $order->add_order_note( $note_text);
         return;
      }

      if (esc_attr( get_option('econnect_ssl') ) == '1') {
        $socket = @fsockopen("ssl://".$host, $port, $errno, $errstr, $connect_timeout);
      }
      else {
        $socket = @fsockopen($host, $port, $errno, $errstr, $connect_timeout);
      }
      
      if (!$socket) {
         //Void Transaction on Gateway
         if ($this->autovoid=='yes'){
            $submit_type='Void';
            $void_response = $this->_sendCCRequest($submit_type,$order,$verifyPaymentResponse);
   
            //Check if Void was approved, add note to Order
            if ($void_response->Status == 'Approved'){
               $void_text .= ' Credit Card payment was successfully voided.';
            } else {
               $void_text .= ' WARNING: Credit Card could not be voided.';
            }

            //Mark Order as Failed
            $error_text = 'Error: eConnect Server not Responding. '.$void_text;
            $order->update_status('failed', __($error_text , 'woothemes'));

   			    // Add notice to the cart
            $response_msg_to_customer = 'There was a Server Communication Error - Please Try Again Later ';
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
         }
         else {
   	      // Autovoid off: Just add note of error to the order for reference
            $error_text = 'eConnect Server not Responding.';
	          $order->add_order_note( 'eConnect Error: '. $error_text );
            
         }
         return ;

      
      } else {

          //Socket connection established with eConnect Server.
          //Create Packet using eConnect Pipe-Delimited protocol and send
          
          $t=time();
          $oID = str_replace( "#", "", $order->get_order_number() );

/*          //Set PreAuth
          if (get_option('econnect_transtype') =='invoice'){
             $trantype = 1 ; //invoice
             $authtype = 0 ; //cannot have preauth on invoice
          }
          else {
             $trantype = 7 ;
             if ($this->authtype=='preauth') {
                 $authtype='1';
             }
             else {
                 $authtype='0';
             }
          }

*/
/*          //Set Card Type
          $CardType=$verifyPaymentResponse->VerifyPaymentResult->CardType;
          switch ($CardType) {
          case "AMEX":
            $paymentdesc='American Express';
            $this->card_type = 'amex';
            break;
          case "M/C":
            $paymentdesc='MasterCard';
            $this->card_type = 'mastercard';
            break;
          case "VISA":
            $paymentdesc='Visa';
            $this->card_type = 'visa';
            break;
          case "DCVR":
            $paymentdesc='Discover';
            $this->card_type = 'discover';
            break;
          default:
            $paymentdesc=$verifyPaymentResponse->VerifyPaymentResult->CardType;  //Other; use CardType
          }
          $expdate = str_replace( array( '/', ' '), '', $verifyPaymentResponse->VerifyPaymentResult->ExpDate );
*/


          //Create Order Packet Array ---------------------------------------------------------------------------

          //Check Account Number on Customer Record, Set Lookup Id, Lookup Type
          $user = wp_get_current_user();
          $user_id = $user->ID ;
          if ($user_id) {
            $custid= get_user_meta( $user_id, '_econnect_custid' , true);
          }
          if ($custid!=''){
          	 $lid = $custid;
          	 $ltp = "1";
          }
          else {
          	 $lid = substr($order->billing_email, 0, 80);
          	 $ltp = "2";
          }

          //check if user id should be passed to eConnect to auto-register the user
          $user_info = get_userdata($user_id);
	        $user_login = $user_info->user_login;
          if (get_option('econnect_auto_register') =='1' && get_option('econnect_crm_enabled') == '1'){
          	 $auto_create_login = $user_login;
          }

          $econnect_order_packet_array = array(
             'WID' => $t ,
             'LID' => $lid,
             'LTP' => $ltp,
             'UTP' => '3',
             'TTP' => $this->trantype,
             'LANG' => '',
             'ALID' => $auto_create_login,
             'OID' => $oID,
             'CN' => substr($order->billing_company, 0, 40) ,
             'FN' => substr($order->billing_first_name, 0, 40) ,
             'LN' => substr($order->billing_last_name, 0, 40) ,
             'BA1' => substr($order->billing_address_1, 0, 40) ,
             'BA2' => substr($order->billing_address_2, 0, 40) ,
             'BC' => substr($order->billing_city, 0, 25) ,
             'BS' => substr($order->billing_state,0,3) ,
             'BZ' => substr($order->billing_postcode, 0, 13) ,
             'BCN' => $order->billing_country ,
             'P1' => substr($order->billing_phone, 0, 25) ,
             'EM' => substr($order->billing_email, 0, 80) ,
             'AST' => '1' ,
             'SCT' => substr($order->shipping_first_name, 0, 20) . ' ' . substr($order->shipping_last_name, 0, 20) ,
             'SCM' => substr($order->shipping_company, 0, 50) ,
             'SA1' => substr($order->shipping_address_1, 0, 40) ,
             'SA2' => substr($order->shipping_address_2, 0, 40) ,
             'SC' => substr($order->shipping_city, 0, 25) ,
             'SS' => substr($order->shipping_state,0,3) ,
             'SZ' => substr($order->shipping_postcode, 0, 13) ,
             'SCN' => $order->shipping_country ,
             'SP' => substr($order->billing_phone, 0, 25) ,
             'LOC' => (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' ,          
             'CCT' => $this->card_desc ,
             'CCN' => substr($verifyPaymentResponse->VerifyPaymentResult->MaskedAccount,-4) ,
             'CCX' => substr($this->card_expdate,0,2) . '/' . substr($this->card_expdate,2,2) ,
             'AUTH' => $this->preauth ,
             'AC' => $this->auth_code ,
             'GR' =>  $verifyPaymentResponse->VerifyPaymentResult->RefNo . '&#124;' . $verifyPaymentResponse->VerifyPaymentResult->Invoice . '&#124;' . $verifyPaymentResponse->VerifyPaymentResult->Token ,
             'GARD' => $verifyPaymentResponse->VerifyPaymentResult->AcqRefData ,
             'GPD' => $verifyPaymentResponse->VerifyPaymentResult->ProcessData
          );

          //Add Order Item Details -----------------------------------------------------------
          $this->_econnect_orderdetails($order,$econnect_order_packet_array,$item_total);


          //Final Order Info -----------------------------------------------------------------
          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];
          $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                'FRT' => $order->get_total_shipping() ,
                'DISC' => $order->get_total_discount(true) ,
                'SVIA' => $order->get_shipping_method( ) , 
                'TXC' => '' ,
                'TAX1' => $order->get_total_tax() ,
                'NOTE' => $order->customer_note ,
                'PPW' => esc_attr( get_option('econnect_password') ) ,
                'BUA' => $user_agent,
                'RIP' => $ip_address,
                'D' => date("m/d/y",$t) ,
                'T' => date("h:iA",$t)
                ));

          /*
          Add custom hook "econnect_before_sending_order"
          Use this hook to do something custom to the econnect_order_packet_array that will be used
          to create the packet to send to eConnect. (e.g, update Tax Code, etc.)
          */
          //add hook
          do_action('econnect_before_sending_order',$order);


          //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
          $in = '';
          foreach ($econnect_order_packet_array as $array_key => $array_value ) {
           	 $in .= '|'. $array_key . '=' . $array_value ;
          } 
          $in = '1007' . $in . chr(4);  //add packet type, end with chr(4)

                   
          fwrite($socket, $in);
          stream_set_timeout($socket, $packet_timeout);
            
      
          // Get Response
          $response = "";
          $i=0;
          do
          { 
            $out = fgets($socket,2);
            $info = stream_get_meta_data($socket);
            $response .= $out;
            $i++;
          } 
          while($out != chr(4) && $i<1000000 && $info['timed_out']==false);

          fclose($socket);

          if ($response != "") {

            if (strpos($response,"|S=1") === false ) {
               //Success Flag not returned
               //Get Error Message returned
               $errpos = strpos($response,"|E=");
               $errend = strpos($response,"|",$errpos+1);
               $errmsg = substr($response,$errpos+3,$errend-$errpos-3);

               //Void CC Transaction
               if ($this->autovoid=='yes'){
                  $submit_type='Void';
                  $void_response = $this->_sendCCRequest($submit_type,$order,$verifyPaymentResponse);

                  //Check if Void was approved, add note to Order
                  if ($void_response->Status == 'Approved'){
                     $void_text .= ' Credit Card payment was successfully voided.';
                  } else {
                     $void_text .= ' WARNING: Credit Card could not be voided.';
                  }
   
                  //Mark Order as Failed
                  $error_text = 'Error: '.$errmsg.'. ' . $void_text;
                  $order->update_status('failed', __($error_text , 'woothemes'));
   
         			// Add notice to the cart
                  $response_msg_to_customer = 'There was a Server Error - Please Try Again Later' ;
                  wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
                  $this->eConnectError=true;
               }
               else {
         	      // Autovoid off: Just add note of error to the order for reference
                  $error_text = $errmsg.'.';
      	         $order->add_order_note( 'eConnect Error: '. $error_text );
               }
               return ;
               
               
            }
            else {
               //Parse Response
               $econnect_response_packet = $response;
               //a) Explode response by pipe delimiter
               $econnect_temp_array = explode("|",$econnect_response_packet);
               //b) For each item, add to packet array with Key, Value
               foreach ($econnect_temp_array as $econnect_value_pair) {
                  $econnect_value_pair_array = explode("=",$econnect_value_pair,2);
                  $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
               }

			         // Add note to the order for reference
			         $success_text = 'Order successfully transmitted to ManageMore. Transction Id = '.$econnect_packet_array['TID'];
			         $order->add_order_note($success_text );

			         //Set CustId
			         $this->custid = $econnect_packet_array['CID'];

               return; //Success
               
            }
            
          }
          else {
            //Invalid response
            //Void CC Transaction
            if ($this->autovoid=='yes'){
               $submit_type='Void';
               $void_response = $this->_sendCCRequest($submit_type,$order,$verifyPaymentResponse);
   
               //Check if Void was approved, add note to Order
               if ($void_response->Status == 'Approved'){
                  $void_text .= ' Credit Card payment was successfully voided.';
               } else {
                  $void_text .= ' WARNING: Credit Card could not be voided.';
               }

               //Mark Order as Failed
               $error_text = 'Error: Invalid Server Response / Timeout. '. $void_text;
               $order->update_status('failed', __($error_text , 'woothemes'));
  
      			   // Add notice to the cart
               $response_msg_to_customer = 'There was a Server Error Invalid Response / Timeout - Please Try Again Later' ;
               wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
               $this->eConnectError=true;
            }
            else {
      	      // Autovoid off: Just add note of error to the order for reference
               $error_text = 'Invalid Server Response / Timeout.';
	            $order->add_order_note(  'eConnect Error: '. $error_text );
            }
            return ;
          }
      }
  }
	


  /**
   * Send Quantity Check packet to eConnect Server.
   */
  function _econnect_checkstock($order) {

      global $woocommerce;
     

      $econnect_disable = esc_attr( get_option('econnect_disable') );
      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );

      //Check if ManageMore processing should be skipped via
      //econnect_disable flag
      if ($econnect_disable=='1') {
         return;
      }

      if (esc_attr( get_option('econnect_ssls') ) == '1') {
        $socket = @fsockopen("ssl://".$host, $port, $errno, $errstr, 10);
      }
      else {
        $socket = @fsockopen($host, $port, $errno, $errstr, 10);
      }
      
      if (!$socket) {
            //Mark Order as Failed
            $error_text = 'Error: eConnect Server not Responding.';
            $order->update_status('failed', __($error_text , 'woothemes'));

   			   // Add notice to the cart
            $response_msg_to_customer = 'There was a Server Communication Error - Please Try Again Later ';
            wc_add_notice( __('We are sorry, but we could not verify stock status.  ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
      }
      else {

          //Socket connection established with eConnect Server.
          //Create Packet using eConnect Pipe-Delimited protocol and send
          
          $t=time();
          $oID = str_replace( "#", "", $order->get_order_number() );
          
          
          //Begin Packet -----------------------------------------------------------------------------
          $alllocations = (get_option('econnect_checkstock')=='all' ) ? '1' : '0' ;
          $econnect_order_packet_array = Array(
                    'WID' => $t ,
                    'ALOC' => $alllocations,
                    'LOC' => (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' 
                    );

                            
          //Order Item Details ------------------------------------------------------------------------
          $this->_econnect_orderdetails($order,$econnect_order_packet_array,$item_total);

          //Finish Packet -----------------------------------------------------------------------------
          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];
          $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                    'PPW' => esc_attr( get_option('econnect_password') ) ,
                    'BUA' => $user_agent,
                    'RIP' => $ip_address,
                    'D' => date("m/d/y",$t) ,
                    'T' => date("h:iA",$t).chr(4)
                    ));


          //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
          $in = '';
          foreach ($econnect_order_packet_array as $array_key => $array_value ) {
           	 $in .= '|'. $array_key . '=' . $array_value ;
          } 
          $in = '1021' . $in . chr(4);  //add packet type, end with chr(4)


          fwrite($socket, $in);
            
      
          // Get Response
          $response = "";
          $i=0;
          do
          { 
            $out = fgets($socket,2);
            $response .= $out;
            $i++;
          } 
          while($out != chr(4) && $i<10000);

          fclose($socket);

          if ($response != "") {

            if (strpos($response,"|S=1") === false ) {
               //Success Flag not returned
               //Get Error Message returned
               $errpos = strpos($response,"|E=");
               $errend = strpos($response,"|",$errpos+1);
               $errmsg = substr($response,$errpos+3,$errend-$errpos-3);

               //Note: Error responses are expected on this packet, since error means out of stock, so we do not notify the Admin
               $error_text = 'We are sorry, but one or more items are not available this time: '.$errmsg;
   
         		   // Add notice to the cart
               wc_add_notice( __('', 'woothemes') . $error_text, 'error' );
               $this->eConnectError=true;
               return ;
               
               
            }
            else {
               return; //Success
            }
            
          }
          else {
            //Invalid response

            //Mark Order as Failed
            $error_text = 'Error: Invalid Server Response.';
            $order->update_status('failed', __($error_text , 'woothemes'));
  
   			    // Add notice to the cart
            $response_msg_to_customer = 'There was a Server Error Invalid Response - Please Try Again Later' ;
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
            return ;
          }

      }
      

  }
  
  /**
   * Build Order Details Part of Packet.
   */
  /**
   * Build Order Details Part of Packet.
   */
  function _econnect_orderdetails($order,&$econnect_order_packet_array,$item_total) {
         
      global $woocommerce;

      $item_total=0;
      $i=0;
      foreach( $order->get_items() as $item ){
             if ($i < 9) {
               $Zeros="00";
             }
             elseif ($i < 99) { 
               $Zeros="0";
             }
             else{
               $Zeros="";
             }
             $num=$i+1;
             $item_total += $order->get_item_total($item,false,true);
             // Check if product has variation.
             $product_variation_id = $item['variation_id'];
             if ($product_variation_id) { 
                $_product = new WC_Product($item['product_id']);
                $_product_variation = new WC_Product_Variation($item['variation_id']);
                $_parent_product = new WC_Product($item['product_id']);
                $skucode = $_product_variation->get_sku();
                if($skucode==''){
                  $skucode = $_parent_product->get_sku();
                }
             } else {
                $_product = new WC_Product($item['product_id']);
                $skucode = $_product->get_sku();
             }
             

             $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                  "SKU".$Zeros.$num  =>  substr($skucode, 0, 16) ,
                  "SKUQ".$Zeros.$num =>  $item['quantity'] ,
                  "SKUA".$Zeros.$num  =>   $item['line_subtotal']
                ));
                
             //If Variation, send attribute types/values, and add their names to description   
             if($product_variation_id) { 
                $variation_data = $_product_variation->get_variation_attributes();
                $attributes     = $_product_variation->parent->get_attributes();
                $description    = array();
                $anum=0;
                foreach($attributes as $attribute){
                    // Only deal with attributes that are variations 
                    if ( ! $attribute[ 'is_variation' ]  ) {
                       continue;
                    }
                    
                    $variation_selected_value = isset( $variation_data[ 'attribute_' . sanitize_title( $attribute[ 'name' ] ) ] ) ? $variation_data[ 'attribute_' . sanitize_title( $attribute[ 'name' ] ) ] : '';
                    $description_name         = esc_html( wc_attribute_label( $attribute[ 'name' ] ) );
                    $description_value        = __( 'Any', 'woocommerce' );

                    $anum++;
                    if ($anum>3){
                      break;
                    }

                    // Get terms for attribute taxonomy or value if its a custom attribute
                    if ( $attribute[ 'is_taxonomy' ] ) {
    
                        $post_terms = wp_get_post_terms( $_product->id, $attribute[ 'name' ] );
    
                        foreach ( $post_terms as $term ) {
                            if ( $variation_selected_value === $term->slug ) {
                                $description_value = esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) );
                            }
                        }
    
                    } else {
    
                        $options = wc_get_text_attributes( $attribute[ 'value' ] );
    
                        foreach ( $options as $option ) {
    
                            if ( sanitize_title( $variation_selected_value ) === $variation_selected_value ) {
                                if ( $variation_selected_value !== sanitize_title( $option ) ) {
                                    continue;
                                }
                            } else {
                                if ( $variation_selected_value !== $option ) {
                                    continue;
                                }
                            }
    
                            $description_value = esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) );
                        }
                    }

                    $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                        "SKAT".$anum.$Zeros.$num =>  $description_name ,
                        "SKAV".$anum.$Zeros.$num =>  $description_value 
                       ));
                    
                    $description[] = rawurldecode( $description_value );
                    
                }
                $variation_detail = ' ('. implode(', ',$description).')';
             }
             else {
                $variation_detail='';
             }

             $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                "SKUD".$Zeros.$num =>  $item['name'].$variation_detail ));

             $i++;
             
      }
      return ;

  }

   	
}
