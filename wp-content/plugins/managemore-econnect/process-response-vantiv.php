<?php

   /**Note on response url:
   
      Since Vantiv's iframe posts its response by calling the response url within the iframe,
      this would cause a "page within a page".  So we have their system call this middle-man php script,
      which "breaks out" of the iframe and posts to the standard url for call back of our plugin
      (e.g. www.mywebsite.com/wc-api/WC_managemore_vantiv), or the value returned by:
      home_url( '/wc-api/WC_managemore_vantiv' )
      We re-pass the order id and posted values to our callback within the url (e.g. ?order_id=xxx&ResponseCode=xxx, etc.)
      
   **/

   //Get Order
   $order_id = $_GET['order_id'];  

   //get values posted by Vantiv
   $cardid = $_POST['PaymentID'];  
   $return_code=$_POST['ReturnCode'];  
   $return_message=$_POST['ReturnMessage'];

   //Set redirect value
   $redirect_url = '/wc-api/WC_managemore_vantiv?order_id=' .$order_id.'&PaymentID='.$cardid.'&ReturnCode='.$return_code.'&ReturnMessage='.$return_message;
;
   

?>
<html>
 <body onload="newpage()">
</body>
<script>
   function newpage() {
      window.top.location.href = "<?php echo $redirect_url; ?>"
   }
</script>

</html>
