=== ManageMore eConnect ===
Contributors:
Tags: crm, erp, connection, woocommerce, managemore
Requires at least: 4.0.1
Tested up to: 4.9.4
Requires PHP: 5.6
Stable tag: trunk
License: GPLv2 or later License
URI: http://www.gnu.org/licenses/gpl-3.0.html

Connects WooCommerce with ManageMore Business Software



