<?php
/*
Plugin Name: ManageMore eConnect
Plugin URI:  http://www.managemore.com/eConnect
Description: Connects WooCommerce with ManageMore Business Software
Version:     2.17
WC requires at least: 2.2
WC tested up to: 3.3
Author:      Intellisoft Solutions, Inc.
Author URI:  http://www.managemore.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: managemore-econnect
Domain Path: /languages
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


// Hooks and Filters------------------------------------------------------------------------------------


// Add Actions to initialize plugin, create menu, etc.
// Register settings
add_action( 'admin_init', 'managemore_econnect_plugin_settings' );
// Create Menu
add_action( 'admin_menu', 'managemore_econnect_plugin_menu' );
// Add action links
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'managemore_econnect_action_links' );
// Add meta links
add_filter( 'plugin_row_meta', 'managemore_econnect_plugin_meta_links', 10, 2 );
// Include Gateway Classes and register Payment Gateways with WooCommerce
add_action( 'plugins_loaded', 'managemore_gateway_init', 0 );


//Add actions for ManageMore Account Number Field in User area
//add actions to save account number
add_action('edit_user_profile_update', 'econnect_update_extra_profile_fields');  //if admin editing user
add_action('personal_options_update', 'econnect_update_extra_profile_fields');   //if current user
//function to show update fields
function econnect_update_extra_profile_fields($user_id) {

      if ( current_user_can('edit_user',$user_id) )
         update_user_meta($user_id, '_econnect_field_CID', $_POST['econnect_custid_data']);
}

//add actions to show account number 
add_action('edit_user_profile', 'econnect_custom_user_profile_fields');           //if current user (just returns)
add_action('show_user_profile', 'econnect_custom_user_profile_fields');           //if admin editing user
//function to show account number
function econnect_custom_user_profile_fields($user) {
	 if (is_admin() == false){
	 	return;
	 }
?>
<table class="form-table">
<tr>
	<th>
		<label for="econnect_custid_data">ManageMore Account No.</label>
	</th>
	<td>
		<input type="text" name="econnect_custid_data" id="econnect_custid_data" value="<?php echo esc_attr( get_user_meta( $user->ID, '_econnect_field_CID', true ) ); ?>" class="regular-text" />
		<br><span class="description">The account number in ManageMore for this customer</span>
	</td>
</tr>

</table>
<?php
}

// When website is loaded, check user
add_action( 'wp_loaded', 'econnect_user_init' );
function  econnect_user_init(){

	  $user = wp_get_current_user();
	  $user_id = $user->ID;
	  if ( $user_id != 0 ){
	  	
	  	$econnect_lastcheck = get_user_meta( $user_id, '_econnect_lastcheck', true); 
	  	$t = time();
	  	if (($t - $econnect_lastcheck) > 86400){
	  		//more than 24 hours since last check
     	  $user_info = get_userdata($user_id);
	      $user_login = $user_info->user_login;
        econnect_user_login( $user_login, $user );
  	  	update_user_meta( $user_id, '_econnect_lastcheck', $t); //update last check
	  	}
	  }
}

//Check MM Login when user logs in to WP
add_action('wp_login', 'econnect_user_login', 10, 2);
//Login Function (used for above hook and for login in econnect_user_init)
function econnect_user_login( $user_login, $user ) {

  global $econnect_request_packet, $econnect_response_packet, $econnect_error;

  $user_id = $user->ID;
  $custid = get_user_meta( $user_id, '_econnect_field_CID', true); 

  //Only perform MM Login if CRM or Customer Pricing is used, and user has email or Account Number
  if ($user_id !=0 && ($user->user_email != '' || $custid != '') &&
     (get_option('econnect_crm_enabled') == '1' || get_option('econnect_customer_pricing') == '1')) { 
    

      $user_info = get_userdata($user_id);
	    $user_login = $user_info->user_login;
      $geo = new WC_Geolocation();
      $ip_address = $geo->get_ip_address();
      $user_agent = $_SERVER['HTTP_USER_AGENT'];
	 
      if (get_option('econnect_crm_enabled') == '1'  && $custid != '' ) {

         $login_type = '10' ;

         // Special Shopping Cart Login (Packet 1028)
         $econnect_request_packet = "1028|WID=" .  $user_id . 
              "|LID=" . $user_login .
              "|LTP=" . $login_type .
              "|CID=" . $custid .
              "|PPW=" . esc_attr( get_option('econnect_password') ) .
              "|BUA=" . $user_agent .
              "|RIP=" . $ip_address .
              "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);
      }
      else {
         $login_id = ( $custid != '') ? $custid : $user->user_email;
         $login_type = ( $custid != '') ? '1' : '2' ;

         // Get Price Level for User (Packet 1027)
         $econnect_request_packet = "1027|WID=" .  $user_id . 
              "|LID=" . $login_id .
              "|LTP=" . $login_type .
              "|PPW=" . esc_attr( get_option('econnect_password') ) .
              "|BUA=" . $user_agent .
              "|RIP=" . $ip_address .
              "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);
      	
      }

      econnect_sendpacket();

      if ($econnect_error === "") {

          //parse packet and save response
          //Parse Response
          //a) Explode response by pipe delimiter
          $econnect_temp_array = explode("|",$econnect_response_packet);
          //b) For each item, add to packet array with Key, Value
          $econnect_packet_array=array();
          foreach ($econnect_temp_array as $econnect_value_pair) {
             $econnect_value_pair_array = explode("=",$econnect_value_pair);
             $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
          }   
         
         
          if (get_option('econnect_crm_enabled') == '1'  && $custid != '' ) {

             //crm used and this user has a custid.  save CRM values for user
             
             //For all fields, write to custom Meta Values
             foreach ($econnect_packet_array as $field => $value) {
                update_user_meta( $user_id, '_econnect_field_'.$field,$value); 
              	 
             } 
             //Write Current Cust Id field to indicate successful CRM login
             update_user_meta( $user_id, '_econnect_field_current_CID',$custid); 
             

          	
          }
          else {
          	
          	 //crm not used or user has no pre-existing account number.  save only price level, acct number
          	
             $price_level = $econnect_packet_array['PRL'];
             if ($price_level != '')
               update_user_meta( $user_id, '_econnect_field_PRL',$price_level); 
               
            
             if ($custid === '') {
                $custid = $econnect_packet_array['CID'];
                if ($custid != '')
                   update_user_meta( $user_id, '_econnect_field_CID' , $custid); 
             }
          
          	
          }
        

      }
      elseif (get_option('econnect_crm_enabled') == '1') {
      	 
         //Write blank value in Current Cust Id field to indicate UNsuccessful login
         update_user_meta( $user_id, '_econnect_field_current_CID',''); 

      	 //removed, because this would send a message for invalid login.
         //econnect_send_admin_email($econnect_error,'Customer Login');
      }
  }  

}

//Optional Hooks ------------------------------------------------------------------------------------
//These are added, depending upon plug-in settings
//If Checking stock before adding/updating cart, add hooks
if (get_option('econnect_checkstock_cart') == 'yes' || get_option('econnect_checkstock_cart') == 'all') {

   function econnect_validate_add_cart_item( $passed, $product_id, $quantity, $variation_id = '', $variations= '' ) {

       global $woocommerce, $post;



       // do  validation, if not met switch $passed to false
       if ($variation_id != '') {
          $_product = new WC_Product($variation_id);
          $skucode = $_product->get_sku();    
          $description = $_product->get_title();
       }
       else {
          $_product = new WC_Product($product_id);
          $skucode = $_product->get_sku(); 
          $description = $_product->get_title();
       }

       $check_quantity = $quantity;
       
       //Go thru Current Cart details to see if this item is already in the cart.  If so, add quantity.
       $items = $woocommerce->cart->get_cart();

       foreach($items as $item) { 
            if ($variation_id) { 
               $_product = new WC_Product($item['variation_id']);
               $cart_skucode = $_product->get_sku();
               if($skucode==''){
                 $_product = new WC_Product($item['product_id']);
                 $cart_skucode = $_product->get_sku();
               }
            } else {
               $_product = new WC_Product($item['product_id']);
               $cart_skucode = $_product->get_sku();
            }
            if ($cart_skucode == $skucode){
            	  $check_quantity += $item['quantity'];
            }
       }
       
       $result = econnect_checkstock_cart($skucode,$check_quantity,$description);
       if ($result != ''){
           $passed = false;
           //$result will contain "Error - Description of Item (# Available)"
           $oos_item = substr($result, 8);
           wc_add_notice( __( 'We are sorry, but the item could not be added at this time.  ' . $oos_item . '  Please try again later.', 'textdomain' ), 'error' );
       }
       return $passed;

   }

   function econnect_validate_update_cart_item( $passed, $cart_item_key, $values, $quantity ){

   	   $_product = $values['data'];
       $skucode = $_product->get_sku();
       $check_quantity=$quantity;
       $description = $_product->get_title();
       

       $result = econnect_checkstock_cart($skucode,$check_quantity,$description);
       if ($result != ''){
           $passed = false;
           //$result will contain "Error - Description of Item (# Available)"
           $oos_item = substr($result, 8);
           wc_add_notice( __( 'We are sorry, but the item could not be updated at this time.  ' . $oos_item . '  Please try again later.', 'textdomain' ), 'error' );
       }
       return $passed;	
   }

   add_filter( 'woocommerce_add_to_cart_validation', 'econnect_validate_add_cart_item', 10, 5 );

   add_filter( 'woocommerce_update_cart_validation', 'econnect_validate_update_cart_item', 10, 4 );

}

//If Customer Pricing is used, add filter for computing price, and hook for saving user (customer) price level
//and custom fields to Product record for Price Levels 1-8
if (get_option('econnect_customer_pricing') == '1' ) {

   //Set Custom Price for Item, based on User Price Level
   add_filter('woocommerce_get_price', 'econnect_custom_price', 10, 2);
   function econnect_custom_price($price, $product) {

      $user = wp_get_current_user();
      $skucode = $product->get_sku();
      $product_id = $product->get_id();

      $t=time();
      $user_id = $user->ID ;  

      if ($user_id) {
       	//user is logged in, check price level
         $price_level =  get_user_meta( $user_id, '_econnect_field_PRL', true); 
      }

      //Set Price based on Price Level
      if ($price_level > '' ) {


         //Get Price Level Price for this user (customer)
         $price_level_price = get_post_meta($product_id,'_econnect_price_'.$price_level,true);
         
         //If less than current price, use it. (if on sale, $price will be the sale price)
         if ($price_level_price !='' and $price_level_price < $price)
               $price =  $price_level_price ;
         
       
      }
       
      return $price;
   }

}

//Add Custom Fields to Woocommerce product screen for price level display
// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'econnect_custom_woo_fields' );
function econnect_custom_woo_fields() {

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_1', 
		'label'             => __( 'Price Level 1', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_2', 
		'label'             => __( 'Price Level 2', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_3', 
		'label'             => __( 'Price Level 3', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_4', 
		'label'             => __( 'Price Level 4', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_5', 
		'label'             => __( 'Price Level 5', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_6', 
		'label'             => __( 'Price Level 6', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_7', 
		'label'             => __( 'Price Level 7', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

  woocommerce_wp_text_input( 
	array( 
		'id'                => '_econnect_price_8', 
		'label'             => __( 'Price Level 8', 'woocommerce' ), 
		'placeholder'       => '', 
		'description'       => __( 'ManageMore Price Level - used for Customer-Specific pricing.', 'woocommerce' ),
    'desc_tip'          => true,
		'type'              => 'number', 
		'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			   ) 
	     )
    );

}


// Save Custom Fields for price levels
add_action( 'woocommerce_process_product_meta', 'econnect_custom_woo_fields_save' );
function econnect_custom_woo_fields_save($post_id) {

  // Price 1
	$woocommerce_field = $_POST['_econnect_price_1'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_1', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_1' );
  }
  // Price 2
	$woocommerce_field = $_POST['_econnect_price_2'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_2', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_2' );
  }
  // Price 3
	$woocommerce_field = $_POST['_econnect_price_3'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_3', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_3' );
  }
  // Price 4
	$woocommerce_field = $_POST['_econnect_price_4'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_4', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_4' );
  }
  // Price 5
	$woocommerce_field = $_POST['_econnect_price_5'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_5', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_5' );
  }
  // Price 6
	$woocommerce_field = $_POST['_econnect_price_6'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_6', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_6' );
  }
  // Price 7
	$woocommerce_field = $_POST['_econnect_price_7'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_7', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_7' );
  }
  // Price 8
	$woocommerce_field = $_POST['_econnect_price_8'];
	if( !empty( $woocommerce_field ) ) {
		update_post_meta( $post_id, '_econnect_price_8', esc_attr( $woocommerce_field ) );
  }
  else {
    delete_post_meta( $post_id, '_econnect_price_8' );
  }
		
		
}




// If Check Shipping option is active, add action to check Shipment Status when viewing order
if (get_option('econnect_check_status') == '1' ) {

   add_action( 'woocommerce_view_order', 'econnect_woocommerce_view_order', 10, 1 ); 
   // define the function 
   function econnect_woocommerce_view_order( $order_id ) { 

          global $econnect_request_packet, $econnect_response_packet, $econnect_error;

          $user = wp_get_current_user( $user_id );
          $t=time();
          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];
        
          $econnect_request_packet = "1011|WID=" .  $user->ID . 
                "|OID=" . $order_id .
                "|PPW=" . esc_attr( get_option('econnect_password') ) .
                "|BUA=" . $user_agent .
                "|RIP=" . $ip_address .
                "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);

          econnect_sendpacket();

          if ($econnect_error == '') {

             //parse packet and save response
             //Parse Response
             //a) Explode response by pipe delimiter
             $econnect_temp_array = explode("|",$econnect_response_packet);
             //b) For each item, add to packet array with Key, Value
             $econnect_packet_array=array();
             foreach ($econnect_temp_array as $econnect_value_pair) {
             	 if (substr($econnect_value_pair,0,5) == 'SURL='){
             	 	  //cannot explode URL, since it contains many = signs most likely,
             	 	  //e.g. http://www.fedex.com/Tracking?action=track&language=english&cntry_code=us&initial=x&mps=y&tracknumbers=
                   $econnect_packet_array['SURL'] = substr($econnect_value_pair,5);
             	 }
             	 else {
                   $econnect_value_pair_array = explode("=",$econnect_value_pair);
                   $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
             	 }
             }   

          	  //Display Tracking Number & Link if available
          	  $tracking_url = $econnect_packet_array['SURL'];
          	  $tracking_no = $econnect_packet_array['TRK01'];
          	  $completed = $econnect_packet_array['CMPL'];
          	  $completed_date = $econnect_packet_array['CMPD'];
          	  if ($tracking_no != ''){

          	  	 if ($tracking_url != ''){
          	  	    //display Tracking Info As Link
          	  	    $tracking_info =  'Tracking Number: <a href="'.esc_attr($tracking_url).$tracking_no.'" target="_blank" >'.$tracking_no.'</a><br><br>';
          	  	 }
          	  	 else {
          	  	    //display Tracking Info As Text
          	  	    $tracking_info = 'Tracking Number: '.$tracking_no.'<br><br>';
          	  	 }
          	  	 //save to post meta
                update_post_meta( $order_id, '_econnect_tracking_info',$tracking_info); 


     
          	  }
          	  if ($completed == "1"){

          	  	 //Get Order
 		             $order = new WC_Order( $order_id );
          	  	 if ($order->get_status() != 'completed') {

             	  	 //update status to completed
             	  	 $completed_text = 'ManageMore eConnect Real Time Status Update.';
 	  	 	           $order->update_status('completed', __($completed_text , 'woothemes'));
                   $this_post_status = 'completed';
                   //redirect back to same page, to ensure status displays properly
                   //changing the status shere without a refresh will cause the user to see the old status
                   //this is done by outputting a self-redirecting body
                	 $redirect_url = get_permalink( $post->ID ).'/view-order/'.$order_id;
                	 //force refresh, by returning a body that will self redirect when loaded
       	           //content will display, with no form, then refresh
        	         $content =  '<body onload="newpage()">
                       </body>
                       <script>
                           function newpage() {
                           window.top.location.href = "'.$redirect_url.'"
                           }
                        </script>';
                    echo $content;
          	  	 	
           	  	 }
          	  	 	
          	  }
                 	 
          }

          $tracking_info=get_post_meta( $order_id, '_econnect_tracking_info',true); 
          
          //Apply filter to allow changes tracking info.
          $tracking_info=apply_filters( 'econnect_order_tracking_filter', $tracking_info, $econnect_packet_array );

          echo $tracking_info;


       
   }

}
         


//If CRM is enabled, update MM when profile, or addresses changed
if (get_option('econnect_crm_enabled') == '1'){

  // add action to update mm with email change
  add_action( 'profile_update', 'econnect_profile_update', 10, 2 );
	// define function 
  function econnect_profile_update( $user_id, $old_user_data ) {
       global $econnect_request_packet, $econnect_response_packet, $econnect_error;

       $user_data = get_userdata( $user_id );
       $t=time();
       $custid = get_user_meta( $user_id, '_econnect_field_CID', true); 
       if ($custid == '')
          return;
         
       $geo = new WC_Geolocation();
       $ip_address = $geo->get_ip_address();
       $user_agent = $_SERVER['HTTP_USER_AGENT'];
     
       $econnect_request_packet = "1003|WID=" .  $user->ID . 
             "|LID=" . $custid .
             "|LTP=1" .
             "|UTP=3" .
             "|EM=" . $user_data->user_email .
             "|PPW=" . esc_attr( get_option('econnect_password') ) .
             "|BUA=" . $user_agent .
             "|RIP=" . $ip_address .
             "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);

       econnect_sendpacket();

       if ($econnect_error != '') {

          wc_add_notice( "The changes were not updated to billing system - " . $econnect_error , "error" );
          //send email to admin
          econnect_send_admin_email($econnect_error,'Customer Update');
       	
       }
       else {

          //parse packet and save response
          //Parse Response
          //a) Explode response by pipe delimiter
          $econnect_temp_array = explode("|",$econnect_response_packet);
          //b) For each item, add to packet array with Key, Value
          $econnect_packet_array=array();
          foreach ($econnect_temp_array as $econnect_value_pair) {
             $econnect_value_pair_array = explode("=",$econnect_value_pair);
             $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
          }   

       	  //update meta field for customer email only
          update_user_meta( $user_id, '_econnect_field_EM',$econnect_packet_array['EM']); 
              	 
       }
        
  }

  // add action to update mm with name/address change 
  add_action( 'woocommerce_customer_save_address', 'econnect_customer_save_address', 10, 2 ); 
	// define function 
  function econnect_customer_save_address( $user_id, $load_address ) { 
       global $econnect_request_packet, $econnect_response_packet, $econnect_error;

       $user = wp_get_current_user(); 
      
       $geo = new WC_Geolocation();
       $ip_address = $geo->get_ip_address();
       $user_agent = $_SERVER['HTTP_USER_AGENT'];
       
       // Send packet to eConnect Server to update Name and Address
       $t=time();
       $custid = get_user_meta( $user_id, '_econnect_field_CID', true); 
       if ($custid == '')
          return;
       
       //Set vars from updated WooCommerce data
       //Billing name and address
       $billing_company = get_user_meta( $user_id, 'billing_company', true);
       $billing_first_name = get_user_meta( $user_id, 'billing_first_name', true);
       $billing_last_name = get_user_meta( $user_id, 'billing_last_name', true);
       $billing_addr_1 = get_user_meta( $user_id, 'billing_address_1', true);
       $billing_addr_2 = get_user_meta( $user_id, 'billing_address_2', true);
       $billing_city = get_user_meta( $user_id, 'billing_city', true);
       $billing_state = get_user_meta( $user_id, 'billing_state', true);
       $billing_postcode = get_user_meta( $user_id, 'billing_postcode', true);
       $billing_phone = get_user_meta( $user_id, 'billing_phone', true);
       $billing_email = get_user_meta( $user_id, 'billing_email', true);
       
       //Alt Email: If billing email is different from main email, make it the alt, otherwise leave alt as is.
       $alt_email =  ($billing_email !=  $user->user_email) ? $billing_email : get_user_meta( $user_id, '_econnect_field_AEM', true);
      
       //Shipping name and address
       $shipping_company = get_user_meta( $user_id, 'shipping_company', true);
       $shipping_first_name = get_user_meta( $user_id, 'shipping_first_name', true);
       $shipping_last_name = get_user_meta( $user_id, 'shipping_last_name', true);
       $shipping_address_1 = get_user_meta( $user_id, 'shipping_address_1', true);
       $shipping_address_2 = get_user_meta( $user_id, 'shipping_address_2', true);
       $shipping_city = get_user_meta( $user_id, 'shipping_city', true);
       $shipping_state = get_user_meta( $user_id, 'shipping_state', true);
       $shipping_postcode = get_user_meta( $user_id, 'shipping_postcode', true);
       $alt_ship_to = ($shipping_address_1 != '') ? "1" : "0";
      
       $econnect_request_packet = "1003|WID=" . $user_id  . 
             "|LID=" . $custid .
             "|LTP=1" .
             "|UTP=10" .
             "|CN=" . $billing_company .
             "|FN=" .	$billing_first_name .
             "|LN=" . $billing_last_name .
             "|BA1=" . $billing_addr_1 .
             "|BA2=" . $billing_addr_2 .
             "|BA3=" .
             "|BC=" . $billing_city .
             "|BS=" . $billing_state .
             "|BZ=" .	$billing_postcode .
             "|BCN=" .
             "|AST=" . $alt_ship_to .
             "|SCM=" . $shipping_company .
             "|SCM=" . $shipping_first_name . ' '. $shipping_last_name .
             "|SA1=" . $shipping_address_1 .
             "|SA2=" . $shipping_address_2 .
             "|SA3=" .
             "|SC=" . $shipping_city .
             "|SS=" . $shipping_state .
             "|SZ=" . $shipping_postcode .
             "|SCN=" .
             "|P1=" . $billing_phone .
             "|PP1=" . get_user_meta( $user_id, '_econnect_field_PP1', true) .
             "|P2=" . get_user_meta( $user_id, '_econnect_field_P2', true) .
             "|PP2=" . get_user_meta( $user_id, '_econnect_field_PP2', true) .
             "|EM=" .  $user->user_email .
             "|AEM=" .  $alt_email .
             "|PPW=" . esc_attr( get_option('econnect_password') ) .
             "|BUA=" . $user_agent .
             "|RIP=" . $ip_address .
             "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);

       econnect_sendpacket();
       
       if ($econnect_error != '') {

          wc_add_notice( "The changes were not updated to billing system - " . $econnect_error , "error" );
          //Send email to admin
          econnect_send_admin_email($econnect_error,'Customer Address Update');
       	
       }
       else {

          //parse packet and save response
          //Parse Response
          //a) Explode response by pipe delimiter
          $econnect_temp_array = explode("|",$econnect_response_packet);
          //b) For each item, add to packet array with Key, Value
          $econnect_packet_array=array();
          foreach ($econnect_temp_array as $econnect_value_pair) {
             $econnect_value_pair_array = explode("=",$econnect_value_pair);
             $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
          }   

       	  //update meta fields for customer
          foreach ($econnect_packet_array as $field => $value) {
             update_user_meta( $user_id, '_econnect_field_'.$field,$value); 
          }
              	 
       }
       

       
       
  }; 
         
}


//Shortcodes -----------------------------------------------------------------------------------------
//Add Shortcode for econnect values
add_shortcode( 'econnect_field', 'econnect_shortcode_field' );
function econnect_shortcode_field( $atts ) {

  //If CRM not enabled, do not display info
	if (get_option('econnect_crm_enabled') != '1')
	   return;

  $user = wp_get_current_user();
  $user_id = $user->ID ;

  if ($user_id != 0) {

  	//If user has not successfully logged in to this Account, do not display info
    $custid = get_user_meta( $user_id, '_econnect_field_CID' , true);
    $current_custid = get_user_meta( $user_id, '_econnect_field_current_CID' , true);
  	if ($custid=='' || $current_custid != $custid)
	    return;


    $value = get_user_meta( $user_id, '_econnect_field_'.$atts['field'] , true);
    //if field is an amount, format appropriately
    if (in_array($atts['field'],array('B','LAR','LSIA'))){
       if (is_numeric($value))
          $value = number_format($value,2);
    	
    }
  	
  }
         

  return $value;
	

}

//Add shortcode for customer payment form
add_shortcode( 'econnect_customer_payment','econnect_shortcode_payment');
function econnect_shortcode_payment ( $atts ) {
	
  //If CRM not enabled, do not display info
	if (get_option('econnect_crm_enabled') != '1')
	   return;
	   

  //Get Customer, and its Balance
  $user = wp_get_current_user();
  $user_id = $user->ID ;

	//If user has not successfully logged in to this Account, do not display info
  $custid = get_user_meta( $user_id, '_econnect_field_CID' , true);
  $current_custid = get_user_meta( $user_id, '_econnect_field_current_CID' , true);
	if ($user_id == 0 || $custid=='' || $current_custid != $custid)
	   return;


  $balance = get_user_meta( $user_id, '_econnect_field_B' , true);
  $balance = number_format($balance,2);

  //set defaults for atts
  $atts = shortcode_atts( array(
		'gateway' => get_option('econnect_default_gateway')
	), $atts, 'econnect_customer_payment' );	
	
	//set current gateway from atts or default
	$current_gateway = $atts['gateway'];
	
	global $woocommerce;

  //Set Payment Module
  if ($current_gateway == 'vantiv' ){
     $paymentmodule = new WC_Gateway_ManageMore_Vantiv();
  }
  else {
     $paymentmodule = new WC_Gateway_ManageMore_Cayan();
  }

	$error_message = "";
	
  //If payment just submitted
  if (isset($_POST['payment_submitted']) && check_admin_referer('econnect_payment_button_clicked')) {
  	  
  	  if ($current_gateway == 'vantiv' ){
  	  	//should not happen yet, not supported.
  	  }
  	  else {
  	  	

     	  //Validation
     	  $success = $paymentmodule->validate_fields(); 
     	  if ( $success == "1"){
    	  	$pmt = str_replace( ',', '', $_POST['econnect_customer_payment_amount'] );  //remove commas from payment amount
    	  	
       	  //Call Gateway
       	  //First, set array for payment info
       	  $curr_time = time();
       	  $payment = Array(
         	  'amount' => number_format($pmt,2,'.',''),
         	  'cust_id' => get_user_meta( $user_id, '_econnect_field_CID' , true),
         	  'payment_no' => substr($curr_time,-8),
         	  'customer_name' => get_user_meta( $user_id, '_econnect_field_CUST' , true),
         	  'company_name' => get_user_meta( $user_id, '_econnect_field_CN' , true),
         	  'first_name' => get_user_meta( $user_id, '_econnect_field_FN' , true),
         	  'last_name' => get_user_meta( $user_id, '_econnect_field_LN' , true),
         	  'cust_name' => get_user_meta( $user_id, '_econnect_field_CUST' , true),
         	  'bill_addr_1' => get_user_meta( $user_id, '_econnect_field_BA1' , true),
         	  'bill_addr_2' => get_user_meta( $user_id, '_econnect_field_BA2' , true),
         	  'bill_city' => get_user_meta( $user_id, '_econnect_field_BC' , true),
         	  'bill_state' => get_user_meta( $user_id, '_econnect_field_BS' , true),
         	  'bill_zip' => get_user_meta( $user_id, '_econnect_field_BZ' , true),
         	  'bill_email' => $user->user_email
       	  );
       	  $paymentmodule->process_payment_crm( $payment );
       	  $redirect_url = get_permalink( $post->ID );
       	  //force refresh, by returning a body that will self redirect when loaded
       	  //content will display, with no form, then refresh
        	$content =  '<body onload="newpage()">
                       </body>
                       <script>
                           function newpage() {
                           window.top.location.href = "'.$redirect_url.'"
                           }
                       </script>';
          return $content;

     	  }

     	  
  	  	
  	  }
  }
	


	if ($current_gateway=='vantiv'){
 	   $result =  "We're sorry, but customer payments cannot be accepted at this time.";
 	   return $result;
	}
	else {
     //default to cayan		
     if ($card_number != ''){
        return $card_number;    	
     }
     if ($balance <= 0){
 	 	   ob_start(); //start buffering output
       wc_print_notices();
       $content = ob_get_clean();  //get output buffer and clean contents
   	   $result =  $content."There is no payment due at this time.";
 	     return $result;
     	
     }
  	 elseif ($paymentmodule->enabled == "no"){
   	   $result =  "We're sorry, but customer payments cannot be accepted at this time.";
 	     return $result;
  	 	
  	 }
  	 else {
  	 	  //Note: The payment module functions echo results.  So we use output buffering.
  	 	  ob_start(); //start buffering output
        wc_print_notices();
      	echo '<form action="'. get_permalink( $post->ID ) .'" method="post">';
      	echo '<input type="hidden" value="true" name="payment_submitted" />';
      	echo 'Payment Amount: <input class="input-text qty text" name="econnect_customer_payment_amount" value="'.$balance.'"><br><br>';
        if ($paymentmodule->cardonfile=='no'){
        	 //card on file not allowed.  show cc form without tokenization options
           $paymentmodule->form();
        }
        else {
        	 //card on file allowed.  show cc form with tokenization options
   	       $paymentmodule->tokenization_script();
           $paymentmodule->saved_payment_methods();
           $paymentmodule->form();
        }
        wp_nonce_field('econnect_payment_button_clicked');
        echo '<input type="submit" id="submit_button" value="Submit"/>';
        echo '</form>';
        $content = ob_get_clean();  //get output buffer and clean contents
        return $content;
  	 }
 
	}
	
}

add_shortcode( 'econnect_transaction_display','econnect_shortcode_transaction_display');
function econnect_shortcode_transaction_display ( $atts ) {
  	if (isset($_GET["OID"])){
	     ob_start(); //start buffering output, beacuse we will return a string with the output
       $oid = $_GET["OID"]; 
       $response = econnect_get_transaction($oid);
       if (!is_array($response)){
       	 //the response is not an array, must be an error. post error
         wc_add_notice( 'Transaction cannot be displayed. '. $response, 'error' ); 
         wc_print_notices();
       }
       else {
       	 $no_buttons = true ;
         econnect_transaction_display($response,$no_buttons);
       }
       $content = ob_get_clean();  //get output buffer and clean contents
       return $content;
       
  	}
  	return;


}

add_shortcode( 'econnect_customer_account_history','econnect_shortcode_account_history');
function econnect_shortcode_account_history ( $atts ) {

	global $woocommerce;

  //If CRM not enabled, do not display info
	if (get_option('econnect_crm_enabled') != '1')
	   return;

  //Get Customer
  $user = wp_get_current_user();
  $user_id = $user->ID ;

	//If user has not successfully logged in to this Account, do not display info
  $custid = get_user_meta( $user_id, '_econnect_field_CID' , true);
  $current_custid = get_user_meta( $user_id, '_econnect_field_current_CID' , true);
	if ($user_id == 0 || $custid=='' || $current_custid != $custid)
	   return;


  if (isset($_POST['account_history_submitted']) && check_admin_referer('econnect_history_button_clicked')) {
    //If payment request just submitted

    $status = 1; // Process Packet
    $response = econnect_customer_history_query();
    //if response is an array, then there was no error
    if (is_array($response)){
    	 $history_list_array = econnect_parsehistorylistpacket($response);
    	 if (is_array($history_list_array)){
    	   $history_type = $_POST['QueryType'];
    	 }
    	 else{ 
         wc_add_notice( 'There is no information to display for the option chosen.  Please choose another option.', 'error' ); 
         $status = 0; // reset status
    	 }
    }
    else {
    	 //the response is not an array, must be an error. post error
       wc_add_notice( 'Customer History cannot be displayed. '. $response, 'error' ); 
       $status = 0; // reset status
    }

  }
  else if (isset($_POST['account_transaction_email_submitted']) && check_admin_referer('econnect_transaction_email_button_clicked')) {
       $oid = sanitize_text_field( $_POST["OID"]); 
       $sid = sanitize_text_field( $_POST["SID"]);
       $document_name = ($sid != '')  ? 'Statement' : 'Transaction';
       $email = sanitize_email($_POST["Email"]); 
       $status = 3; // return to form
       //If email request just submitted
       $response = econnect_transaction_email_send($oid,$sid,$email);
       //if response is an array, then there was no error
       if (is_array($response)) {
         wc_add_notice( $document_name . ' was e-mailed to '.$email, 'success' ); 
       	
       }
       else {
         wc_add_notice( 'Transaction could not be sent. '. $response, 'error' ); 
       	
       }
  }
  else {
  	if (isset($_POST["EM"])){
  		 //if flag was passed that user is looking to request an email copy
  		 //the user has not confirmed email address yet, so call the appropriate form
  		 $sid = sanitize_text_field( $_POST["SID"]);
  		 $oid = sanitize_text_field( $_POST["OID"]);
	     $status = 3;  //email transaction
  	}
  	elseif (isset($_POST["OID"])){
       $status = 2; //Display Transaction 
       $oid =sanitize_text_field(  $_POST["OID"]); 
       $response = econnect_get_transaction($oid);
       if (!is_array($response)){
       	 //the response is not an array, must be an error. post error
         wc_add_notice( 'Transaction cannot be displayed. '. $response, 'error' ); 
         $status = 0; // reset status
       	
       }
  	}
    else {
       $status = 0; // Asking for history type
    }
  	
  }


  switch ($status) { 	
 	  case 1:  
 	     //Display History List
	     ob_start(); //start buffering output, beacuse we will return a string with the output
       wc_print_notices();
       econnect_customer_history_display($history_type,$history_list_array);
       $content = ob_get_clean();  //get output buffer and clean contents
 	     break;

 	  case 2:  
 	     //Display Transaction
	     ob_start(); //start buffering output, beacuse we will return a string with the output
       wc_print_notices();
       econnect_transaction_display($response);
       $content = ob_get_clean();  //get output buffer and clean contents
 	     break;

 	  case 3:  
 	     //Request E-Mail of Transaction/Statement
	     ob_start(); //start buffering output, beacuse we will return a string with the output
       wc_print_notices();
       econnect_transaction_email_form();
       $content = ob_get_clean();  //get output buffer and clean contents
 	     break;
 	  
 	  default: 
 	  	
 	     //default (0) - Ask for type of history to display and date range
 	     
 	     //enqueue our script for hiding, showing date range
       wp_enqueue_script('econnect-acccount-history-selection', plugins_url( 'js/econnect-account-history-selection.js', __FILE__), array('jquery'), NULL, false);
       
	     ob_start(); //start buffering output, beacuse we will return a string with the output
       wc_print_notices();
       econnect_customer_history_form();
       $content = ob_get_clean();  //get output buffer and clean contents
 	     break;
     
	}
     
    return $content;
	
}

function econnect_transaction_email_form(){

       $user = wp_get_current_user(); 
       $email = $user->user_email;
       $oid=sanitize_text_field( $_POST["OID"]);
       $sid=sanitize_text_field( $_POST["SID"]);

       $heading = ($sid != '') ? 'E-Mail Statement' : 'E-Mail Transaction';
       $is_statement = ($sid != '') ? true : false;
       $heading = apply_filters( 'econnect_email_form_heading_filter', $heading, $is_statement );

       echo '<body>';
       echo '<form action="'. get_permalink( $post->ID ).'" method="post">';
       echo '<b>'.$heading.'</b><br><br>';
       echo '<label for="Email">E-Mail Address</label>';
       echo '<input required class="form-control" type="email" name="Email" id="Email" maxlength="80" placeholder="E-Mail Address" value="'. $email .'"><br>';
       wp_nonce_field('econnect_transaction_email_button_clicked');
       echo '<input type="hidden" value="true" name="account_transaction_email_submitted" />';
       echo '<input type="hidden" value="'.$oid.'" name="OID" />';
       echo '<input type="hidden" value="'.$sid.'" name="SID" />';
       echo '<br>';
       echo '<input type="submit" id="trans_email_submit_button" value="Submit"/>';
       echo '</form>';
       if ($is_statement == true){
       	 echo '<form action="'. get_permalink( $post->ID ).'" method="post">'.
              '<input type="submit" id="submit_button" value="Back"/>'.
              '</form>'; 
       }
       else {
          echo econnect_view_transaction_button($oid,"Back to Transaction");
       }
       echo '</body>';

}

function econnect_customer_history_form(){


       //set start date
       $startdate=date_create();
       date_modify($startdate,"-30 days");
       $startdatestr= date_format($startdate,"Y-m-d");

       //set end date
       $enddate=date_create();
       $enddatestr= date_format($enddate,"Y-m-d");

       //Apply filter to allow changes to strings used.
       $heading=apply_filters( 'econnect_account_history_heading_filter', "Details to Show" );
       $outstanding_invoices=apply_filters( 'econnect_account_history_choice_filter', "Outstanding Invoices",1 );
       $account_details=apply_filters( 'econnect_account_history_choice_filter', "Account Details",2 );
       $statement_history=apply_filters( 'econnect_account_history_choice_filter', "Statement History",3 );

  
       echo '<body>';
       echo '<form action="'. get_permalink( $post->ID ).'" method="post">';
       echo '<b>'.$heading.'</b><br><br>';
       echo '<label><input required type="radio" checked name="QueryType" id="QueryType" value="1"> '.$outstanding_invoices.'</label><br><br>';
       echo '<label><input required type="radio"  name="QueryType" id="QueryType" value="2"> '.$account_details.'</label><br><br>';
       if (get_option('econnect_allow_statements') == '1' ) {
          echo '<label><input required type="radio" name="QueryType" id="QueryType" value="3"> '.$statement_history.'</label><br><br><br>';
       }
       echo '<div class="form-group form-inline" id="DateEntry">';
       echo '<label for="HistoryStartDate">Date Range</label>';
       echo '<input required class="form-control" type="date" name="HistoryStartDate" id="HistoryStartDate" maxlength="10" size="10" placeholder="StartDate" value="'. $startdatestr .'">&nbsp;&nbsp;&nbsp; to &nbsp;&nbsp;';
       echo '<input required class="form-control" type="date" name="HistoryEndDate" id="HistoryEndDate" maxlength="10" size="10" placeholder="EndDate" value="'.$enddatestr .'">';
       echo '<br><br>';
       echo '</div>';
       // this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces
       wp_nonce_field('econnect_history_button_clicked');
       echo '<input type="hidden" value="true" name="account_history_submitted" />';
       echo '<input type="submit" id="submit_button" value="Submit"/>';
       echo '</form>';
       echo '</body>';

}

//output a table for customer history ( invoices, payments, statements )
function econnect_customer_history_display($history_type,$history_list_array) {

      $column_values = array();
      $column_values[1]=array('1'=>'Invoice Info','2'=>'Transaction Info','3'=>'Statement Info');
      $column_values[2]=array('1'=>'Other Info','2'=>'Other Info','3'=>'Other Info');
      $column_values[3]=array('1'=>'Balance','2'=>'Amount','3'=>'Balance');

      $history_display  =  '<table>';
      $column_header = array();
      for ($column = 1; $column < 4; $column++) {
       	  $column_header[$column] = apply_filters('econnect_history_list_column_header_filter',$column_values[$column][$history_type],$column,$history_type);
      }
      
      $table_header =  '<thead>'.
                       '<tr>'.
                       '<th>'. $column_header[1].'</th>'.
                       '<th>'. $column_header[2].'</th>'.
                       '<th class="text-right">'.$column_header[3].'&nbsp;&nbsp;</th>'.
                       '<th></th>'.
                       '</tr>'.
                       '</thead>';

      //Filter Header
      $table_header = apply_filters( 'econnect_history_list_header_filter', $table_header, $history_list_array, $history_type );
      
      $history_display .= $table_header;

      foreach ($history_list_array as $history_list_row) { 
   	  	  $due_string = ($history_list_row["DUE"] != '') ? '<br>Due: ' . $history_list_row["DUE"]:'';
   	  	  $po_string = ($history_list_row["PON"]!='') ? '<br>PO: ' . $history_list_row["PON"]:'';
      	  switch($history_type){
      	  	case 1:
              $table_row = '<tr>'.
       	  	               '<td>';
       	  	               
       	  	  $column_info= '<br><b>'.$history_list_row["TID"].'</b><br>'.
                            $history_list_row["TDT"] .'<br>';
              //Filter Column Info
              $column = 1;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                            
              $table_row .= $column_info .
                           '</td>'.
                           '<td>';

              $column_info = $due_string . 
                             $po_string ;

              //Filter Column Info
              $column = 2;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                            
              $table_row .= $column_info .
                            '</td>'.
                            '<td class="text-right">';
                           
              $column_info = '<br>'.
                            number_format($history_list_row["B"],2) . '&nbsp;&nbsp;<br>'.
                            '<br><br>';
              //Filter Column Info
              $column = 3;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );

              $table_row .= $column_info .
                            '</td>'.
                            '<td>';
                            
              $column_info = "";
                           if ($history_list_row["OID"] != ""){
                           	 $column_info .= econnect_view_transaction_button($history_list_row["OID"]);
                           }
              //Filter Column Info
              $column = 4;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );

              $table_row .= $column_info .
                            '</td>'.
                            '</tr>';
      	  	               break;
      	  	case 2:
              $table_row = '<tr>'.
                           '<td>';
                           
              $column_info = $history_list_row["DESC"] . ' ' . $history_list_row["REF"]. '<br>'.
                             '<b>'. $history_list_row["TID"].'</b><br>'.
                             $history_list_row["TDT"];
              //Filter Column Info
              $column = 1;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                            
              $table_row .= $column_info .
                           '</td>'.
                           '<td>';

              $column_info = $due_string . 
                             $po_string ;

              //Filter Column Info
              $column = 2;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                            
              $table_row .= $column_info .
                           '</td>'.
                           '<td class="text-right">';
                           
              $column_info = '<br>'.
                            number_format($history_list_row["TOT"],2) . '&nbsp;&nbsp;<br>'.
                           '<br>'.  $history_list_row["STAT"] .'<br>';
              //Filter Column Info
              $column = 3;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                           
              $table_row .= $column_info .
                           '</td>'.
                           '<td>';
                           
              $column_info = "";
                           if ($history_list_row["OID"] != ""){
                           	 $column_info .= econnect_view_transaction_button($history_list_row["OID"]);
                           }
              //Filter Column Info
              $column = 4;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );

              $table_row .= $column_info .
                            '</td>'.
                            '</tr>';
      	  	               break;
      	  	case 3:
      	  	  $table_row =  '<tr>'.
                            '<td>';
                            
              $column_info = '<br><b>'.$history_list_row['SID'].'</b><br>'.
                             $history_list_row['TDT']. '<br>';
              //Filter Column Info
              $column = 1;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                             
              $table_row .= $column_info .
                           '</td>'.
                           '<td>';

              $column_info = $due_string . 
                             $po_string ;

              //Filter Column Info
              $column = 2;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                            
              $table_row .= $column_info .
                            '</td>'.
                            '<td class="text-right">';
                            
              $column_info = '<br>'.
                             number_format($history_list_row["B"],2) . '&nbsp;&nbsp;<br>'.
                            '<br><br>';
              //Filter Column Info
              $column = 3;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                            
              $table_row .= $column_info .
                            '</td>'.
                            '<td>';
                            
              $column_info = econnect_email_request_button($oid,$history_list_row["SID"],"E-Mail Statement");
              /*
                            '<form action="'. get_permalink( $post->ID ).'" method="post">'.
                            '<input type="hidden" id="SID" value="'.$history_list_row["SID"].'" />'.
                            '<input type="hidden" id="Email" value="1" />'.
                            '<input type="submit" id="submit_button" value="E-Mail" />'.
                            '</form>';
              */
              
              //Filter Column Info
              $column = 3;
              $column_info = apply_filters( 'econnect_history_list_column_filter', $column_info, $history_list_row, $column, $history_type );
                            
              $table_row .= $column_info .
                            '</td>'.
                            '</tr>';
                            break;
 
      	  	default:
      	  	                break;
      	  }

      	  //Filter Row
 	        $table_row = apply_filters( 'econnect_history_list_row_filter', $table_row, $history_list_row, $history_type );
          $history_display .= $table_row;
          
      }

   	  if ($history_list_array["LXC"]==1){

   	  	 $max_lines = '<tr>'.
                      '<td colspan="3"> Maximum lines shown.  Please choose a smaller <a href="'. get_permalink( $post->ID ).'">date range.</a>'.
                      '</td>'.
                      '</tr>';
         //Filter Max Lines Display
         $max_lines = apply_filters( 'econnect_history_list_max_filter', $max_lines, $history_list_array, $history_type );

         $history_display .= $max_lines ;             
   	  }

      $history_display .= '</table>'; 

      $back_button = '<form action="'. get_permalink( $post->ID ).'" method="post">'.
                     '<input type="submit" id="submit_button" value="Back"/>'.
                     '</form>'; 

      //Filter Back Button
      $back_button = apply_filters( 'econnect_history_list_back_filter', $back_button, $history_list_array, $history_type );

      $history_display .= $back_button;
      echo ($history_display);
}


// Call econnect server to send a Customer History Query packet (1002) and return resoponse array or error
function econnect_customer_history_query() {

   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

 
   //Get current user (customer) and set date fields for passing
   $user = wp_get_current_user();
   $user_id = $user->ID ;
   $startdate=date_create($_POST['HistoryStartDate']);
   $startdatestr= date_format($startdate,"m/d/Y");
   $enddate=date_create($_POST['HistoryEndDate']);
   $enddatestr= date_format($enddate,"m/d/Y");

 
   //Setup packet Info
   $packet_array = array();
   $geo = new WC_Geolocation();
   $ip_address = $geo->get_ip_address();
   $user_agent = $_SERVER['HTTP_USER_AGENT'];
   $packet_array['WID']=time();
   $packet_array['LID']=get_user_meta( $user_id, '_econnect_field_CID' , true);
   $packet_array['LTP']="1";
   $packet_array['QT']=$_POST["QueryType"];
   $packet_array['RT']="1";
   $packet_array['SD']=$startdatestr;
   $packet_array['ED']=$enddatestr;
   $packet_array['PPW']=esc_attr( get_option('econnect_password') );
   $packet_array['BUA']=$user_agent;
   $packet_array['RIP']=$ip_address;
   $packet_array['D']=date("m/d/y",$t);
   $packet_array['T']=date("h:iA",$t);
      
   //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
   $econnect_request_packet = '';
   foreach ($packet_array as $array_key => $array_value ) {
      	 $econnect_request_packet .= '|'. $array_key . '=' . $array_value ;
   } 
   $econnect_request_packet = '1002' . $econnect_request_packet . chr(4);  //add packet type, end with chr(4)


   econnect_sendpacket();

   if ($econnect_error == '') {

      //Parse Response
      //a) Explode response by pipe delimiter
      $econnect_temp_array = explode("|",$econnect_response_packet);
      //b) For each item, add to packet array with Key, Value
      $econnect_packet_array=array();
      foreach ($econnect_temp_array as $econnect_value_pair) {
      	 if (substr($econnect_value_pair,0,5) == 'SURL='){
      	 	  //cannot explode URL, since it contains many = signs most likely,
      	 	  //e.g. http://www.fedex.com/Tracking?action=track&language=english&cntry_code=us&initial=x&mps=y&tracknumbers=
            $econnect_packet_array['SURL'] = substr($econnect_value_pair,5);
      	 }
      	 else {
            $econnect_value_pair_array = explode("=",$econnect_value_pair);
            $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
      	 }
      } 
             
      return $econnect_packet_array;       
   }
   else {
   	
      return $econnect_error;

   }

}  


// Call econnect server to send a Transaction Email Request packet (1002) and return resoponse array or error
function econnect_transaction_email_send($oid = "",$sid="",$email) {

   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

 
   //Get current user (customer) and set date fields for passing
   $user = wp_get_current_user();
   $user_id = $user->ID ;
   $user_info = get_userdata($user_id);
   $user_login = $user_info->user_login;

 
   //Setup packet Info
   $packet_array = array();
   $geo = new WC_Geolocation();
   $ip_address = $geo->get_ip_address();
   $user_agent = $_SERVER['HTTP_USER_AGENT'];
   $packet_array['WID']=time();
   $packet_array['OID']=$oid;
   $packet_array['SID']=$sid;
   $packet_array['LID']=$user_login;
   $packet_array['EM']=$email;
   $packet_array['PPW']=esc_attr( get_option('econnect_password') );
   $packet_array['BUA']=$user_agent;
   $packet_array['RIP']=$ip_address;
   $packet_array['D']=date("m/d/y",$t);
   $packet_array['T']=date("h:iA",$t);
      
   //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
   $econnect_request_packet = '';
   foreach ($packet_array as $array_key => $array_value ) {
      	 $econnect_request_packet .= '|'. $array_key . '=' . $array_value ;
   } 
   $econnect_request_packet = '1098' . $econnect_request_packet . chr(4);  //add packet type, end with chr(4)


   econnect_sendpacket();

   if ($econnect_error == '') {

      //Parse Response
      //a) Explode response by pipe delimiter
      $econnect_temp_array = explode("|",$econnect_response_packet);
      //b) For each item, add to packet array with Key, Value
      $econnect_packet_array=array();
      foreach ($econnect_temp_array as $econnect_value_pair) {
         $econnect_value_pair_array = explode("=",$econnect_value_pair);
         $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
      } 
             
      return $econnect_packet_array;       
   }
   else {
   	
      return $econnect_error;

   }

}  


/* econnect_parsehistorylistpacket()
   --------------------------------
   This function is used convert the econnect_packet_array, which contains
   field/value pair elements, to the econnect_history_list_array, which is a
   multi-dimensional array which contains an element for each statement/tranasction returned
   and a sub-element for each field of the item.  (e.g., $econnect_history_list_array[1]['SID'])
   Return Value: array - $econnect_history_list_array
*/
function econnect_parsehistorylistpacket($econnect_packet_array) {
  
     $returned_quantity = $econnect_packet_array['RETQTY'];
     for ($i=1; $i <= $returned_quantity; $i++){
        $item = sprintf("%03s",$i); //001 - 999
        $econnect_statement_list_array[$i]['SID'] = $econnect_packet_array['SID' . $item];
        $econnect_statement_list_array[$i]['TID'] = $econnect_packet_array['TID' . $item];
        $econnect_statement_list_array[$i]['OID'] = $econnect_packet_array['OID' . $item];
        $econnect_statement_list_array[$i]['PON'] = $econnect_packet_array['PON' . $item];
        $econnect_statement_list_array[$i]['DESC'] = $econnect_packet_array['DESC' . $item];
        $econnect_statement_list_array[$i]['REF'] = $econnect_packet_array['REF' . $item];
        $econnect_statement_list_array[$i]['STAT'] = $econnect_packet_array['STAT' . $item];
        $econnect_statement_list_array[$i]['LOC'] = $econnect_packet_array['LOC' . $item];
        $econnect_statement_list_array[$i]['TRM'] = $econnect_packet_array['TRM' . $item];
        $econnect_statement_list_array[$i]['TXC'] = $econnect_packet_array['TXC' . $item];
        $econnect_statement_list_array[$i]['NTE'] = $econnect_packet_array['NTE' . $item];
        $econnect_statement_list_array[$i]['UDF1'] = $econnect_packet_array['UDF1' . $item];
        $econnect_statement_list_array[$i]['DUE'] = $econnect_packet_array['DUE' . $item];
        $econnect_statement_list_array[$i]['TDT'] = $econnect_packet_array['TDT' . $item];
        $econnect_statement_list_array[$i]['PDT'] = $econnect_packet_array['PDT' . $item];
        $econnect_statement_list_array[$i]['TAX'] = $econnect_packet_array['TAX' . $item];
        $econnect_statement_list_array[$i]['DISC'] = $econnect_packet_array['DISC' . $item];
        $econnect_statement_list_array[$i]['FRT'] = $econnect_packet_array['FRT' . $item];
        $econnect_statement_list_array[$i]['TOT'] = $econnect_packet_array['TOT' . $item];
        $econnect_statement_list_array[$i]['B'] = $econnect_packet_array['B' . $item];
        
     }
     return $econnect_statement_list_array;
}


function econnect_transaction_display($econnect_packet_array, $no_buttons = false){


     $header_text =  '<header>'.     
                '<h2>'.$econnect_packet_array['THD'].'</h2>'.
                '</header>';


     $header_table = '<table class="shop_table customer_details">'.
	                   '<tbody>'.
	                   '<tr>'.
		                 '<th>Transaction No</th>'.
		                 '<td>'.$econnect_packet_array['TID'].'</td>'.
		                 '</tr>'.
	                   '<tr>'.
		                 '<th>Transaction Date</th>'.
		                 '<td>'.$econnect_packet_array['TDT'].'</td>'.
		                 '</tr>'.
	                   '<tr>'.
		                 '<th>Account</th>'.
		                 '<td>'.$econnect_packet_array['CID'].'</td>'.
		                 '</tr>';
     $end_of_header = apply_filters( 'econnect_transaction_end_of_header_filter', $end_of_header, $econnect_packet_array );
     $header_table .= $end_of_header .
	                   '</tbody>'.
	                   '</table>';


 	  $tracking_url = $econnect_packet_array['SURL'];
 	  $tracking_no = $econnect_packet_array['TRK01'];
 	  if ($tracking_no != ''){

 	  	 if ($tracking_url != ''){
 	  	    //display Tracking Info As Link
 	  	    $tracking_info =  'Tracking Number: <a href="'.esc_attr($tracking_url).$tracking_no.'" target="_blank" >'.$tracking_no.'</a><br><br>';
 	  	 }
 	  	 else {
 	  	    //display Tracking Info As Text
 	  	    $tracking_info = 'Tracking Number: '.$tracking_no.'<br><br>';
 	  	 }

 	  }

	   

     $address_table = '<table>'.
	                    '<tr>'.
	                    '<td>'.
	                    '<header class="title">'.
	                    '<h3>'.$econnect_packet_array['BTH'].'</h3>'.
	                    '</header>'.
	                    '<address>'.
	                    $econnect_packet_array['BAD1'].'<br>'.
	                    $econnect_packet_array['BAD2'].'<br>'.
	                    $econnect_packet_array['BAD3'].'<br>'.
	                    $econnect_packet_array['BAD4'].'<br>'.
	                    $econnect_packet_array['BAD5'].'<br>'.
	                    '</address>'.
	                    '</td>'.
	                    '<td>';
	                    
	                    
	   if ($econnect_packet_array['SAD1']!=''){
	     $address_table .= '<header class="title">'.
	                       '<h3>'.$econnect_packet_array['STH'].'</h3>'.
	                       '</header>'.
	                       '<address>'.
	                       $econnect_packet_array['SAD1'].'<br>'.
	                       $econnect_packet_array['SAD2'].'<br>'.
	                       $econnect_packet_array['SAD3'].'<br>'.
	                       $econnect_packet_array['SAD4'].'<br>'.
	                       $econnect_packet_array['SAD5'].'<br>'.
   	                     '</address>';
	   }
	   $address_table .=  '</td>'.
		                    '</tr>'.
	                      '</table>';
	
	   //Transaction Details
	   $transaction_main_table_begin = '<table class="shop_table order_details">';

     //Details Header
	   $transaction_details_header = '<thead>'.
		                               '<tr>'.
		                               '<th class="product-name" width="50%">Item</th>'.
		                               '<th class="product-total">Quantity</th>'.
		                               '<th class="product-total">Price</th>'.
		                               '<th class="product-total">Total</th>'.
		                               '</tr>'.
	                                 '</thead>';
	                          
     //Details Body
	   $transaction_details_body = '<tbody>';
	   $returned_quantity = $econnect_packet_array['RETQTY'];
     for ($i=1; $i <= $returned_quantity; $i++){
          $item = sprintf("%03s",$i); //001 - 999
          $transaction_details_body .= '<tr class="order_item alt-table-row">'.
	                                     '<td class="product-name">'.
	                                     $econnect_packet_array['SKU' . $item] . ' - ' . $econnect_packet_array['DESC' . $item];

          if ($econnect_packet_array['INOT' . $item] != "")
             $transaction_details_body .= '<br><br>' . $econnect_packet_array['INOT' . $item] . '<br>';
                   

	        $transaction_details_body .= '</td>'.
	                                     '<td class="product-total">'.
	                                     $econnect_packet_array['QTY' . $item].
	                                     '</td>'.
	                                     '<td class="product-total">'.
	                                     $econnect_packet_array['UPR' . $item].
	                                     '</td>'.
	                                     '<td class="product-total">'.
	                                     $econnect_packet_array['AMT' . $item].
	                                     '</td>'.
                                       '</tr>';
          
     }
		 $transaction_details_body .= '</tbody>';



     //Footer
	   $transaction_footer = '<tfoot>'.
                           '<tr>'.
                           '<th scope="row"><br><br><br>Total Taxes:</th>'.
		                       '<td><br><br><br><span class="woocommerce-Price-amount amount">'.$econnect_packet_array['TTX'].'</span></td>'.
                           '</tr>';
     if($econnect_packet_array['FRT']!=0 ){
        $transaction_footer .= '<tr>'.
                               '<th scope="row">Shipping Amount:</th>'.
		                           '<td><span class="woocommerce-Price-amount amount">'.$econnect_packet_array['FRT'].'</span></td>'.
                               '</tr>';
     }
     if($econnect_packet_array['DSC']!=0 ){
        $transaction_footer .= '<tr>'.
                               '<th scope="row">Discount Amount:</th>'.
		                           '<td><span class="woocommerce-Price-amount amount">'.$econnect_packet_array['DSC'].'</span></td>'.
                               '</tr>';
     }
     $transaction_footer .= '<tr>'.
                            '<th scope="row">Total Amount:</th>'.
		                        '<td><span class="woocommerce-Price-amount amount">'.$econnect_packet_array['TOT'].'</span></td>'.
                            '</tr>';
     if(($econnect_packet_array['TTP']==1 || $econnect_packet_array['TTP']==7) && $econnect_packet_array['TST']==0){
       $transaction_footer .= '<tr>'.
                              '<th scope="row">Balance:</th>'.
	  	                        '<td><b><span class="woocommerce-Price-amount amount">'.$econnect_packet_array['BAL'].'</span></b></td>'.
                              '</tr>';
     }

     $end_of_footer = apply_filters( 'econnect_transaction_end_of_footer_filter', $end_of_footer, $econnect_packet_array );

		 $transaction_footer .= $end_of_footer . '</tfoot>';


     $transaction_main_table_end = '</table>';
     
     //Buttons
     if ($no_buttons == false){
     	  
        $transaction_buttons = econnect_email_request_button($econnect_packet_array["OID"],$sid,"Get E-Mail Copy").
        
        /*
                               '<form action="'. get_permalink( $post->ID ).'" method="post">'.
                               '<input type="hidden" value="'.$econnect_packet_array["OID"].'" name="OID" />'.
                               '<input type="hidden" value="1" name="EM" />'.
                               '<input type="submit" id="email_submit_button" value="Get E-Mail Copy"/>'.
                               '</form>'.
        */
                               '<input type="submit" id="submit_button" value="Back" onclick="window.history.back()"/>';
                               
     }
     else {
     	  $transaction_buttons = '';
     }

     //Filter each part
     $header_text = apply_filters( 'econnect_transaction_header_text_filter', $header_text, $econnect_packet_array );
     $header_table = apply_filters( 'econnect_transaction_header_table_filter', $header_table, $econnect_packet_array );
     $tracking_info=apply_filters( 'econnect_transaction_tracking_filter', $tracking_info, $econnect_packet_array );
     $address_table  = apply_filters( 'econnect_transaction_address_table_filter', $address_table, $econnect_packet_array );
     $transaction_details_header = apply_filters( 'econnect_transaction_details_header_filter', $transaction_details_header, $econnect_packet_array );
     $transaction_details_body = apply_filters( 'econnect_transaction_details_body_filter', $transaction_details_body, $econnect_packet_array );
     $transaction_footer = apply_filters( 'econnect_transaction_footer_filter', $transaction_footer, $econnect_packet_array );
     $transaction_before_buttons = apply_filters( 'econnect_transaction_before_buttons_filter', $transaction_before_buttons, $econnect_packet_array );
     $transaction_buttons = apply_filters( 'econnect_transaction_buttons_filter', $transaction_buttons, $econnect_packet_array );
     $transaction_end = apply_filters( 'econnect_transaction_end_filter', $transaction_end, $econnect_packet_array );
     
     //Put it all together
     $transaction_display = $header_text . 
                            $header_table .
                            $tracking_info .
                            $address_table .
                            $transaction_main_table_begin .
                            $transaction_details_header .
                            $transaction_details_body .
                            $transaction_footer .
                            $transaction_main_table_end .
                            $transaction_before_buttons .
                            $transaction_buttons .
                            $transaction_end ;
                            
     //Filter the whole thing
     $transaction_display = apply_filters( 'econnect_transaction_display_filter', $transaction_display, $econnect_packet_array );
                           
     echo $transaction_display;

	
}


function econnect_view_transaction_button($oid,$value = "View"){

   $button_code =  '<form action="'. get_permalink( $post->ID ).'" method="post">'.
                   '<input type="hidden" value="'.$oid.'" name="OID" />'.
                   '<input type="submit" id="submit_button" value="'.$value.'" />'.
                   '</form>';
   return($button_code);
}

function econnect_email_request_button($oid="",$sid="",$value = "E-Mail"){

   $button_code =  '<form action="'. get_permalink( $post->ID ).'" method="post">'.
                   '<input type="hidden" value="'.$oid.'" name="OID" />'.
                   '<input type="hidden" value="'.$sid.'" name="SID" />'.
                   '<input type="hidden" value="1" name="EM" />'.
                   '<input type="submit" id="submit_button" value="'.$value.'" />'.
                   '</form>';
   return($button_code);
}


function econnect_get_transaction($oid){

   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

   //Setup packet Info
   $packet_array = array();
   $geo = new WC_Geolocation();
   $ip_address = $geo->get_ip_address();
   $user_agent = $_SERVER['HTTP_USER_AGENT'];
   $packet_array['WID']=time();
   $packet_array['OID']=$oid;
   $packet_array['PPW']=esc_attr( get_option('econnect_password') );
   $packet_array['BUA']=$user_agent;
   $packet_array['RIP']=$ip_address;
   $packet_array['D']=date("m/d/y",$t);
   $packet_array['T']=date("h:iA",$t);
      
   //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
   $econnect_request_packet = '';
   foreach ($packet_array as $array_key => $array_value ) {
      	 $econnect_request_packet .= '|'. $array_key . '=' . $array_value ;
   } 
   $econnect_request_packet = '1099' . $econnect_request_packet . chr(4);  //add packet type, end with chr(4)


   econnect_sendpacket();

   if ($econnect_error == '') {

      //Parse Response
      //a) Explode response by pipe delimiter
      $econnect_temp_array = explode("|",$econnect_response_packet);
      //b) For each item, add to packet array with Key, Value
      $econnect_packet_array=array();
      foreach ($econnect_temp_array as $econnect_value_pair) {
      	 if (substr($econnect_value_pair,0,5) == 'SURL='){
      	 	  //cannot explode URL, since it contains many = signs most likely,
      	 	  //e.g. http://www.fedex.com/Tracking?action=track&language=english&cntry_code=us&initial=x&mps=y&tracknumbers=
            $econnect_packet_array['SURL'] = substr($econnect_value_pair,5);
      	 }
      	 else {
            $econnect_value_pair_array = explode("=",$econnect_value_pair);
            $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
      	 }
      } 
             
      return $econnect_packet_array;       
   }
   else {
   	
      return $econnect_error;

   }

	 
}

//Plugin Settings
function managemore_econnect_plugin_settings() {


	//register our settings
	register_setting( 'managemore-econnect-settings-group', 'econnect_disable' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_ip_address' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_port' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_password' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_ssl' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_transtype' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_checkstock' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_checkstock_cart' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_customer_pricing' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_related_method' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_location' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_packetsize' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_crm_enabled' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_default_gateway' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_connect_timeout' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_packet_timeout' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_check_status' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_auto_register' );
	register_setting( 'managemore-econnect-settings-group', 'econnect_allow_statements' );

}


//Setup Plugin Menu
function managemore_econnect_plugin_menu() {
   global $menu, $submenu;
	 //create new top-level menu
	 add_menu_page( 'ManageMore eConnect', 'ManageMore eConnect', 'administrator', 'econnect-main','managemore_econnect_settings_page',  plugins_url('/images/mmlogo.png', __FILE__ ) );
   // workaround to remove submenu for main item
	 $submenu['econnect-main'] = array();   

   //create submenu pages
   add_submenu_page( 'econnect-main', 'ManageMore eConnect Settings', 'Plugin Settings', 'administrator', 'econnect-settings', 'managemore_econnect_settings_page'); 
   add_submenu_page( 'econnect-main', 'ManageMore eConnect Import', 'Import Products', 'administrator', 'econnect-import', 'managemore_econnect_import_page'); 
   add_submenu_page( 'econnect-main', 'ManageMore eConnect Test', 'Test Connection', 'administrator', 'econnect-test', 'managemore_econnect_test_page'); 

   

}

//Setup Action Links
function managemore_econnect_action_links( $links ) {
	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=econnect-settings' ) . '">Plugin Settings</a>',
		'<a href="' . admin_url( 'admin.php?page=econnect-import' ) . '">Import Products</a>',
		'<a href="' . admin_url( 'admin.php?page=econnect-test' ) . '">Test Connection</a>'
	);

	// Merge our new link with the default ones
	return array_merge(  $links, $plugin_links );	
}

//Setup Meta Links
function managemore_econnect_plugin_meta_links( $links, $file ) {
	$plugin = plugin_basename(__FILE__);
	// create link
	if ( $file == $plugin ) {
		return array_merge(
			$links,
			array( '<a href="https://www.managemore.com/docs/v9/econnect/wordpress.htm" target="_blank" >Documentation</a>',
			       '<a href="https://www.managemore.com/intellicharge/application" target="_blank">Sign-Up For a FREE Merchant Account</a>' )
		);
	}
	return $links;

}



//Initialize Gateways
function managemore_gateway_init() {
	// If the parent WC_Payment_Gateway class doesn't exist
	// it means WooCommerce is not installed on the site
	// so do nothing
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;
	
	// If we made it this far, then include the Gateway Classes
	include_once( 'class-wc-gateway-managemore-cayan.php' );
	include_once( 'class-wc-gateway-managemore-vantiv.php' );
	include_once( 'class-wc-gateway-managemore-onaccount.php' );

	// Now that we have successfully included our classes,
	// Add them to WooCommerce
	add_filter( 'woocommerce_payment_gateways', 'add_managemore_cayan_gateway' );
	add_filter( 'woocommerce_payment_gateways', 'add_managemore_vantiv_gateway' );
	add_filter( 'woocommerce_payment_gateways', 'add_managemore_onaccount_gateway' );

	function add_managemore_vantiv_gateway( $methods ) {
		$methods[] = 'WC_Gateway_ManageMore_Vantiv';
		return $methods;
	}

	function add_managemore_cayan_gateway( $methods ) {
		$methods[] = 'WC_Gateway_ManageMore_Cayan';
		return $methods;
	}

	function add_managemore_onaccount_gateway( $methods ) {
		$methods[] = 'WC_Gateway_ManageMore_Onaccount';
		return $methods;
	}


}

//Show "Woo not Installed" Error Message
function econnect_woocommerce_error(){
      ?>
      <div class="notice notice-error is-dismissible">
      	   <p>
           <h3><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;ERROR!</h3>
           The ManageMore eConnect Plugin requires WooCommerce!
           <br>
           <br>
           <a class="button-secondary" href="<?php echo admin_url('plugins.php') ?>">Plugins</a>
          </p>
      </div>
      <?php
}


//Setup Settings Page
function managemore_econnect_settings_page() {

	 // If the WC_Product class doesn't exist
	 // it means WooCommerce is not installed on the site
	 // so give an error, and do nothing else.
 	 if ( ! class_exists( 'WC_Product' ) ) {
 	 	  econnect_woocommerce_error();
 		  return;
   }
   
   //Set Defaults
   if (get_option('econnect_packetsize') ==''){
       update_option('econnect_packetsize',20);
   }

   if (get_option('econnect_connect_timeout') ==''){
       update_option('econnect_connect_timeout',5);
   }

   if (get_option('econnect_packet_timeout') ==''){
       update_option('econnect_packet_timeout',15);
   }
   
   
?>
<div class="wrap">
<h1><?php echo '<img src="' . plugins_url( 'images/mmpill.png', __FILE__ ) . '" > '; ?>ManageMore eConnect - Plugin Settings</h1>


<?php settings_errors(); ?>

<form method="post" action="options.php">
    <?php settings_fields( 'managemore-econnect-settings-group' ); ?>
    <?php do_settings_sections( 'managemore-econnect-settings-group' ); ?>
    <br>
    <br>
    <h2>Main Settings</h2>
    <table class="form-table">

        <tr valign="top">
        <th scope="row">Disable eConnect</th>
        <td><input type="checkbox" name="econnect_disable" value="1" <?php checked( get_option('econnect_disable'), 1 ); ?> />Disable eConnect Communication</td>
        <td><i>Check to disable packet communication to the ManageMore eConnect Server on successful order.  Credit Card will still process.</i></td>
        </tr>


        <tr valign="top">
        <th scope="row">eConnect IP Address</th>
        <td><input type="text" name="econnect_ip_address" value="<?php echo esc_attr( get_option('econnect_ip_address') ); ?>" /></td>
        <td><i>The IP Address of your eConnect Server</i></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">eConnect Port</th>
        <td><input type="text" name="econnect_port" value="<?php echo esc_attr( get_option('econnect_port') ); ?>" /></td>
        <td><i>The port on which your eConnect Server is listening</i></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">eConnect Password</th>
        <td><input type="text" name="econnect_password" value="<?php echo esc_attr( get_option('econnect_password') ); ?>" /></td>
        <td><i>The password required to send a packet to your eConnect Server</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Customer Pricing</th>
        <td><input type="checkbox" name="econnect_customer_pricing" value="1" <?php checked( get_option('econnect_customer_pricing'), 1 ); ?> />Allow Customer-Specific Pricing</td>
        <td><i>Check to allow customer-specific pricing for logged in users based on the customer's ManageMore Price Level</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Transaction Type</th>
        <td>
            <select name="econnect_transtype" >
                <option value="order" <?php selected( get_option('econnect_transtype'), 'order' ); ?> >Sales Order</option>
                <option value="invoice" <?php selected( get_option('econnect_transtype'), 'invoice' ); ?> >Invoice</option>
            </select>
        </td>
        <td><i>Type of transaction to create in ManageMore</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Check Stock Before Adding/Updating Cart</th>
        <td>
           <select name="econnect_checkstock_cart" >
                <option value="no" <?php selected( get_option('econnect_checkstock_cart'), 'no' ); ?> >No</option>
                <option value="yes" <?php selected( get_option('econnect_checkstock_cart'), 'yes' ); ?> >At Order Location</option>
                <option value="all" <?php selected( get_option('econnect_checkstock_cart'), 'all' ); ?> >At All Locations</option>
            </select>
        </td>
        <td><i>Check ManageMore stock before adding/updating item.  Do not allow the item to be added to cart or updated for more than quantity available.</i></td>
        </tr>


        <tr valign="top">
        <th scope="row">Check Stock Before Processing</th>
        <td>
           <select name="econnect_checkstock" >
                <option value="no" <?php selected( get_option('econnect_checkstock'), 'no' ); ?> >No</option>
                <option value="yes" <?php selected( get_option('econnect_checkstock'), 'yes' ); ?> >At Order Location</option>
                <option value="all" <?php selected( get_option('econnect_checkstock'), 'all' ); ?> >At All Locations</option>
            </select>
        </td>
        <td><i>Check ManageMore stock before submitting order.  Do not allow the order to be placed if one or more items are out of stock.</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Import Related Items As</th>
        <td>
           <select name="econnect_related_method" >
                <option value="cross" <?php selected( get_option('econnect_related_method'), 'cross' ); ?> >Cross-sells</option>
                <option value="up" <?php selected( get_option('econnect_related_method'), 'up' ); ?> >Up-sells</option>
            </select>
        </td>
        <td><i>Choose whether ManageMore Web Related Items will be imported into the database as Cross-sells or Up-sells.</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Real-Time Status</th>
        <td><input type="checkbox" name="econnect_check_status" value="1" <?php checked( get_option('econnect_check_status'), 1 ); ?> />Allow Real-Time Status Information</td>
        <td><i>Check to allow automatic query of order status and tracking information when viewing order </i></td>
        </tr>

    </table>
    <br>
    <br>
    <br>
    <h2>Advanced Settings</h2>

    <table class="form-table">
        <tr valign="top">
        <th scope="row">Override Location</th>
        <td><input type="text" name="econnect_location" value="<?php echo esc_attr( get_option('econnect_location') ); ?>" /></td>
        <td><i>Enter the override ManageMore location for this site</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Packet Size</th>
        <td><input type="number" min="1" max="100" name="econnect_packetsize" value="<?php echo esc_attr( get_option('econnect_packetsize') ); ?>" /></td>
        <td><i>Enter the number of items to process per cycle (default = 20)</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Connection Timeout</th>
        <td><input type="number" min="5" max="60" name="econnect_connect_timeout" value="<?php echo esc_attr( get_option('econnect_connect_timeout') ); ?>" /></td>
        <td><i>Enter the number seconds to wait for an eConnect Connection (default = 5)</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Communication Timeout</th>
        <td><input type="number" min="10" max="60" name="econnect_packet_timeout" value="<?php echo esc_attr( get_option('econnect_packet_timeout') ); ?>" /></td>
        <td><i>Enter the number seconds to wait for an eConnect Communication (default = 15)</i></td>
        </tr>


        <tr valign="top">
        <th scope="row">Default Payment Gateway</th>
        <td>
            <select name="econnect_default_gateway" >
                <option value="cayan" <?php selected( get_option('econnect_default_gateway'), 'cayan' ); ?> >Cayan</option>
                <option value="vantiv" <?php selected( get_option('econnect_default_gateway'), 'vantiv' ); ?> >Vantiv</option>
            </select>
        </td>
        <td><i>Default Payment Gateway used for processing transactions</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">eConnect SSL/TLS</th>
        <td><input type="checkbox" name="econnect_ssl" value="1" <?php checked( get_option('econnect_ssl'), 1 ); ?> />Use SSL/TLS for communication with eConnect</td>
        <td><i>Check to use SSL/TLS encryption when sending packets (non-sensitive) to and from your eConnect Server.</i></td>
        </tr>

        <tr valign="top">
        <th scope="row">CRM Enabled</th>
        <td><input type="checkbox" name="econnect_crm_enabled" value="1" <?php checked( get_option('econnect_crm_enabled'), 1 ); ?> />Allow CRM Functions</td>
        <td><i>Check to allow CRM Functionality on your site. (e.g. View Balance, Account History, Make Payment etc.) <b>Requires the eConnect CRM Module</b></i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Auto Register</th>
        <td><input type="checkbox" name="econnect_auto_register" value="1" <?php checked( get_option('econnect_auto_register'), 1 ); ?> />Automatically Register CRM User</td>
        <td><i>Check to automatically register the user in ManageMore CRM upon first purchase. <b>Requires the eConnect CRM Module</b></i></td>
        </tr>

        <tr valign="top">
        <th scope="row">Allow Statements</th>
        <td><input type="checkbox" name="econnect_allow_statements" value="1" <?php checked( get_option('econnect_allow_statements'), 1 ); ?> />Allow Recurring Statement History</td>
        <td><i>Check to allow customers to view history of recurring statements that have been generated.  Recurring statements must be active in ManageMore. <b>Requires the eConnect CRM Module</b></i></td>
        </tr>

    </table>
    
    <?php 
       submit_button(); 
    ?>

    <br>
    <br>
    See Also:<br>
    <?php echo   '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_managemore_cayan' ) . '">Cayan Gateway Settings</a><br>' .
                 '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_managemore_vantiv' ) . '">Vantiv Gateway Settings</a><br>' .
                 '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_managemore_onaccount' ) . '">On Account Settings</a><br>' .
                 '<a href="' . admin_url( 'admin.php?page=econnect-test' ) . '">Test Connection</a>' ?>

</form>

</div>
<?php 
} 


//Setup Import Page
function managemore_econnect_import_page() {

	 // If the WC_Product class doesn't exist
	 // it means WooCommerce is not installed on the site
	 // so give an error, and do nothing else.
 	 if ( ! class_exists( 'WC_Product' ) ) {
 	 	  econnect_woocommerce_error();
 		  return;
   }


  // Check the stage we are in, based on the button we are processing
  $import_start=false;  //flag to indicate actual import is beginning, signalling prep to be done (i.e., product cleanup)
  $econnect_quantity_requested = (get_option('econnect_packetsize') > 0 ) ? get_option('econnect_packetsize') : 20; 
  
  if (isset($_POST['import_button']) && check_admin_referer('import_button_clicked')) {
    $import_stage=1;
    $econnect_check_total=true;
    $import_response = import_button_action($econnect_check_total,$import_start);
    if (strtoupper(substr($import_response,0,5)) == 'ERROR'){
      //error
      $import_stage = 4;
      $import_error_message = $import_response;
    }
    else {
      $econnect_quantity_total = $import_response;
      $econnect_quantity_start = 1;
    }
  }
  else {
    if (isset($_POST['import_confirm']) && check_admin_referer('confirm_button_clicked')) {
       $import_stage=2;   //Processing
       $import_start=true;
    }
    elseif (isset($_POST['import_processing']) && check_admin_referer('confirm_processing')) {
       $import_stage=2;   //Processing
    }
    else {
       //Not Processing
       $import_stage=0;
    }
    if ($import_stage==2) { 
       //Processing
       $econnect_check_total=false;
       $import_response = import_button_action($econnect_check_total,$import_start);
       if (strtoupper(substr($import_response,0,5)) == 'ERROR'){
         //error
         $import_stage = 4;
         $import_error_message = $import_response;
       }
       else {
         $econnect_quantity_total = $_POST['QuantityTotal'];
         $econnect_quantity_returned = $import_response;  
         $econnect_last_quantity_start = $_POST['QuantityStart'];
         $econnect_quantity_start = $econnect_last_quantity_start + $econnect_quantity_returned;
         $econnect_quantity_processed = $econnect_last_quantity_start + $econnect_quantity_returned - 1 ;
         if ($econnect_quantity_processed >= $econnect_quantity_total){
            //Done
            econnect_check_related_items();
            //Add hook for end of import
            do_action('econnect_end_of_product_import');
            $import_stage=3;   //Done
         }
       }
    
    
    }
    
    
  }

?>
<div class="wrap">
<h1><?php echo '<img src="' . plugins_url( 'images/mmpill.png', __FILE__ ) . '" > '; ?>ManageMore eConnect - Import Products</h1>
<?php switch($import_stage){
      case "1": {
         //Import Ready, show stats, confirmation form
         
         //If removing products, show warning notice
         if ($_POST['RemoveAll']==1) {
         ?>
         <div class="notice notice-warning is-dismissible">
         	   <p>
              <h3><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;WARNING<br><br>All Products, Product Categories and related records will be DELETED prior to import!<br>This will deactivate any menu items and links to the products and categories.</h3>
             </p>
         </div>
         <?php
          	
          	
         }
?>
<h3>Import Ready</h3>
Import Type: <b><?php echo $_POST['ImportType']; ?> </b><br>
Items to Import: <b><?php echo $econnect_quantity_total; ?></b><br>
<form action="admin.php?page=econnect-import" method="post">
  <input type="hidden" name="WebProfile" value="<?php ECHO $_POST["WebProfile"] ?>">
  <input type="hidden" name="ImportType" value="<?php ECHO $_POST['ImportType'] ?>">
  <input type="hidden" name="ImportStart" value="1">
  <input type="hidden" name="Category" value="<?php ECHO $_POST['Category'] ?>">
  <input type="hidden" name="Manufacturer" value="<?php ECHO $_POST['Department'] ?>">
  <input type="hidden" name="Department" value="<?php ECHO $_POST['Manufacturer'] ?>">
  <input type="hidden" name="SpecificSKU" value="<?php ECHO $_POST["SpecificSKU"] ?>">
  <input type="hidden" name="UpdateDate" value="<?php ECHO $_POST["UpdateDate"] ?>">
  <input type="hidden" name="UseDescription" value="<?php ECHO $_POST['UseDescription'] ?>">
  <input type="hidden" name="RemoveAll" value="<?php ECHO $_POST['RemoveAll'] ?>">
  <input type="hidden" name="QuantityTotal" value="<?php ECHO $econnect_quantity_total ?>">
  <input type="hidden" name="QuantityStart" value="<?php ECHO $econnect_quantity_start ?>">
  <input type="hidden" name="QuantityRequested" value="<?php ECHO $econnect_quantity_requested ?>">
  <input type="hidden" value="true" name="import_confirm" />
  <?php 
  // this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces
  wp_nonce_field('confirm_button_clicked');
  ?>
  <br>
  <br>
  <button type="submit" class="button-primary">Confirm Import</button>
  <a href="<?php echo admin_url( 'admin.php?page=econnect-import' ) ?>" class="button-secondary">Cancel</a>
</form>
<?php
         break;
      }
      case "2" : {
          //Import is processing.  Show auto-posting "progress" form, and cancel form
?>
<h3>Processing Import ...</h3>
<img src="<?php echo plugins_url( 'images/spinner3.gif', __FILE__ ) ?>" ><br><br>
Items Imported: <b><?php echo $econnect_quantity_processed . ' of ' . $econnect_quantity_total; ?></b><br>
<body onload="document.frm1.submit()">
<form action="admin.php?page=econnect-import" name="frm1" method="post">
  <input type="hidden" name="WebProfile" value="<?php ECHO $_POST["WebProfile"] ?>">
  <input type="hidden" name="ImportType" value="<?php ECHO $_POST['ImportType'] ?>">
  <input type="hidden" name="ImportStart" value="1">
  <input type="hidden" name="Category" value="<?php ECHO $_POST['Category'] ?>">
  <input type="hidden" name="Manufacturer" value="<?php ECHO $_POST['Department'] ?>">
  <input type="hidden" name="Department" value="<?php ECHO $_POST['Manufacturer'] ?>">
  <input type="hidden" name="SpecificSKU" value="<?php ECHO $_POST["SpecificSKU"] ?>">
  <input type="hidden" name="UpdateDate" value="<?php ECHO $_POST["UpdateDate"] ?>">
  <input type="hidden" name="UseDescription" value="<?php ECHO $_POST['UseDescription'] ?>">
  <input type="hidden" name="RemoveAll" value="<?php ECHO $_POST['RemoveAll'] ?>">
  <input type="hidden" name="QuantityTotal" value="<?php ECHO $econnect_quantity_total ?>">
  <input type="hidden" name="QuantityStart" value="<?php ECHO $econnect_quantity_start ?>">
  <input type="hidden" name="QuantityRequested" value="<?php ECHO $econnect_quantity_requested ?>">
  <input type="hidden" value="true" name="import_processing" />
  <?php 
  // this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces
  wp_nonce_field('confirm_processing');
  
  ?>
</form>
<form action="admin.php?page=econnect-import" name="frm2" method="post">
  <input type="hidden" value="true" name="import_cancel" />
  <?php 
  // this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces
  wp_nonce_field('cancel_button_clicked');
  ?>
  <?php submit_button('Cancel'); ?>
</form>
</body>
<?php         
         break;
      }
      case "3" : {
?>
<div class="notice notice-success is-dismissible">
     <h3><p><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;Import Complete</h3>
      You have successfully imported <?php echo $econnect_quantity_processed ?> items.</p>
</div>
<h3>Import Complete</h3>
Items Imported: <b><?php echo $econnect_quantity_processed . ' of ' . $econnect_quantity_total; ?></b><br>
<br>
<a href="<?php echo admin_url( 'edit.php?post_type=product' ) ?>">Product List</a><br>
<a href="<?php echo admin_url( 'admin.php?page=econnect-import' ) ?>">New Import</a>

<?php         
         break;
      }
      case "4" : {
?>
<div class="notice notice-error is-dismissible">
   <h3><p><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;Import Error</h3>
   There was an error importing your inventory from ManageMore.<br>
   <br>
   <?php echo $import_error_message ?></p>
</div>
<br>
<a href="<?php echo admin_url( 'admin.php?page=econnect-import' ) ?>">New Import</a><br>
<a href="<?php echo admin_url( 'admin.php?page=econnect-settings' ) ?>">Plugin Settings</a>
<?php         
         break;
      }
      default: {
         //Show Import Settings Form as default
?>
<h3>Import Settings:</h3>
<?php if (get_option('econnect_ip_address')=='') { ?>
         <div class="notice notice-warning is-dismissible">
         	   <p>
              <h3><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;WARNING: You have not setup the ManageMore eConnect Plugin properly.</h3>
              <br>
              <br>
              <a class="button-secondary" href="<?php echo admin_url('admin.php?page=econnect-settings') ?>">Plugin Settings</a>
             </p>
         </div>
<?php } else { ?>
         <div class="notice notice-info is-dismissible">
         	   <p>
              <h4><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;You will need to upload your full size images BEFORE importing.</h4>
              <br>
              <a class="button-secondary" href="<?php echo admin_url('media-new.php') ?>">Upload Images</a>
             </p>
         </div>

<?php } ?>
<form action="admin.php?page=econnect-import" method="post">
<?php 
  // this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces
  wp_nonce_field('import_button_clicked');
?>
  <table class="form-table">
      <tr>
        <th scope="row">Web Profile:</th>
        <td><input type="text" name="WebProfile" size="30" value=""><i> Leave blank for all profiles.</i></td>
      </tr>
      <tr>
         <td><input type="radio" name="ImportType" value="Full Import" checked="checked"> Full Import</td>
         <td><i>This will import all items from ManageMore and add them to the inventory. The filters below do not apply to this import type. This process will also deactivate any items which are currently in inventory and not imported. It will also refresh the categories and attributes.</i></td>
      </tr>
      <tr>
        <td></td>
        <td><input type="checkbox" name="RemoveAll" value="1" />Remove all products and related tables before importing</td>
      </tr>
      <tr>
        <td><input type="radio" name="ImportType" value="Add/Update"> Add/Update</td>
        <td><i>This will import items from ManageMore and add them to the current inventory. If the SKU already exists in inventory, it is updated. This process will add the related categories and/or attributes, if necessary.</i></td>
      </tr>
      <tr>
         <td></td>
         <td><b>Add/Update Filters</b><br>
             <i>For the filters below, you must provide a valid ManageMore code value. Leave filter(s) blank for all items.</i>
         </td>
      </tr>
      <tr>
         <td></td>
         <td>
             <table class="form-table">
                <tr>
                  <td>Category Id:</td>
                  <td><input type="text" name="Category" size="20" maxlen="30"></td>
                </tr>
                <tr>
                  <td>Manufacturer Id:</td>
                  <td><input type="text" name="Manufacturer" size="20" maxlen="30"></td>
                </tr>
                <tr>
                  <td>Department Id:</td>
                  <td><input type="text" name="Department" size="20" maxlen="30"></td>
                </tr>
                <tr>
                  <td>Specific SKU:</td>
                  <td><input type="text" name="SpecificSKU" size="20" maxlen="30"></td>
                </tr>
                <tr>
                  <td>Added/Changed in ManageMore on or after:</td>
                  <td><input type="text" name="UpdateDate" size="20" maxlen="30"><i> e.g., 12/18/2009</i></td>
                </tr>
                <tr>
                  <td colspan="2"><input type="checkbox" name="UseDescription" value="1" > Use ManageMore description for filters (case sensitive)</td>
                </tr>
             </table>      
         </td>
      </tr>
      <tr>
         <td colspan="2"><i>Note: Unless specified in your server, items must have the "Post SKU to Internet" check box selected in ManageMore</i></td>
      </tr>
  </table>
  <input type="hidden" value="true" name="import_button" />
  <?php submit_button('Begin Import'); ?>
</form>
<?php }} //End switch 

?>

</div>

<?php   

}


//Setup Test Page
function managemore_econnect_test_page() {

	 // If the WC_Product class doesn't exist
	 // it means WooCommerce is not installed on the site
	 // so give an error, and do nothing else.
 	 if ( ! class_exists( 'WC_Product' ) ) {
 	 	  econnect_woocommerce_error();
 		  return;
   }


?>
<div class="wrap">
<h1><?php echo '<img src="' . plugins_url( 'images/mmpill.png', __FILE__ ) . '" > '; ?>ManageMore eConnect - Test Connection</h1>
<?php 
  // Check whether the test button has been pressed AND also check the nonce
  if (isset($_POST['test_button']) && check_admin_referer('test_button_clicked')) {
    // the button has been pressed AND we've passed the security check
    test_button_action();
  }

  $host = esc_attr( get_option('econnect_ip_address') );
  echo '<br><b>Click below to send a test packet to your eConnect Server at:</b> <i>' . $host . '</i>' ;

?>
<form action="admin.php?page=econnect-test" method="post">
<?php 
  // this is a WordPress security feature - see: https://codex.wordpress.org/WordPress_Nonces
  wp_nonce_field('test_button_clicked');

?>
  <input type="hidden" value="true" name="test_button" />
  <?php submit_button('Test Connection'); ?>
</form>
</div>

<?php   
   
}


/* test_button_action()
   --------------------------------
   This function is used to send a test packet to the eConnect Server
*/
function test_button_action() {

   global $econnect_request_packet, $econnect_response_packet, $econnect_error;



   $t=time();
   $current_user = wp_get_current_user();
      
   $econnect_request_packet = "9999|WID=" .  $current_user->ID . 
           "|PPW=" . esc_attr( get_option('econnect_password') ) .
           "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);

   econnect_sendpacket();

   if ($econnect_error != "") {
         ?>
         <div class="notice notice-error is-dismissible">
         	   <p>
              <h3><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;Test Error</h3>
               There was an error testing the connection to your ManageMore eConnect Server.<br>
               <br>
               <b><?php echo $econnect_error ?></b><br>
               <br>
               <a href="<?php echo admin_url( 'admin.php?page=econnect-settings' ) ?>">Plugin Settings</a>
             </p>
         </div>
         <?php

   }
   else {
         ?>
         <div class="notice notice-success is-dismissible">
         	   <p>
              <h3><img src="<?php echo plugins_url('/images/mmlogo.png', __FILE__ ) ?>" > &nbsp;&nbsp;Test Successful!</h3>
             </p>
         </div>
         <?php
   }
   
   echo '<b>'. $econnect_test_response .'</b><br><br>'; 


}  

/* import_button_action()
   --------------------------------
   This function processes the import
*/
function import_button_action($econnect_check_total,$import_start) {   

	 // Inlcude the econnect functions
	 include_once( 'managemore-econnect-functions.php' );

   if($_POST['ImportType']== 'Full Import'){
   	     if ($import_start == true){
   	     	   $econnect_remove_inventory = $_POST['RemoveAll']; 
             econnect_fullimportprep($econnect_remove_inventory);
   	     }
         $econnect_inventory_request = array( 
             'WebProfile' => $_POST['WebProfile'],
             'QuantityRequested' => (($econnect_check_total == true) ? 1 : $_POST['QuantityRequested']),
             'QuantityStart' => (($econnect_check_total == true) ? 1 : $_POST['QuantityStart'])
             );
   }
   else {
      //Add/Update
      if($_POST['UseDescription']=='1'){
         //Descriptions used for filters
         $econnect_inventory_request = array( 
             'WebProfile' => $_POST['WebProfile'],
             'SKU' => $_POST['SpecificSKU'],
             'CategoryDesc' => $_POST['Category'],
             'DepartmentDesc' => $_POST['Department'],
             'ManufacturerName' => $_POST['Manufacturer'],
             'UpdateDate' => $_POST['UpdateDate'],
             'QuantityRequested' => (($econnect_check_total == true) ? 1 : $_POST['QuantityRequested']),
             'QuantityStart' => (($econnect_check_total == true) ? 1 : $_POST['QuantityStart'])
             );
      }
      else {
         //Ids used for filters
         $econnect_inventory_request = array( 
             'WebProfile' => $_POST['WebProfile'],
             'SKU' => strtoupper($_POST['SpecificSKU']),
             'CategoryId' => strtoupper($_POST['Category']),
             'DepartmentId' => strtoupper($_POST['Department']),
             'ManufacturerId' => strtoupper($_POST['Manufacturer']),
             'UpdateDate' => $_POST['UpdateDate'],
             'QuantityRequested' => (($econnect_check_total == true) ? 1 : $_POST['QuantityRequested']),
             'QuantityStart' => (($econnect_check_total == true) ? 1 : $_POST['QuantityStart'])
             );
      }
   }
   $response = econnect_sendinventorypacket($econnect_inventory_request,$econnect_check_total);
   return $response;

}  



function econnect_create_prospect($prospect_array = array()) {

   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

   //ensure packet array contains web id, prospect status code, packet password, date, time
   $geo = new WC_Geolocation();
   $ip_address = $geo->get_ip_address();
   $user_agent = $_SERVER['HTTP_USER_AGENT'];
   $prospect_array['WID']=time();
   $prospect_array['PS']='1';
   $prospect_array['PPW']=esc_attr( get_option('econnect_password') );
   $prospect_array['BUA']=$user_agent;
   $prospect_array['RIP']=$ip_address;
   $prospect_array['D']=date("m/d/y",$t);
   $prospect_array['T']=date("h:iA",$t);
      
   //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
   $econnect_request_packet = '';
   foreach ($prospect_array as $array_key => $array_value ) {
      	 $econnect_request_packet .= '|'. $array_key . '=' . $array_value ;
   } 
   $econnect_request_packet = '1006' . $econnect_request_packet . chr(4);  //add packet type, end with chr(4)


   econnect_sendpacket();

   return $econnect_error;

}  




/* econnect_sendpacket()
   --------------------------------
   This function is the main function used to send packets to the eConnect Server,
   and receive the response.  It checks for errors in sending, and checks any error message
   returned in the packet. Sets global variables $econnect_response_packet and $econnect_error
*/
function econnect_sendpacket() {

   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );
      $ssl = esc_attr( get_option('econnect_ssl'));
      $connect_timeout = get_option('econnect_connect_timeout');
      $packet_timeout = get_option('econnect_packet_timeout');
      //make sure timeouts not less than minimum
      if ($connect_timeout < 5) $connect_timeout = 5;
      if ($packet_timeout < 10) $packet_timeout = 10;
      $econnect_error = "";
      $econnect_response_packet = "";

      //connect to server 
      if ($ssl == '1') {
         $socket = @fsockopen("ssl://".$host, $port, $errno, $errstr, $connect_timeout);
      }
      else {
         $socket = @fsockopen($host, $port, $errno, $errstr, $connect_timeout);
      }

      if (is_resource($socket)) {
         
      
          fwrite($socket, $econnect_request_packet);
          stream_set_timeout($socket, $packet_timeout);

      
          // Get Response
          do
          { 
          $out = fgets($socket,2);
          $info = stream_get_meta_data($socket);
          $econnect_response_packet .= $out;
          $i++;
          } 
          while($out != chr(4) && $i<1000000 && $info['timed_out']==false);
          fclose($socket);

          if ($econnect_response_packet != "") {
            if (strpos($econnect_response_packet,"|S=1") === false ) {
               //Success Flag not returned

               //Get Error Message returned
               $errpos = strpos($econnect_response_packet,"|E=");
               $errend = strpos($econnect_response_packet,"|",$errpos+1);
               $errmsg = substr($econnect_response_packet,$errpos+3,$errend-$errpos-3);

               $econnect_error = "Error - " . $errmsg;
            }
          }
          else {
            //Invalid response
            $econnect_error = ($info['timed_out']==true) ? "Error - Server Timeout." : "Error - Invalid response received from server.";
          }
      
      } else {
         //Socket connection not made
         $econnect_error = "Error - Server connection could not be made.";
      }
      
      
      
      
      return;
 


   
}


/* econnect_checkstock_cart()
   --------------------------------
   This function is used to send a stock check packet (1021) to the eConnect Server for one item.
   It requires $sku and $quantity array to be passed, which contains all the elements
   necessary to build the request packet 
   Return Value: null if ok, or item which is out of stock. (eConnect returns as error)
*/
function econnect_checkstock_cart($sku,$quantity,$description) {
   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

   $geo = new WC_Geolocation();
   $ip_address = $geo->get_ip_address();
   $user_agent = $_SERVER['HTTP_USER_AGENT'];


   $t=time();
   $current_user = wp_get_current_user();
   $alllocations = (get_option('econnect_checkstock_cart')=='all' ) ? '1' : '0' ;
   $location = (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' ;
   
   $econnect_request_packet = "1021|WID=" . $current_user->ID .
           "|ALOC=" . $alllocations .
           "|LOC=" . $location .
           "|SKU001=" . $sku .
           "|SKUQ001=" . $quantity .
           "|SKUD001=" . $description .
           "|PPW=" . esc_attr( get_option('econnect_password') ) .
           "|BUA=" . $user_agent .
           "|RIP=" . $ip_address .
           "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);

     

   econnect_sendpacket();
   $econnect_response = $econnect_error ;

   return $econnect_response; 

}



//Send E-mail to admin if there is an eConnect issue
function econnect_send_admin_email($econnect_error,$error_location = 'Sending Packet'){
	
	    $site = get_bloginfo('name');
    	$to = get_bloginfo('admin_email');
      $subject = 'eConnect Error - '.$site;
      $message = 'There was an eConnect error encountered on site: '.$site.'  The error was: '.$econnect_error.' The error happened during: '.$error_location;
    	wp_mail( $to, $subject, $message );
	
}




?>