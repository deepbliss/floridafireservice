<?php
/*
   These are the functions used for the eConnect plugin
*/


/* econnect_fullimportprep()
   --------------------------------
   This function is used to prepare the Zen Cart inventory database for a full import.
   1. Delete/Inactivate records.
      Either:
      a) Deletes all inventory and related records (if user chose to do so) or
      b) Marks all products and categories as inactive, so that if they are not imported,
         then they will not show in the site.
   2. Import all categories from ManageMore, so that all parent categories will be present
*/
function econnect_fullimportprep($econnect_remove_inventory) {


  //define word press database object
  global $wpdb;
  
   
  // 1. Delete/Inactivate records.
  if ($econnect_remove_inventory == "1") {
  
     $prefix = $wpdb->prefix;
  
     //Remove All Product Categories
     $wpdb->query("DELETE a,c FROM $wpdb->terms AS a
              LEFT JOIN $wpdb->term_taxonomy AS c ON a.term_id = c.term_id
              LEFT JOIN $wpdb->term_relationships AS b ON b.term_taxonomy_id = c.term_taxonomy_id
              WHERE c.taxonomy = 'product_cat'");

              
              
     //Remove All Attribute Types
     $wpdb->query("DELETE FROM " . $prefix . "woocommerce_attribute_taxonomies");
      
              
     //Remove All Attribute Values
     $wpdb->query("DELETE FROM $wpdb->terms WHERE term_id IN 
              (SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy LIKE 'pa_%')");
     $wpdb->query("DELETE FROM $wpdb->term_taxonomy WHERE taxonomy LIKE 'pa_%'");
     $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id not IN
              (SELECT term_taxonomy_id FROM $wpdb->term_taxonomy)");

     //Remove All Products, Variations
     $wpdb->query("DELETE FROM $wpdb->term_relationships WHERE object_id IN 
              (SELECT ID FROM $wpdb->posts WHERE post_type IN ('product','product_variation'))");
     $wpdb->query("DELETE FROM $wpdb->postmeta WHERE post_id IN 
              (SELECT ID FROM $wpdb->posts WHERE post_type IN ('product','product_variation'))");
     $wpdb->query("DELETE FROM $wpdb->posts WHERE post_type IN ('product','product_variation')");

     //Clean up Orphaned PostMeta
     $wpdb->query("DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL");


     //Add hook for after product cleanup
     do_action('econnect_after_deleting_products');
    



  }
  else {

     //Deactivate (trash) All Products, Variations
     $query = "UPDATE $wpdb->posts SET post_status=%s WHERE post_type IN ('product','product_variation')";
     $wpdb->query($wpdb->prepare($query,"trash"));
   


  }


  //Always import All Categories when doing a Full Import, for parent category purposes.
  econnect_import_categories();
  


}



function econnect_check_related_items() {
	  global $wpdb;
	
	  //Check Web related items, to update 
	  $results=$wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE meta_key = '_econnect_related_skus' AND post_id IN 
              (SELECT ID FROM $wpdb->posts WHERE post_type ='product')");
              
    foreach($results as $postmeta){
        if ($postmeta->meta_value != ''){
        	  $skus = explode(',', $postmeta->meta_value);
        	  $related_ids = array();
        	  foreach ($skus as $sku) {
               $query = "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1";
               $post_id = $wpdb->get_var( $wpdb->prepare( $query, $sku ) );
               if ($post_id){
               	  array_push($related_ids,$post_id);
               }
        	  	 
        	  } 
            if (get_option('econnect_related_method') == 'up') {
        	     update_post_meta( $postmeta->post_id, '_upsell_ids',$related_ids);
        	  }
        	  else {
        	     update_post_meta( $postmeta->post_id, '_crosssell_ids',$related_ids);
        	  }


        	  
        }
    }
    
    //Remove temporary meta value 
	  $wpdb->query("DELETE FROM $wpdb->postmeta WHERE meta_key = '_econnect_related_skus' AND post_id IN 
              (SELECT ID FROM $wpdb->posts WHERE post_type='product')");
      
      
              
    

	
	
}

/* econnect_processitem()
   --------------------------------
   This function is used to process an individual SKU Item being imported.
*/

function econnect_processitem($econnect_inventory_item,$econnect_related_ids) {

  //Declare wordpress database global object, and woocommerce globals
  global $wpdb, $woocommerce;


  //First Check whether product exists:
  $sku =  $econnect_inventory_item['SKU'];
  $query = "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1";
  $post_id = $wpdb->get_var( $wpdb->prepare( $query, $sku ) );
  $econnect_matrix_child = false;

  //Check if matrix child.  If so, set flag and get parent SKU post id
  if ($econnect_inventory_item['MatrixSKU'] != ''){
    $econnect_matrix_child = true;
    $parent_sku =  $econnect_inventory_item['MatrixSKU'];
    $query = "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1";
    $parent_post_id = $wpdb->get_var( $wpdb->prepare( $query, $parent_sku ) );
  } 	


  
  if ($post_id) {
  	
     //Update Product:
     
     //Define Post array
     if ($econnect_matrix_child) {
        $post = array(
            'ID' => $post_id,
            'post_author' => '',
            'post_content' => $econnect_inventory_item['Notes'] ,
            'post_status' => "publish",
            'post_title' => esc_attr($econnect_inventory_item['Description']),             //Title of product
            'post_parent' => $parent_post_id,
            'post_type' => "product_variation",
        );
     }
     else {
        $post = array(
            'ID' => $post_id,
            'post_author' => '',
            'post_content' => $econnect_inventory_item['Notes'] ,
            'post_status' => "publish",
            'post_title' => esc_attr($econnect_inventory_item['Description']),             //Title of product
            'post_parent' => '',
            'post_type' => "product",
        );
      }
     
     //Update the post
     wp_update_post( $post, $wp_error );
     $econnect_item_created=false;

  	
  	
  }
  else {
   
     // Add Product:
     
     //Define Post array
     if ($econnect_matrix_child) {
        $post = array(
            'post_author' => '',
            'post_content' => $econnect_inventory_item['Notes'] ,                          //HTML content: Do not use esc_attr() on this field
            'post_status' => "publish",
            'post_title' => esc_attr($econnect_inventory_item['Description']),             //Title of product
            'post_parent' => $parent_post_id,
            'post_type' => "product_variation",
        );
     }
     else {
        $post = array(
            'post_author' => '',
            'post_content' => $econnect_inventory_item['Notes'] ,                          //HTML content: Do not use esc_attr() on this field
            'post_status' => "publish",
            'post_title' => esc_attr($econnect_inventory_item['Description']),             //Title of product
            'post_parent' => '',
            'post_type' => "product",
        );
     }
      
     
     //Create the post
     $post_id = wp_insert_post( $post, $wp_error );
     if($post_id){
         $attach_id = get_post_meta($post_id, "_thumbnail_id", true);
         add_post_meta($post_id, '_thumbnail_id', $attach_id);
     }
     $econnect_item_created=true;
  }  

  //Set other properties:  Category, product type, meta values
  wp_set_object_terms( $post_id, $econnect_inventory_item['Category'], 'product_cat' );
  
  //If Matrix Parent make a variable product type
  if ($econnect_inventory_item['MatrixParent'] == '1'){
     wp_set_object_terms($post_id, 'variable', 'product_type');
     //update_post_meta( $post_id, '_product_attributes', array());   //This will get updated by the children
  }
  else {
  	
  	 if ($econnect_matrix_child == true) {
  	 	   //This is a Matrix Child
  	 	   //Check Attributes
  	 	   $available_attributes = Array();
  	 	   $i=0;
  	 	   if ($econnect_inventory_item['AttributeType1'] != ''){
  	 	   	   econnect_check_attributes($econnect_inventory_item['AttributeType1'],$econnect_inventory_item['AttributeValue1'], $post_id, $parent_post_id);
             /*
                Set "sanitized" version of names (for slug values)
                Note: We add mm- to the beginning and the attribute type to the value, to get a more unique value
             */
             $sanitize_type = 'mm-'. sanitize_title($econnect_inventory_item['AttributeType1']);
             $taxonomy_name = wc_attribute_taxonomy_name( $sanitize_type );  // pa_ prepended 
             //set attribute array for this attribute
             $available_attributes[$taxonomy_name]=Array(
             'name'=>$taxonomy_name,
             'value'=>'',
             'is_visible' => '1', 
             'is_variation' => '1',
             'is_taxonomy' => '1'
             );
  	 	   }
  	 	   if ($econnect_inventory_item['AttributeType2'] != ''){
  	 	   	   econnect_check_attributes($econnect_inventory_item['AttributeType2'],$econnect_inventory_item['AttributeValue2'], $post_id, $parent_post_id);
             /*
                Set "sanitized" version of names (for slug values)
                Note: We add mm- to the beginning and the attribute type to the value, to get a more unique value
             */
             $sanitize_type = 'mm-'. sanitize_title($econnect_inventory_item['AttributeType2']);
             $taxonomy_name = wc_attribute_taxonomy_name( $sanitize_type );  // pa_ prepended 
             //set attribute array for this attribute
             $available_attributes[$taxonomy_name]=Array(
             'name'=>$taxonomy_name,
             'value'=>'',
             'is_visible' => '1', 
             'is_variation' => '1',
             'is_taxonomy' => '1'
             );
  	 	   }
  	 	   if ($econnect_inventory_item['AttributeType3'] != ''){
  	 	   	   econnect_check_attributes($econnect_inventory_item['AttributeType3'],$econnect_inventory_item['AttributeValue3'], $post_id, $parent_post_id);
             /*
                Set "sanitized" version of names (for slug values)
                Note: We add mm- to the beginning and the attribute type to the value, to get a more unique value
             */
             $sanitize_type = 'mm-'. sanitize_title($econnect_inventory_item['AttributeType3']);
             $taxonomy_name = wc_attribute_taxonomy_name( $sanitize_type );  // pa_ prepended 
             //set attribute array for this attribute
             $available_attributes[$taxonomy_name]=Array(
             'name'=>$taxonomy_name,
             'value'=>'',
             'is_visible' => '1', 
             'is_variation' => '1',
             'is_taxonomy' => '1'
             );
  	 	   }
  	 	   $econnect_matrix_child_price = $econnect_inventory_item['PromoPrice']+$econnect_inventory_item['AttributePrice1']+$econnect_inventory_item['AttributePrice2']+$econnect_inventory_item['AttributePrice3'];

     
         update_post_meta( $parent_post_id,'_product_attributes',$available_attributes);


  	 }
     else {
     	  //Non-Matrix product
        wp_set_object_terms($post_id, 'simple', 'product_type');
        update_post_meta( $post_id, '_product_attributes', array());
     }
  }	
  
  
  //Update Common Meta Values used by WooCommerce
  update_post_meta( $post_id, '_visibility', 'visible' );
  update_post_meta( $post_id, '_stock_status', 'instock');
  update_post_meta( $post_id, '_downloadable', 'no');
  update_post_meta( $post_id, '_virtual', 'no');
  update_post_meta( $post_id, '_regular_price', $econnect_inventory_item['WebPrice'] );
  update_post_meta( $post_id, '_sale_price', $econnect_inventory_item['PromoPrice'] );
  update_post_meta( $post_id, '_purchase_note', "" );
  update_post_meta( $post_id, '_featured', (($econnect_inventory_item['WebFeaturedItem']=='1') ? "yes" : "no" ));
  update_post_meta( $post_id, '_weight', $econnect_inventory_item['ShipWeight'] );
  update_post_meta( $post_id, '_length', "" );
  update_post_meta( $post_id, '_width', "" );
  update_post_meta( $post_id, '_height', "" );
  update_post_meta( $post_id, '_sku', $econnect_inventory_item['SKU']);
  update_post_meta( $post_id, '_sale_price_dates_from', $econnect_inventory_item['PromoStartDate'] );
  update_post_meta( $post_id, '_sale_price_dates_to', $econnect_inventory_item['PromoEndDate'] );
  update_post_meta( $post_id, '_price', ($econnect_matrix_child == true) ? $econnect_matrix_child_price : $econnect_inventory_item['PromoPrice'] );
  update_post_meta( $post_id, '_sold_individually', "" );
  update_post_meta( $post_id, '_manage_stock', "no" );
  update_post_meta( $post_id, '_backorders', "no" );
  update_post_meta( $post_id, '_stock', "" );
  update_post_meta( $post_id, '_downloadable_files', '');
  update_post_meta( $post_id, '_download_limit', '');
  update_post_meta( $post_id, '_download_expiry', '');
  update_post_meta( $post_id, '_download_type', '');
  update_post_meta( $post_id, '_product_image_gallery', '');
  update_post_meta( $post_id, '_tax_status', ($econnect_inventory_item['Tax1'] == '1') ? 'taxable' : 'none');
  
  //Update Level Prices (for Customer Pricing)
  update_post_meta( $post_id, '_econnect_price_1', $econnect_inventory_item['Price1']);
  update_post_meta( $post_id, '_econnect_price_2', $econnect_inventory_item['Price2']);
  update_post_meta( $post_id, '_econnect_price_3', $econnect_inventory_item['Price3']);
  update_post_meta( $post_id, '_econnect_price_4', $econnect_inventory_item['Price4']);
  update_post_meta( $post_id, '_econnect_price_5', $econnect_inventory_item['Price5']);
  update_post_meta( $post_id, '_econnect_price_6', $econnect_inventory_item['Price6']);
  update_post_meta( $post_id, '_econnect_price_7', $econnect_inventory_item['Price7']);
  update_post_meta( $post_id, '_econnect_price_8', $econnect_inventory_item['Price8']);
  
  update_post_meta( $post_id, '_econnect_related_skus', $econnect_inventory_item['WebRelatedItems']);
  
  
 
  //Set Tags
  $tag_array = explode(',',$econnect_inventory_item['WebKeywords']);
  wp_set_object_terms($post_id,$tag_array, 'product_tag');
 
  
  
  //Check if the image was uploaded.  Get the Id to assign
  $image_name = $econnect_inventory_item['Image']; //Try Full Size Image
	$attachment = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE guid LIKE '%".  $image_name ."'" ); 
  $attach_id =  $attachment[0];

  if ( ! $attach_id){
  	  //not found: maybe they uploaded the thumbnail from MM?
      $image_name = $econnect_inventory_item['Thumbnail'];  //Try Thumbnail
	    $attachment = $wpdb->get_col("SELECT ID FROM $wpdb->posts WHERE guid LIKE '%".  $image_name ."'" ); 
      $attach_id =  $attachment[0];
  }

  update_post_meta($post_id, '_thumbnail_id', $attach_id);
  
  
  /*
     Add custom hook "econnect_after_processing_sku"
     Use this hook to do something custom to the product/variation that was just added/updated
  */
  //add hook
  do_action('econnect_after_processing_sku',$post_id,$econnect_inventory_item);
  
}


/* econnect_sendinventorypacket()
   --------------------------------
   This function is used to send an inventory query packet (1009) to the eConnect Server.
   It requires $econnect_inventory_request array to be passed, which contains all the elements
   necessary to build the request packet (e.g., filters, quantity requested, etc.)
   Return Value: string - Total number of items found/Error message
*/
function econnect_sendinventorypacket($econnect_inventory_request,$econnect_check_total) {
   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

   $t=time();
   $current_user = wp_get_current_user();
   $location = (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' ;

   $econnect_request_packet = "1009|WID=" . $current_user->ID .
           "|SKU=" . $econnect_inventory_request['SKU'] .
           "|LOC=" . $location . 
           "|WPF=" . $econnect_inventory_request['WebProfile'] .
           "|DESC=" . $econnect_inventory_request['Description'] .
           "|CATI=" . $econnect_inventory_request['CategoryId'] .
           "|CATG=" . $econnect_inventory_request['CategoryDesc'] .
           "|DPTI=" . $econnect_inventory_request['DepartmentId'] .
           "|DEPT=" . $econnect_inventory_request['DepartmentDesc'] .
           "|MANI=" . $econnect_inventory_request['ManufacturerId'] .
           "|MANN=" . $econnect_inventory_request['ManufacturerName'] .
           "|UPD=" . $econnect_inventory_request['UpdateDate'] .
           "|REQQTY=" . $econnect_inventory_request['QuantityRequested'] .
           "|STQTY=" . $econnect_inventory_request['QuantityStart'] .
           "|PPW=" . esc_attr( get_option('econnect_password') ) .
           "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);

     

   econnect_sendpacket();
   if ($econnect_error != "") {
      $econnect_response = $econnect_error ;
    }
   else {
      if ($econnect_check_total) {

         // Getting Total Quantity --------------
         $totpos = strpos($econnect_response_packet,"|TOTQTY=");
         $totend = strpos($econnect_response_packet,"|",$totpos+1);
         $totqty = substr($econnect_response_packet,$totpos+8,$totend-$totpos-8);
         if ($totqty == '0') {
            $econnect_response = 'Error - no items found to import.';
         }
         else{
            $econnect_response = $totqty ;
         }
      }
      else {
         
         //Importing -------------------------

         //Parse Response
         //a) Explode response by pipe delimiter
         $econnect_temp_array = explode("|",$econnect_response_packet);
         //b) For each item, add to packet array with Key, Value
         $econnect_packet_array = array();
         foreach ($econnect_temp_array as $econnect_value_pair) {
            $econnect_value_pair_array = explode("=",$econnect_value_pair,2);
            $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
         }
         //c) Call function to parse inventory packet into array 
         $econnect_inventory_array = econnect_parseinventorypacket($econnect_packet_array);
   
         //Process Items
         foreach ($econnect_inventory_array as $econnect_inventory_item){
   
          
            $econnect_response = econnect_processitem($econnect_inventory_item,$econnect_related_ids);
            //check for error
            if (strtoupper(substr($econnect_response,0,5)) == 'ERROR') {
               return $econnect_response; 
            }
         }

         // Get Quantity Returned
         $totpos = strpos($econnect_response_packet,"|RETQTY=");
         $totend = strpos($econnect_response_packet,"|",$totpos+1);
         $totqty = substr($econnect_response_packet,$totpos+8,$totend-$totpos-8);
         $econnect_response = $totqty ;

      } 
    }

    return $econnect_response; 

}


/* econnect_parseinventorypacket()
   --------------------------------
   This function is used convert the econnect_packet_array, which contains
   field/value pair elements, to the econnect_inventory_array, which is a
   multi-dimensional array which contains an element for each inventory item returned
   and a sub-element for each field of the item.  (e.g., $econnect_inventory_array[1]['SKU'])
   Return Value: array - $econnect_inventory_array
*/
function econnect_parseinventorypacket($econnect_packet_array) {
  
     $returned_quantity = $econnect_packet_array['RETQTY'];
     for ($i=1; $i <= $returned_quantity; $i++){
        $item = sprintf("%03s",$i); //001 - 999
        $econnect_inventory_array[$i]['SKU'] = $econnect_packet_array['SKU' . $item];
        $econnect_inventory_array[$i]['UPC'] = $econnect_packet_array['UPC' . $item];
        $econnect_inventory_array[$i]['Description'] = $econnect_packet_array['DESC' . $item];
        $econnect_inventory_array[$i]['Notes'] = $econnect_packet_array['NOTE' . $item];
        $econnect_inventory_array[$i]['Image'] = $econnect_packet_array['IMAG' . $item];
        $econnect_inventory_array[$i]['Thumbnail'] = $econnect_packet_array['THMB' . $item];
        $econnect_inventory_array[$i]['WebProfile'] = $econnect_packet_array['WPF' . $item];
        $econnect_inventory_array[$i]['WebKeywords'] = $econnect_packet_array['WKW' . $item];
        $econnect_inventory_array[$i]['WebFeaturedItem'] = $econnect_packet_array['WFI' . $item];
        $econnect_inventory_array[$i]['WebRelatedItems'] = $econnect_packet_array['WRI' . $item];
        $econnect_inventory_array[$i]['WebURL'] = $econnect_packet_array['WURL' . $item];
        $econnect_inventory_array[$i]['WebPrice'] = $econnect_packet_array['PRW' . $item];
        $econnect_inventory_array[$i]['RetailPrice'] = $econnect_packet_array['PRR' . $item];
        $econnect_inventory_array[$i]['Price1'] = $econnect_packet_array['PR1' . $item];
        $econnect_inventory_array[$i]['Price2'] = $econnect_packet_array['PR2' . $item];
        $econnect_inventory_array[$i]['Price3'] = $econnect_packet_array['PR3' . $item];
        $econnect_inventory_array[$i]['Price4'] = $econnect_packet_array['PR4' . $item];
        $econnect_inventory_array[$i]['Price5'] = $econnect_packet_array['PR5' . $item];
        $econnect_inventory_array[$i]['Price6'] = $econnect_packet_array['PR6' . $item];
        $econnect_inventory_array[$i]['Price7'] = $econnect_packet_array['PR7' . $item];
        $econnect_inventory_array[$i]['Price8'] = $econnect_packet_array['PR8' . $item];
        $econnect_inventory_array[$i]['PromoPrice'] = $econnect_packet_array['PRP' . $item];
        $econnect_inventory_array[$i]['PromoStartDate'] = ($econnect_packet_array['PRMS' . $item] != '' ) ? strtotime($econnect_packet_array['PRMS' . $item]) : '' ;
        $econnect_inventory_array[$i]['PromoEndDate'] =  ($econnect_packet_array['PRME' . $item] != '' ) ? strtotime($econnect_packet_array['PRME' . $item]) : '' ; 
        $econnect_inventory_array[$i]['Category'] = $econnect_packet_array['CATG' . $item];
        $econnect_inventory_array[$i]['CategoryId'] = $econnect_packet_array['CATI' . $item];
        $econnect_inventory_array[$i]['ParentCategory'] = $econnect_packet_array['PCTG' . $item];
        $econnect_inventory_array[$i]['ParentCategoryId'] = $econnect_packet_array['PCTI' . $item];
        $econnect_inventory_array[$i]['Department'] = $econnect_packet_array['DEPT' . $item];
        $econnect_inventory_array[$i]['DepartmentId'] = $econnect_packet_array['DPTI' . $item];
        $econnect_inventory_array[$i]['Manufacturer'] = substr($econnect_packet_array['MANN' . $item],0,30); //Zen Cart supports on 30 characters in Manufacturer Name
        $econnect_inventory_array[$i]['ManufacturerId'] = $econnect_packet_array['MANI' . $item];
        $econnect_inventory_array[$i]['Warranty'] = $econnect_packet_array['WARR' . $item];
        $econnect_inventory_array[$i]['WarrantyId'] = $econnect_packet_array['WARI' . $item];
        $econnect_inventory_array[$i]['QtyOnHand'] = $econnect_packet_array['QTY' . $item];
        $econnect_inventory_array[$i]['QtyIncrement'] = $econnect_packet_array['QTYI' . $item];
        $econnect_inventory_array[$i]['QtyPrecision'] = $econnect_packet_array['QTYP' . $item];
        $econnect_inventory_array[$i]['QtyDefault'] = $econnect_packet_array['QTYD' . $item];
        $econnect_inventory_array[$i]['QtyCommitted'] = $econnect_packet_array['QTYC' . $item];
        $econnect_inventory_array[$i]['QtyAvailable'] = $econnect_inventory_array[$i]['QtyOnHand'] - $econnect_inventory_array[$i]['QtyCommitted'];
        $econnect_inventory_array[$i]['Kind'] = $econnect_packet_array['SKND' . $item];
        $econnect_inventory_array[$i]['TaxClassId'] = $econnect_packet_array['TXCL' . $item];
        $econnect_inventory_array[$i]['Tax1'] = $econnect_packet_array['TAX1' . $item];
        $econnect_inventory_array[$i]['Tax2'] = $econnect_packet_array['TAX2' . $item];
        $econnect_inventory_array[$i]['Tax3'] = $econnect_packet_array['TAX3' . $item];
        $econnect_inventory_array[$i]['Tax4'] = $econnect_packet_array['TAX4' . $item];
        $econnect_inventory_array[$i]['Tax5'] = $econnect_packet_array['TAX5' . $item];
        $econnect_inventory_array[$i]['Tax6'] = $econnect_packet_array['TAX6' . $item];
        $econnect_inventory_array[$i]['Tax7'] = $econnect_packet_array['TAX7' . $item];
        $econnect_inventory_array[$i]['Tax8'] = $econnect_packet_array['TAX8' . $item];
        $econnect_inventory_array[$i]['UnitOfMeasure'] = $econnect_packet_array['MEAS' . $item];
        $econnect_inventory_array[$i]['MatrixParent'] = $econnect_packet_array['MPAR' . $item];
        $econnect_inventory_array[$i]['MatrixSKU'] = $econnect_packet_array['MSKU' . $item];
        $econnect_inventory_array[$i]['AttributeType1'] = $econnect_packet_array['SKAT1' . $item];
        $econnect_inventory_array[$i]['AttributeValue1'] = $econnect_packet_array['SKAV1' . $item];
        $econnect_inventory_array[$i]['AttributePrice1'] = $econnect_packet_array['SKAP1' . $item];
        $econnect_inventory_array[$i]['AttributeType2'] = $econnect_packet_array['SKAT2' . $item];
        $econnect_inventory_array[$i]['AttributeValue2'] = $econnect_packet_array['SKAV2' . $item];
        $econnect_inventory_array[$i]['AttributePrice2'] = $econnect_packet_array['SKAP2' . $item];
        $econnect_inventory_array[$i]['AttributeType3'] = $econnect_packet_array['SKAT3' . $item];
        $econnect_inventory_array[$i]['AttributeValue3'] = $econnect_packet_array['SKAV3' . $item];
        $econnect_inventory_array[$i]['AttributePrice3'] = $econnect_packet_array['SKAP3' . $item];
        $econnect_inventory_array[$i]['UDF01'] = $econnect_packet_array['UDF01' . $item];
        $econnect_inventory_array[$i]['UDF02'] = $econnect_packet_array['UDF02' . $item];
        $econnect_inventory_array[$i]['UDF03'] = $econnect_packet_array['UDF03' . $item];
        $econnect_inventory_array[$i]['UDF04'] = $econnect_packet_array['UDF04' . $item];
        $econnect_inventory_array[$i]['UDF05'] = $econnect_packet_array['UDF05' . $item];
        $econnect_inventory_array[$i]['ShipWeight'] = $econnect_packet_array['SWT' . $item];
        $econnect_inventory_array[$i]['NetWeight'] = $econnect_packet_array['NWT' . $item];
     }
     return $econnect_inventory_array;
}



/* econnect_check_attributes($attribute_type, $attribute_value)
   --------------------------------
   This function is used to check the existence of attributes.  
   It will create the attributes if they do not exist.
*/
function econnect_check_attributes($attribute_type, $attribute_value,$post_id,$parent_post_id){

   //define word press database object
   global $wpdb;
   $prefix = $wpdb->prefix;

   /*
      Set "sanitized" version of names (for slug values)
      Note: We add mm- to the beginning and the attribute type to the value, to get a more unique value
   */
   $sanitize_type = 'mm-'. sanitize_title($attribute_type);
   $sanitize_value = 'mm-'. sanitize_title($attribute_type).'-'.sanitize_title($attribute_value);

   $taxonomy_name = wc_attribute_taxonomy_name( $sanitize_type );  // pa_ prepended to value

   //If Attribute Name (e.g. 'Color', 'Size') does not exist, add it.
   $query = "SELECT * FROM ". $prefix . "woocommerce_attribute_taxonomies WHERE attribute_name=%s";
   $attribute_found = $wpdb->get_results($wpdb->prepare($query,$sanitize_type));
   if ( $attribute_found == null ) {
	      $attribute=array();
        $attribute['attribute_type']='select';
        $attribute['attribute_orderby'] = 'name';
        $attribute['attribute_public']=0;
        $attribute['attribute_label']=$attribute_type ;
        $attribute['attribute_name'] =$sanitize_type;

        $wpdb->insert( $prefix . 'woocommerce_attribute_taxonomies', $attribute );
        do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );
 
        flush_rewrite_rules();
        delete_transient( 'wc_attribute_taxonomies' );
     }

     // If Attribute Value (e.g. "Large", "Medium") does not exist within the Attribute Type, add it

     $query = "SELECT a.term_id FROM $wpdb->terms AS a LEFT JOIN $wpdb->term_taxonomy AS c 
               ON a.term_id = c.term_id WHERE a.slug = %s AND c.taxonomy = %s";
     $term = $wpdb->get_var($wpdb->prepare($query,$sanitize_value,$taxonomy_name));
     if ( $term == null ) {
    
        // add the new Attribute Value 
        $query = "INSERT INTO $wpdb->terms (name, slug) VALUES (%s, %s)";
        $wpdb->query($wpdb->prepare($query, $attribute_value, $sanitize_value)); 
        $term_id =$wpdb->insert_id;

        // create the relationship 
        $query = "INSERT INTO $wpdb->term_taxonomy (term_id, taxonomy) VALUES (%d, %s)";  
        $wpdb->query($wpdb->prepare($query, $term_id, $taxonomy_name));           
        $term_taxonomy_id =$wpdb->insert_id;


        $query = "INSERT INTO $wpdb->term_relationships (object_id, term_taxonomy_id) VALUES (%d, %d)";  
        $wpdb->query($wpdb->prepare($query, $term_id,$term_taxonomy_id));           


     }




     //Now add the variation to the product

     //First, set the available attributes
     //We need to pass an array of slugs for this attribute type (taxonomy)
     
     //Get previously set attributes for this parent
     $existing_terms = wp_get_object_terms($parent_post_id, $taxonomy_name);
     $term_exists = false;
     $avail_attributes = array();
     foreach($existing_terms as $existing_term ) {
     	  if ($existing_term->slug == $sanitize_value){
     	  	$term_exists = true ;
     	  }
     	  //add existing slug to our array
     	  array_push($avail_attributes,$existing_term->slug);
     }

     //If this attribute was not previously added, add it to the available attributes array
     if ($term_exists == false){
     	  array_push($avail_attributes,$sanitize_value);
     } 

     
     //Add available attributes to terms of parent for this attribute type (taxonomy)
     wp_set_object_terms($parent_post_id, $avail_attributes, $taxonomy_name); //replace terms with new array

     //Add available attributes to terms of child for this attribute type (taxonomy)
     wp_set_object_terms($post_id, $avail_attributes, $taxonomy_name); //replace terms with new array

     //Update variation for this attribue value for this attribute type (taxonomy)
     update_post_meta($post_id, 'attribute_'. $taxonomy_name , $sanitize_value);
     
     return;

}


function econnect_import_categories(){

  global $econnect_request_packet, $econnect_response_packet, $econnect_error;

  // Import all categories from ManageMore, so that all categories (and more importantly, parent, grandparent categories) will be present
  // Note: We hard coded this to request 100 at a time, up to 100 times.  Deemed enough to receive all categories.  Will break when completed.
  $econnect_category_request=array();
  $econnect_category_request['QuantityStart']=1;
  $econnect_category_request['QuantityRequested']=100 ;
  for ($c=1; $c<=100; $c++) {
      
        //Send Packet
        $econnect_response = econnect_sendcategorypacket($econnect_category_request);
        
        //Check for Error
        if (strtoupper(substr($econnect_response,0,5)) == 'ERROR') {
          return $econnect_response; 
        }
   
        //Parse Response
        //a) Explode response by pipe delimiter
        $econnect_temp_array = explode("|",$econnect_response_packet);
        //b) For each item, add to packet array with Key, Value
        $econnect_packet_array=array();
        foreach ($econnect_temp_array as $econnect_value_pair) {
           $econnect_value_pair_array = explode("=",$econnect_value_pair);
           $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
        }
        //c) Call function to parse category packet into array 
        $econnect_category_array = econnect_parsecategorypacket($econnect_packet_array);
       
  
        //Process Categories
        foreach ($econnect_category_array as $econnect_category_item){
         	  econnect_check_category($econnect_category_item);
        }

     //Update Counters, Stop if Complete
     $econnect_processed_category_quantity += $econnect_packet_array['RETQTY'];
     $econnect_total_category_quantity = $econnect_packet_array['TOTQTY'];
     $econnect_category_request['QuantityStart'] += $econnect_packet_array['RETQTY'];
     if ($econnect_processed_category_quantity >= $econnect_total_category_quantity) {
        break;
     }
 
 
  } // loop to get all categories in MM


}


/* econnect_sendcategorypacket()
   --------------------------------
   This function is used to send an category query packet (1010) to the eConnect Server.
   It requires $econnect_category_request array to be passed, which contains all the elements
   necessary to build the request packet (e.g., quantity requested, etc.)
   Return Value: string - Total number of items found/Error message
*/
function econnect_sendcategorypacket($econnect_category_request) {
   global $econnect_request_packet, $econnect_response_packet, $econnect_error;

   $t=time();
   $current_user = wp_get_current_user();
   
   
   $econnect_request_packet = "1010|WID=" . $current_user->ID .
           "|LANG=" .
           "|REQQTY=" . $econnect_category_request['QuantityRequested'] .
           "|STQTY=" . $econnect_category_request['QuantityStart'] .
           "|PPW=" . esc_attr( get_option('econnect_password') ) .
           "|D=".date("m/d/y",$t)."|T=".date("h:iA",$t).chr(4);

     

   econnect_sendpacket();
   if ($econnect_error != "") {
      $econnect_response = $econnect_error ;
    }
   else {
      // Get Total Qutantity Returned
      $totpos = strpos($econnect_response_packet,"|TOTQTY=");
      $totend = strpos($econnect_response_packet,"|",$totpos+1);
      $totqty = substr($econnect_response_packet,$totpos+8,$totend-$totpos-8);
      if ($totqty == '0') {
         $econnect_response = 'Error - no items found to import.';
      }
      else{
         $econnect_response = $totqty ;
      }
    }
   return $econnect_response; 

}




/* econnect_parsecategorypacket()
   --------------------------------
   This function is used convert the econnect_packet_array, which contains
   field/value pair elements, to the econnect_category_array, which is a
   an array which contains an element for each category returned
   and a sub-element for each field of the category.  (e.g., $econnect_inventory_array[1]['CategoryId'])
   Return Value: array - $econnect_inventory_array
*/
function econnect_parsecategorypacket($econnect_packet_array) {

     $econnect_category_array=array();

     $returned_quantity = $econnect_packet_array['RETQTY'];
     for ($i=1; $i <= $returned_quantity; $i++){
        $item = sprintf("%03s",$i); //001 - 999
        $econnect_category_array[$i]['Category'] = $econnect_packet_array['CATG' . $item];
        $econnect_category_array[$i]['CategoryId'] = $econnect_packet_array['CATI' . $item];
        $econnect_category_array[$i]['ParentCategory'] = $econnect_packet_array['PCTG' . $item];
        $econnect_category_array[$i]['ParentCategoryId'] = $econnect_packet_array['PCTI' . $item];
        $econnect_category_array[$i]['Image'] = $econnect_packet_array['IMAG' . $item];
        $econnect_category_array[$i]['Thumbnail'] = $econnect_packet_array['THMB' . $item];
     }
     return $econnect_category_array;
}



/* econnect_check_category()
   --------------------------------
   This function is used to check whether a particular category/parent exists
*/

function econnect_check_category($econnect_category_item){

   //   Set "sanitized" version of names (for slug values)
	 $sanitize_category = sanitize_title($econnect_category_item['Category']);
	 $sanitize_parent_category = sanitize_title($econnect_category_item['ParentCategory']);


	 //Check if parent category exists
	 if ( $econnect_category_item['ParentCategory'] != '' ) {
      wp_insert_term(
        $econnect_category_item['ParentCategory'], // the term 
        'product_cat', // the taxonomy
        array(
          'description'=> '',                //not supported in managemore
          'slug' => $sanitize_parent_category
        )
      );	 	  

	 }

   $parent_term_id=0;
	 if ( $econnect_category_item['ParentCategory'] != '' ) {
      $parent_term = term_exists( $sanitize_parent_category, 'product_cat' ); // array is returned if taxonomy is given
      $parent_term_id = $parent_term['term_id']; // get numeric term id
   }

   wp_insert_term(
     $econnect_category_item['Category'], // the term 
     'product_cat', // the taxonomy
     array(
       'description'=> '',                //not supported in managemore
       'slug' => $sanitize_category,
       'parent' => $parent_term_id
     )
   );	 	  


	 return;
	 
	
}
?>