<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * ManageMore Payment Gateway
 *
 * Provides a ManageMore Payment Gateway
 *
 * @class 		WC_Gateway_ManageMore_Cayan
 * @extends		WC_Payment_Gateway_CC
 * @version		2.17
 * @package		WooCommerce/Classes/Payment
 * @author 		Intellisoft Solutions, Inc.
 */
class WC_Gateway_ManageMore_Cayan extends WC_Payment_Gateway_CC {

    /**
     * Constructor for the gateway.
     */
	public function __construct() {
		$this->id                 = 'managemore_cayan';
		$this->icon               = null;
		$this->has_fields         = true;
		// Supports the default credit card form
		$this->supports = array( 'default_credit_card_form',
		                         'refunds',
		                         'tokenization' );
		$this->method_title       = __( 'ManageMore eConnect with Cayan', 'woocommerce' );
		$this->method_description = __( '<img src="' . plugins_url( 'images/mmpill.png', __FILE__ ) . '" >&nbsp;&nbsp;&nbsp;<img src="' . plugins_url( 'images/cayan.png', __FILE__ ) . '" ><br>Allows payments via ManageMore\'s unique IntelliCharge Payment Gateway and automatically transmits orders to ManageMore via eConnect.<br><br>
		                                 <a href="http://www.managemore.com/intellicharge/application.htm" target="_blank">Sign-Up For a FREE Merchant Account</a>
		                                 <a href="' . admin_url( 'admin.php?page=econnect-settings' ) . '">Plug-In Settings</a>', 'woocommerce' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

      // Turn these settings into variables we can use
		foreach ( $this->settings as $setting_key => $value ) {
			$this->$setting_key = $value;
		}
		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );

    //Test mode setting (for dev use)
    if ($this->merchantname == "Test ManageMore")
               $this->testmode = true;

  	   //Save Settings
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
    
  }

    /**
     * Initialise Gateway Settings Form Fields
     */
    public function init_form_fields() {
      
    	$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable ManageMore eConnect with Cayan', 'woocommerce' ),
				'default' => 'no'
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'Credit Card Payment', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'merchantname' => array(
				'title'       => __( 'Merchant Name', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'The Merchant Name assigned by Cayan', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'siteid' => array(
				'title'       => __( 'Site Id', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'The Site Id assigned by Cayan', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'sitekey' => array(
				'title'       => __( 'Site Key', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'The Site Key assigned by Cayan', 'woocommerce' ),
				'default'     => __( '', 'woocommerce' ),
				'desc_tip'    => true,
			),
			'authtype' => array(
				'title'       => __( 'Authorization Type', 'woocommerce' ),
				'type'        => 'select',
				'description' => __( 'Type of authorization to perform', 'woocommerce' ),
				'default'     => 'Capture',
				'options' => array(
                              'preauth' => __( 'PreAuth', 'woocommerce' ),
                              'capture' => __( 'Capture', 'woocommerce' )
                               ),
				'desc_tip'    => true,
			),
			'cardonfile' => array(
				'title'   => __( 'Allow Card on File', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Allow customer to save token of credit card on file for future checkout', 'woocommerce' ),
				'default' => 'no'
			),
			'autovoid' => array(
				'title'   => __( 'Auto Void on Failure', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Automatically cancel order and void credit card if there is an eConnect failure', 'woocommerce' ),
				'default' => 'yes'
			),
		);
    }


 
   /**
   * Override field name function
   */
   public function field_name( $name ) {

        return ' name="' . esc_attr( $this->id . '-' . $name ) . '" ' ;
     
   }

  /**
  * Display payment form.
  */
  function payment_fields(){
     if (is_ssl() == false and $this->testmode != true) {
     	 echo "We're sorry, payments cannot be accepted at this time.  A secure connection (HTTPS) is required.";
     	 return;
     }
     if ( is_checkout() ) {
        if ($this->cardonfile=='no'){
        	 //card on file not allowed.  show cc form without tokenization options
           $this->form();
        }
        else {
        	 //card on file allowed.  show cc form with tokenization options
	         $this->tokenization_script();
           $this->saved_payment_methods();
           $this->form();
           $this->save_payment_method_checkbox();
           echo '<br>';
           echo '<br>';
        }
     }
     else {
        if ($this->cardonfile=='no'){
        	echo "We're sorry, but you cannot save cards on file at this time.";
        }
        else {
        	 //card on file allowed, in add payment method area.  show cc form only
	         $this->tokenization_script();
           $this->form();
      	}
     }
  	 return;
  	
  }

  // Validate fields
  public function validate_fields() {


    //if token used, no need for futher validation
    if ( isset( $_POST['wc-'.$this->id.'-payment-token'] ) && 'new' !== $_POST['wc-'.$this->id.'-payment-token'] )
      return true;  

  	$card_number = $_POST[$this->id.'-card-number'];
 
  	if ( $card_number === '' ){
  		 $error = "Credit Card Number is required.";
       wc_add_notice( $error , "error" );
       return false;
          
  	}
  	
  	if ( $this->mod_10_check($card_number) == false){
  		 $error = "Credit Card Number is invalid.";
       wc_add_notice( $error , "error" );
       return false;
 		
  	}
  	
  	$card_expiry = $_POST[$this->id.'-card-expiry'];
  	$len = strlen($card_expiry);
   	if ($len != 7){
  		 //should be "mm / yy"
  		 $error = "Expiration Date is invalid.";
       wc_add_notice( $error , "error" );
       return false;
  	}
  	
  	$cvc = $_POST[$this->id.'-card-cvc'];
 
  	if ( $cvc === '' ){
  		 $error = "Card Security Code is required.";
       wc_add_notice( $error , "error" );
       return false;
  	}
  	

   	return true;
  }

  //mod 10 check to validate card
  function mod_10_check($number) {

    // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
    $number=preg_replace('/\D/', '', $number);

    // Set the string length and parity
    $number_length=strlen($number);
    $parity=$number_length % 2;

    // Loop through each digit and do the maths
    $total=0;
    for ($i=0; $i<$number_length; $i++) {
      $digit=$number[$i];
      // Multiply alternate digits by two
      if ($i % 2 == $parity) {
        $digit*=2;
        // If the sum is two digits, add them together (in effect)
        if ($digit > 9) {
          $digit-=9;
        }
      }
      // Total up the digits
      $total+=$digit;
    }

    // If the total mod 10 equals 0, the number is valid
    return ($total % 10 == 0) ? TRUE : FALSE;

  }

  public function add_payment_method() {

            if (is_ssl() == false and $this->testmode != true) {
               wc_add_notice( "We're sorry, payment methods cannot be added at this time.  A secure connection (HTTPS) is required." , "error" );
               return false;
            }

            //Board the Card in Vault 
            $submit_type = 'VaultCreditCard';
            $order = null;
            $vault_response = $this->_sendCCRequest($submit_type , $order);
            
            if ($vault_response->VaultToken != '') {

               $cardnum = $_POST[$this->id.'-card-number'];
               $firstdigit = substr($cardnum, 0, 1);
               switch ($firstdigit){
                   case "3":
                      $this->card_type = 'amex';
                      break;
                   case "4":
                      $this->card_type = 'visa';
                      break;
                   case "5":
                      $this->card_type = 'mastercard';
                      break;
                   case "6":
                      $this->card_type = 'discover';
                      break;
                   default:
			                $response_msg_to_customer = 'Card Type Error';
                      wc_add_notice( __('Token error: ', 'woothemes') . $response_msg_to_customer, 'error' );
    	    		        return false;
                      
               }
               $ccexp = str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] );
	       		   $ccmonth = substr($ccexp,0,2);
	       		   $ccyear = '20'.substr($ccexp,2,2); 
               $token = new WC_Payment_Token_CC();
               $token_string = $vault_response->VaultToken;
               $token->set_token( $token_string );
               $token->set_gateway_id( $this->id ); // `$this->id` references the gateway ID set in `__construct`
               $token->set_card_type( $this->card_type );
               $token->set_last4( substr(str_replace( array(' ', '-' ), '', $_POST[$this->id.'-card-number'] ),-4) );
               $token->set_expiry_month( $ccmonth );
               $token->set_expiry_year( $ccyear );
               $token->set_user_id( get_current_user_id() );
               $token->save();
               wc_add_notice( __('Card Saved on File', 'woothemes') , 'success' );
 
               return true;
    	    	}
    	    	else {
			         // Add notice
			         $response_msg_to_customer = $vault_response->ErrorMessage;
               wc_add_notice( __('Token error: ', 'woothemes') . $response_msg_to_customer, 'error' );
    	    		 return false;
    	    	}
  	
  }

  /**
  * Special version of process_payment used by crm for a customer payment
  *
 * @return string
  */
	public function process_payment_crm( $payment ) {
      global $woocommerce;


      if (is_ssl() == false and $this->testmode != true) {
          $response_msg_to_customer ="We're sorry, payments cannot be accepted at this time.  A secure connection (HTTPS) is required.";
          wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
          return;
      }

      //Check if Token (Card on File) to be used
      if ( isset( $_POST['wc-'.$this->id.'-payment-token'] ) && 'new' !== $_POST['wc-'.$this->id.'-payment-token'] ) {

         $token_id = wc_clean( $_POST['wc-'.$this->id.'-payment-token'] );
         $token    = WC_Payment_Tokens::get( $token_id );

         // Token user ID does not match the current user... bail out of payment processing.
         if ( $token->get_user_id() !== get_current_user_id() ) {
         	   $response_msg_to_customer = 'The card on file selected is invalid for your user id.';
             wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
             return;
         }
         $this->token_id = $token_id;
         $this->token_value = $token->get_token();

         $submit_type = 'SaleVault' ; 


      } 
      else {

         $submit_type =  'Sale' ;

      }

      //Call Gateway
      unset($response);
      
      //Check if card processing should be skipped via
      //special 'skip' value in the Merchant Name
      if (strtoupper($this->merchantname) != 'SKIP'){
         $response = $this->_sendCCRequest_crm($submit_type,$payment);
         //Check Response
         $response_status = $response->ApprovalStatus;
         $auth_code = $response->AuthorizationCode;  //fixed 5.10.2018 - was using AuthCode which was not correct
         $this->gateway_refid = $response->Token;
         $this->auth_code = $auth_code;
      }
      else {
         $response_status = 'APPROVED'; 
         $this->gateway_refid ='123456';
         $this->auth_code = 'FAKE123';
      }
      
      // If the response code is not APPROVED then redirect back to the payment page with the appropriate error message
      if ($response_status == 'APPROVED') {	

         //Payment Approved
         
												 
         //Send Order to eConnect
         $this->_econnect_sendpacket_crm_payment($payment);
         if ($this->eConnectError ==true){
            return;
         }

         //All OK, approved and sent to eConnect

   
         //Save Account Number, other info on Customer Record
         $user = wp_get_current_user();
         $user_id = $user->ID ;
         if ($user_id) {
            $value = $this->econnect_response_array['B'];
            update_user_meta( $user_id, '_econnect_field_B' , $value);
            $value = $this->econnect_response_array['LPD'];
            update_user_meta( $user_id, '_econnect_field_LPD' , $value);
            $value = $this->econnect_response_array['LAR'];
            update_user_meta( $user_id, '_econnect_field_LAR' , $value);
         }
         
			   // Add notice
         wc_add_notice( 'Thank you.  Your payment of '.number_format($payment['amount'],2).' has been received and posted.', 'success' );

         return;

		  } else {
		  	
			   // Transaction was not succesful
	       if ($this->commError == '' ){ 
	          //If "normal" error (e.g. Declined; not an internal gateway communication error)
	          if ($response_status != ""){
               $response_text_array = explode(';',$response_status);        
               $response_msg_to_customer = strtoupper($response_text_array[2]) . ' (' . $response_text_array[1] . ') - Payment Not Approved' ;
            }
            else{
            	//Other error, will be in ErrorMessage field
            	$response_msg_to_customer = "We are sorry, there was an error processing your card.  Please try again later.";
            }
            
            
         }
          else {
            $response_msg_to_customer = 'There was a Credit Card Communication Error - Please Try Again Later' ;
         }   

			   // Add notice
         wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
         return;

      }	

  }


  /**
  * Process the payment and return the result
  *
  * @param int $payment
  * @return array
  */
	public function process_payment( $order_id ) {
      global $woocommerce;
      
      if (is_ssl() == false and $this->testmode != true) {
          wc_add_notice( "We're sorry, payments cannot be accepted at this time.  A secure connection (HTTPS) is required." , "error" );
          return;
      }


      //Get Order
 		  $order = new WC_Order( $order_id );
      
		
		  //See if we should send a Check Stock Packet
		  if (get_option('econnect_checkstock')=='yes' || get_option('econnect_checkstock')=='all'){
		    $this->_econnect_checkstock($order);
          if ($this->eConnectError ==true){
            return;
          }
      }

      //Check if Token (Card on File) to be used
      if ( isset( $_POST['wc-'.$this->id.'-payment-token'] ) && 'new' !== $_POST['wc-'.$this->id.'-payment-token'] ) {

         $token_id = wc_clean( $_POST['wc-'.$this->id.'-payment-token'] );
         $token    = WC_Payment_Tokens::get( $token_id );

         // Token user ID does not match the current user... bail out of payment processing.
         if ( $token->get_user_id() !== get_current_user_id() ) {
         	   $response_msg_to_customer = 'The card on file selected is invalid for your user id.';
             // Add notice to the cart
             wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
             return;
         }
         $this->token_id = $token_id;
         $this->token_value = $token->get_token();

         $submit_type = ($this->authtype=='preauth' && get_option('econnect_transtype') != 'invoice') ? 'PreAuthVault' : 'SaleVault' ; 


      } 
      else {

         $submit_type = ($this->authtype=='preauth' && get_option('econnect_transtype') != 'invoice') ? 'PreAuth' : 'Sale' ; 

      }

      //Call Gateway
      unset($response);
      
      //Check if card processing should be skipped via
      //special 'skip' value in the Merchant Name
      if (strtoupper($this->merchantname) != 'SKIP'){
         $response = $this->_sendCCRequest($submit_type,$order);
         //Check Response
         $response_status = $response->ApprovalStatus;
         $auth_code = $response->AuthorizationCode; //fixed 2.16 - (12.20.2017) was ->AuthCode which was not correct
         $this->gateway_refid = $response->Token;
         $this->auth_code = $auth_code;
      }
      else {
         $response_status = 'APPROVED'; 
         $this->gateway_refid ='123456';
         $this->auth_code = 'FAKE123';
      }
      
      // If the response code is not APPROVED then redirect back to the payment page with the appropriate error message
      // If APPROVED, send packet to eConnect Server, and finish order
      if ($response_status == 'APPROVED') {	

          //Payment Approved
          
          //Set PreAuth -------------------------------------------------------------------
          if (get_option('econnect_transtype') == 'invoice'){
             $this->trantype = 1 ; //invoice
             $this->preauth = 0 ; //cannot have preauth on invoice - would have been trapped above to process as full auth
          }
          else {
             $this->trantype = 7 ;  //assume sales order
             if ($this->authtype=='preauth') {
                 $this->preauth='1';
             }
             else {
                 $this->preauth='0';
             }
          }

          //Set Payment Info -------------------------------------------------------------------
          if ($this->token_value!= ''){
          	 //card info from token
             $token_id = $this->token_id;
             $this->card_token    = WC_Payment_Tokens::get( $token_id );
             $this->card_num = $token->get_last4();
             $this->card_type= $token->get_card_type();
             switch ($this->card_type){
                 case "amex":
                    $this->card_desc = "American Express";
                   break;
                 case "visa":
                    $this->card_desc = "Visa";
                    break;
                 case "mastercard":
                    $this->card_desc = "MasterCard";
                    break;
                 case "discover":
                    $this->card_desc = "Discover";
                    break;
                 default:
                    $this->card_desc = "Credit Card";
                    break;
             }
             $this->card_expdate = $token->get_expiry_month() . substr($token->get_expiry_year(),-2);
         	
          }
          else {
          	 //card info entered
             $this->card_num = $_POST[$this->id.'-card-number'];
             $firstdigit = substr($this->card_num, 0, 1);
             switch ($firstdigit){
                 case "3":
                    $this->card_desc = "American Express";
                    $this->card_type = 'amex';
                    break;
                 case "4":
                    $this->card_desc = "Visa";
                    $this->card_type = 'visa';
                    break;
                 case "5":
                    $this->card_desc = "MasterCard";
                    $this->card_type = 'mastercard';
                    break;
                 case "6":
                    $this->card_desc = "Discover";
                    $this->card_type = 'discover';
                    break;
                 default:
                    $this->card_desc = "Credit Card";
                    break;
             }
             $this->card_expdate = str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] );
          	
          }
         
			   // Add note to order
			   $order->add_order_note( 'Credit Card Payment completed. Gateway Ref Id=' . $this->gateway_refid);
												 
         //Send Order to eConnect
         $this->_econnect_sendpacket($order);
         if ($this->eConnectError ==true){
            return;
         }

         //All OK, approved and sent to eConnect
         
         //Update Post Meta with Gateway Ref Id (for refunds), and other payment info (for REST-based importing into MM)
         update_post_meta( $order->id, '_econnect_card_desc', $this->card_desc );
         update_post_meta( $order->id, '_econnect_card_num', substr($this->card_num,-4) );
         update_post_meta( $order->id, '_econnect_auth_code', substr($this->auth_code,10)  );  //note: substring fixes rare cases (esp with test account) that auth_code is null
         update_post_meta( $order->id, '_econnect_auth_type', $this->preauth );
         update_post_meta( $order->id, '_econnect_gateway_refid', $this->gateway_refid );
         update_post_meta( $order->id, '_econnect_card_expdate', substr($this->card_expdate,0,2) . '/' . substr($this->card_expdate,2,2) );
   
         //Save Account Number, other info on Customer Record
         $user = wp_get_current_user();
         $user_id = $user->ID ;
         if ($user_id) {
            $value = $this->econnect_response_array['CID'];
            update_user_meta( $user_id, '_econnect_field_CID' , $value);
            $value = $this->econnect_response_array['B'];
            update_user_meta( $user_id, '_econnect_field_B' , $value);
            $value = $this->econnect_response_array['LPD'];
            update_user_meta( $user_id, '_econnect_field_LPD' , $value);
            $value = $this->econnect_response_array['LAR'];
            update_user_meta( $user_id, '_econnect_field_LAR' , $value);
            $value = $this->econnect_response_array['LSIA'];
            update_user_meta( $user_id, '_econnect_field_LSIA' , $value);
            $value = $this->econnect_response_array['LSID'];
            update_user_meta( $user_id, '_econnect_field_LSID' , $value);
         }

         // Save Token, if necessary
         $save_token = ( isset( $_POST['wc-'.$this->id.'-payment-token'] ) && 'new' == $_POST['wc-'.$this->id.'-payment-token'] ) ? 'yes' : 'no' ;
         if ($save_token=="yes") {

            //Board the Card in Vault by Reference
            $submit_type = 'VaultByReference';
            $vault_response = $this->_sendCCRequest($submit_type,$order);
            
            if ($vault_response->VaultToken != '') {

               $ccexp = str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] );
	       		   $ccmonth = substr($ccexp,0,2);
	       		   $ccyear = '20'.substr($ccexp,2,2); 
               $token = new WC_Payment_Token_CC();
               $token_string = $vault_response->VaultToken;
               $token->set_token( $token_string );
               $token->set_gateway_id( $this->id ); // `$this->id` references the gateway ID set in `__construct`
               $token->set_card_type( $this->card_type );
               $token->set_last4( substr(str_replace( array(' ', '-' ), '', $_POST[$this->id.'-card-number'] ),-4) );
               $token->set_expiry_month( $ccmonth );
               $token->set_expiry_year( $ccyear );
               $token->set_user_id( get_current_user_id() );
               $token->save();

               //Save note to order about token being stored
               $tokeninfo = 'Token stored.';
    	    	   $order->add_order_note( $tokeninfo );
    	    	}
    	    	else {

               //Save note to order about token not being stored
               $tokeninfo = 'Token requested but could NOT be stored.  Error: '. $vault_response->ErrorMessage;
    	    	   $order->add_order_note( $tokeninfo );


            }
            	       		


         }

   			 // Mark order as Paid
   			 $order->payment_complete();
   
   			 // Empty the cart (Very important step)
   			 $woocommerce->cart->empty_cart();
   

   			 // Redirect to thank you page
		   	 return array(
	   			'result'   => 'success',
			   	'redirect' => $this->get_return_url( $order ),
			    );
			    
			    
		  } else {
		  	
			   // Transaction was not succesful
	       if ($this->commError == '' ){ 
	          //If "normal" error (e.g. Declined; not an internal gateway communication error)
	          if ($response_status != ""){
               $response_text_array = explode(';',$response_status);        
               $response_msg_to_customer = strtoupper($response_text_array[2]) . ' (' . $response_text_array[1] . ') - Payment Not Approved' ;
               $error_text = $response_msg_to_customer;
            }
            else{
            	//Other error, will be in ErrorMessage field
            	$response_msg_to_customer = "We are sorry, there was an error processing your card.  Please try again later.";
              $order->update_status('failed', 'Gateway Error: '. $response->ErrorMessage);
            }
            
            
         }
          else {
            //Mark Order as Failed if gateway communication error (so admin will know)
            $order->update_status('failed', 'Gateway Communication Error: '. $this->commError );
            $response_msg_to_customer = 'There was a Credit Card Communication Error - Please Try Again Later' ;
            $error_text = $this->commError;
         }   

			   // Add notice to the cart
         wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );


         return;

      }	
	}


  /**
   * Process Refund from Admin section View Order page
   */
  public function process_refund( $order_id, $amount = null, $reason='' ) {

     //Get Order
	   $order = new WC_Order( $order_id );

     //Get Post Meta with Gateway Ref Id
     $this->gateway_refid = get_post_meta( $order_id, '_econnect_gateway_refid', true );
     $this->refund_amount = $amount;
     $submit_type = "Refund";
     $response = $this->_sendCCRequest($submit_type,$order);
     $response_status = $response->ApprovalStatus;
     $success = ($response_status == 'APPROVED') ? true : false;
     
     return $success;
  }

    	
  /**
   * Send Credit Card communication request
   */
  public function _sendCCRequest($submit_type,$order) {
     // set URL
    $url = 'https://ps1.merchantware.net/Merchantware/ws/RetailTransaction/v4/Credit.asmx?WSDL';
    if($submit_type == 'Void'){
        //Build array to send for voiding
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'token' => $this->gateway_refid,
                      'registerNumber' => "",
                      'merchantTransactionId' => "");
    }
    elseif($submit_type == 'Refund'){
        //Build array to send for refunding
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'token' => $this->gateway_refid,
                      'invoiceNumber' => str_replace( "#", "", $order->get_order_number() ),
                      'overrideAmount' => number_format($this->refund_amount, 2,'.',''),
                      'registerNumber' => "",
                      'merchantTransactionId' => "");
    }
    elseif($submit_type == 'VaultCreditCard'){
    	  $current_user_id = get_current_user_id() ;
    	  $fname = get_user_meta( $current_user_id, 'first_name', true );
        $lname = get_user_meta( $current_user_id, 'last_name', true );
        $address_1 = get_user_meta( $current_user_id, 'billing_address_1', true ); 

    	  //Prevent problems with postal code.  For US AVS only, must be no more than 5 digits, numeric.
        if (get_user_meta( $current_user_id, 'billing_country', true ) != 'US'){
        	$post_code = '';
        }
        else {
          $post_code = get_user_meta( $current_user_id, 'billing_postcode', true );
    	    $post_code = substr($post_code,5);
    	    $post_code = preg_replace("/[^0-9]/", "", $post_code);
    	  }

        //Build array to send for getting token by keyed card
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'cardNumber' => str_replace( array(' ', '-' ), '', $_POST[$this->id.'-card-number'] ),
                      'expirationDate' => str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] ),
                      'cardholder' => $fname . ' '. $lname,
                      'avsStreetAddress' => $address_1,
                      'avsStreetZipCode' => $post_code
                       );
    }
    elseif($submit_type == 'VaultByReference'){
        //Build array to send for getting token by reference
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'referenceNumber' => $this->gateway_refid
                       );
    }
    elseif($submit_type == 'SaleVault' || $submit_type == 'PreAuthVault'){
    	  
    	
        //Build array to send for Vault Sale, Vault PreAuth
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'invoiceNumber' => str_replace( "#", "", $order->get_order_number() ),
                      'amount' => number_format($order->order_total, 2,'.',''),
                      'vaultToken' => $this->token_value
                       );

    }
    else {
    	
    	  //Prevent problems with postal code.  For US AVS only, must be no more than 5 digits, numeric.
    	  if ($order->billing_country != 'US'){
    	  	$post_code = '';
    	  }
    	  else {
      	  $post_code = substr($order->billing_postcode,5);
      	  $post_code = preg_replace("/[^0-9]/", "", $post_code);
    	  }
    	  
    	
        //Build array to send for Sale, PreAuth
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'invoiceNumber' => str_replace( "#", "", $order->get_order_number() ),
                      'amount' => number_format($order->order_total, 2,'.',''),
                      'cardNumber' => str_replace( array(' ', '-' ), '', $_POST[$this->id.'-card-number'] ),
                      'expirationDate' => str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] ),
                      'cardholder' => $order->billing_first_name . ' '. $order->billing_last_name,
                      'avsStreetAddress' => $order->billing_address_1,
                      'avsStreetZipCode' => $post_code,
                      'cardSecurityCode' => ( isset( $_POST[$this->id.'-card-cvc'] ) ) ? $_POST[$this->id.'-card-cvc'] : '');
    }

    // Post order info data to MerchantWARE via SOAP - Requires that PHP has SOAP support installed
    $this->commError = '';
    $client = new SoapClient($url,array("exceptions" => true));
    switch($submit_type) {
      case 'PreAuth':
        try {
            $result = $client->PreAuthorizationKeyed($submit_data);
            $response = $result->PreAuthorizationKeyedResult;
            }
        catch (Exception $e) {
            $this->commError =$e->getMessage();  
            }  
        break;
      case 'Void':
        try {
            $result = $client->Void($submit_data);
            $response = $result->VoidResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      case 'Refund':
        try {
            $result = $client->Refund($submit_data);
            $response = $result->RefundResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      case 'VaultCreditCard':
        try {
            $result = $client->VaultBoardCreditKeyed($submit_data);
            $response = $result->VaultBoardCreditKeyedResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      case 'VaultByReference':
        try {
            $result = $client->VaultBoardCreditByReference($submit_data);
            $response = $result->VaultBoardCreditByReferenceResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      case 'SaleVault':
        try {
            $result = $client->SaleVault($submit_data);
            $response = $result->SaleVaultResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      case 'PreAuthVault':
        try {
            $result = $client->PreAuthVault($submit_data);
            $response = $result->PreAuthVaultResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      default:
        try {
            $result = $client->SaleKeyed($submit_data); 
            $response = $result->SaleKeyedResult;
            }  
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
     }
     return $response ;
  }

  /**
   * Send Credit Card communication request for customer payment
   */
  public function _sendCCRequest_crm($submit_type,$payment) {
  	
  	
  	
     // set URL
    $url = 'https://ps1.merchantware.net/Merchantware/ws/RetailTransaction/v4/Credit.asmx?WSDL';
    if($submit_type == 'Void'){
        //Build array to send for voiding
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'token' => $this->gateway_refid,
                      'registerNumber' => "",
                      'merchantTransactionId' => "");
    }
    elseif($submit_type == 'SaleVault' || $submit_type == 'PreAuthVault'){
    	  
    	
        //Build array to send for Sale, PreAuth
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'invoiceNumber' => $payment['payment_no'],
                      'amount' => number_format($payment['amount'], 2,'.',''),
                      'vaultToken' => $this->token_value
                       );

    }
    else {
    	
    	  //Prevent problems with postal code.  For US AVS only, must be no more than 5 digits, numeric.
    	  //Country not available here
    	  $post_code = substr($payment['bill_zip'],5);
    	  $post_code = preg_replace("/[^0-9]/", "", $post_code);
    	  
    	
        //Build array to send for Sale, PreAuth
        $submit_data = array(
                      'merchantName' => $this->merchantname,
                      'merchantSiteId' => $this->siteid, 
                      'merchantKey' => $this->sitekey,
                      'invoiceNumber' => $payment['payment_no'],
                      'amount' => number_format($payment['amount'], 2,'.',''),
                      'cardNumber' => str_replace( array(' ', '-' ), '', $_POST[$this->id.'-card-number'] ),
                      'expirationDate' => str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] ),
                      'cardholder' => $payment['first_name'] . ' '. $payment['last_name'],
                      'avsStreetAddress' => $payment['cust_id'],
                      'avsStreetZipCode' => $post_code,
                      'cardSecurityCode' => ( isset( $_POST[$this->id.'-card-cvc'] ) ) ? $_POST[$this->id.'-card-cvc'] : '');
    }

    // Post order info data to MerchantWARE via SOAP - Requires that PHP has SOAP support installed
    $this->commError = '';
    $client = new SoapClient($url,array("exceptions" => true));
    switch($submit_type) {
      case 'PreAuth':
        try {
            $result = $client->PreAuthorizationKeyed($submit_data);
            $response = $result->PreAuthorizationKeyedResult;
            }
        catch (Exception $e) {
            $this->commError =$e->getMessage();  
            }  
        break;
      case 'Void':
        try {
            $result = $client->Void($submit_data);
            $response = $result->VoidResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      case 'SaleVault':
        try {
            $result = $client->SaleVault($submit_data);
            $response = $result->SaleVaultResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      case 'PreAuthVault':
        try {
            $result = $client->PreAuthVault($submit_data);
            $response = $result->PreAuthVaultResult;
            }
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
        break;
      default:
        try {
            $result = $client->SaleKeyed($submit_data); 
            $response = $result->SaleKeyedResult;
            }  
        catch (Exception $e) {
            $this->commError = $e->getMessage();  
            }  
     }
     return $response ;
  }


  /**
   * Send transaction packet to eConnect Server.
   */

  function _econnect_sendpacket($order) {

      global $woocommerce,$econnect_order_packet_array;
     
      $econnect_disable = esc_attr( get_option('econnect_disable') );
      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );
      $connect_timeout = get_option('econnect_connect_timeout');
      $packet_timeout = get_option('econnect_packet_timeout');
      //make sure timeouts not less than minimum
      if ($connect_timeout < 5) $connect_timeout = 5;
      if ($packet_timeout < 10) $packet_timeout = 10;

      //Check if ManageMore processing should be skipped via
      //econnect_disable flag
      if ($econnect_disable=='1') {
         $note_text = "eConnect Processing Disabled.  Order not transmitted.";
         $order->add_order_note( $note_text);
         return;
      }


      if (esc_attr( get_option('econnect_ssl') ) == '1') {
        $socket = @fsockopen("ssl://".$host, $port, $errno, $errstr, $connect_timeout);
      }
      else {
        $socket = @fsockopen($host, $port, $errno, $errstr, $connect_timeout);
      }
      
      if (!$socket) {
         //Void Transaction on Gateway
         if ($this->autovoid=='yes'){
            $submit_type='Void';
            $void_response = $this->_sendCCRequest($submit_type,$order);
   
            //Check if Void was approved, add note to Order
            if ($void_response->ApprovalStatus == 'APPROVED'){
               $void_text .= ' Credit Card payment was successfully voided.';
            } else {
               $void_text .= ' WARNING: Credit Card could not be voided.';
            }

            //Mark Order as Failed
            $error_text = 'Error: eConnect Server not Responding. '.$void_text;
            $order->update_status('failed', __($error_text , 'woothemes'));

   			// Add notice to the cart
            $response_msg_to_customer = 'There was a Server Communication Error - Please Try Again Later ';
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
         }
         else {
   	      // Autovoid off: Just add note of error to the order for reference
            $error_text = 'eConnect Server not Responding.';
	          $order->add_order_note( 'eConnect Error: '. $error_text );
            
         }
         return ;

      
      } else {

          //Socket connection established with eConnect Server.
          //Create Packet using eConnect Pipe-Delimited protocol and send
          
          $t=time(); //for web id
          $oID = str_replace( "#", "", $order->get_order_number() );

/*          //Set PreAuth
          if (get_option('econnect_transtype') == 'invoice'){
             $trantype = 1 ; //invoice
             $authtype = 0 ; //cannot have preauth on invoice
          }
          else {
             $trantype = 7 ;  //assume sales order
             if ($this->authtype=='preauth') {
                 $authtype='1';
             }
             else {
                 $authtype='0';
             }
          }
*/
/*          //Set Payment Info -------------------------------------------------------------------
          if ($this->token_value!= ''){
          	 //card info from token
             $token_id = $this->token_id;
             $token    = WC_Payment_Tokens::get( $token_id );
             $cardnum = $token->get_last4();
             $this->card_type= $token->get_card_type();
             switch ($this->card_type){
                 case "amex":
                    $cardtype = "American Express";
                   break;
                 case "visa":
                    $cardtype = "Visa";
                    break;
                 case "mastercard":
                    $cardtype = "MasterCard";
                    break;
                 case "discover":
                    $cardtype = "Discover";
                    break;
                 default:
                    $cardtype = "Credit Card";
                    break;
             }
             $expdate = $token->get_expiry_month() . substr($token->get_expiry_year(),-2);
         	
          }
          else {
          	 //card info entered
             $cardnum = $_POST[$this->id.'-card-number'];
             $firstdigit = substr($cardnum, 0, 1);
             switch ($firstdigit){
                 case "3":
                    $cardtype = "American Express";
                    $this->card_type = 'amex';
                    break;
                 case "4":
                    $cardtype = "Visa";
                    $this->card_type = 'visa';
                    break;
                 case "5":
                    $cardtype = "MasterCard";
                    $this->card_type = 'mastercard';
                    break;
                 case "6":
                    $cardtype = "Discover";
                    $this->card_type = 'discover';
                    break;
                 default:
                    $cardtype = "Credit Card";
                    break;
             }
             $expdate = str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] );
          	
          }
*/

          //Create Order Packet Array ---------------------------------------------------------------------------

          //Check Account Number on Customer Record, Set Lookup Id, Lookup Type
          $user = wp_get_current_user();
          $user_id = $user->ID ;
          if ($user_id) {
            $custid= get_user_meta( $user_id, '_econnect_custid' , true);
          }
          if ($custid!=''){
          	 $lid = $custid;
          	 $ltp = "1";
          }
          else {
          	 $lid = substr($order->billing_email, 0, 80);
          	 $ltp = "2";
          }
          
          //check if user id should be passed to eConnect to auto-register the user
          $user_info = get_userdata($user_id);
	        $user_login = $user_info->user_login;
          if (get_option('econnect_auto_register') =='1' && get_option('econnect_crm_enabled') == '1'){
          	 $auto_create_login = $user_login;
          }

          $econnect_order_packet_array = array(
             'WID' => $t ,
             'LID' => $lid,
             'LTP' => $ltp,
             'UTP' => '3',
             'TTP' => $this->trantype,
             'LANG' => '',
             'ALID' => $auto_create_login,
             'OID' => $oID,
             'CN' => substr($order->billing_company, 0, 40) ,
             'FN' => substr($order->billing_first_name, 0, 40) ,
             'LN' => substr($order->billing_last_name, 0, 40) ,
             'BA1' => substr($order->billing_address_1, 0, 40) ,
             'BA2' => substr($order->billing_address_2, 0, 40) ,
             'BC' => substr($order->billing_city, 0, 25) ,
             'BS' => substr($order->billing_state,0,3) ,
             'BZ' => substr($order->billing_postcode, 0, 13) ,
             'BCN' => $order->billing_country ,
             'P1' => substr($order->billing_phone, 0, 25) ,
             'EM' => substr($order->billing_email, 0, 80) ,
             'AST' => '1' ,
             'SCT' => substr($order->shipping_first_name, 0, 20) . ' ' . substr($order->shipping_last_name, 0, 20) ,
             'SCM' => substr($order->shipping_company, 0, 50) ,
             'SA1' => substr($order->shipping_address_1, 0, 40) ,
             'SA2' => substr($order->shipping_address_2, 0, 40) ,
             'SC' => substr($order->shipping_city, 0, 25) ,
             'SS' => substr($order->shipping_state,0,3) ,
             'SZ' => substr($order->shipping_postcode, 0, 13) ,
             'SCN' => $order->shipping_country ,
             'SP' => substr($order->billing_phone, 0, 25) ,
             'LOC' => (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' ,          
             'CCT' => $this->card_desc ,
             'CCN' => substr($this->card_num,-4) ,
             'CCX' => substr($this->card_expdate,0,2) . '/' . substr($this->card_expdate,2,2) ,
             'AUTH' => $this->preauth ,
             'AC' => $this->auth_code ,
             'GR' =>  $this->gateway_refid
          );

          //Add Order Item Details -----------------------------------------------------------
          $this->_econnect_orderdetails($order,$econnect_order_packet_array,$item_total);


          //Final Order Info -----------------------------------------------------------------
          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];
          $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                'FRT' => $order->get_total_shipping() ,
                'DISC' => $order->get_total_discount(true) ,
                'SVIA' => $order->get_shipping_method( ) , 
                'TXC' => '' ,
                'TAX1' => $order->get_total_tax() ,
                'NOTE' => $order->customer_note ,
                'PPW' => esc_attr( get_option('econnect_password') ) ,
                'BUA' => $user_agent,
                'RIP' => $ip_address,
                'D' => date("m/d/y",$t) ,
                'T' => date("h:iA",$t)
                ));

          /*
          Add custom hook "econnect_before_sending_order"
          Use this hook to do something custom to the econnect_order_packet_array that will be used
          to create the packet to send to eConnect. (e.g, update Tax Code, etc.)
          */
          //add hook
          do_action('econnect_before_sending_order',$order);

                   

          //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
          $in = '';
          foreach ($econnect_order_packet_array as $array_key => $array_value ) {
           	 $in .= '|'. $array_key . '=' . $array_value ;
          } 
          $in = '1007' . $in . chr(4);  //add packet type, end with chr(4)



          fwrite($socket, $in);
          stream_set_timeout($socket, $packet_timeout);
            
      
          // Get Response
          $response = "";
          $i=0;
          do
          { 
            $out = fgets($socket,2);
            $info = stream_get_meta_data($socket);
            $response .= $out;
            $i++;
          } 
          while($out != chr(4) && $i<1000000 && $info['timed_out']==false);

          fclose($socket);

          if ($response != "") {

            if (strpos($response,"|S=1") === false ) {
               //Success Flag not returned
               //Get Error Message returned
               $errpos = strpos($response,"|E=");
               $errend = strpos($response,"|",$errpos+1);
               $errmsg = substr($response,$errpos+3,$errend-$errpos-3);

               //Void CC Transaction
               if ($this->autovoid=='yes'){
                  $submit_type='Void';
                  $void_response = $this->_sendCCRequest($submit_type,$order);

                  //Check if Void was approved, add note to Order
                  if ($void_response->ApprovalStatus == 'APPROVED'){
                     $void_text .= ' Credit Card payment was successfully voided.';
                  } else {
                     $void_text .= ' WARNING: Credit Card could not be voided.';
                  }
   
                  //Mark Order as Failed
                  $error_text = 'Error: '.$errmsg.'. ' .$void_text;
                  $order->update_status('failed', __($error_text , 'woothemes'));
   
         			// Add notice to the cart
                  $response_msg_to_customer = 'There was a Server Error - Please Try Again Later' ;
                  wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
                  $this->eConnectError=true;
               }
               else {
         	        // Autovoid off: Just add note of error to the order for reference
                  $error_text = $errmsg.'.';
      	          $order->add_order_note( 'eConnect Error: '. $error_text );
               }
               return ;
               
               
            }
            else {
               //Parse Response
               $econnect_response_packet = $response;
               //a) Explode response by pipe delimiter
               $econnect_temp_array = explode("|",$econnect_response_packet);
               //b) For each item, add to packet array with Key, Value
               foreach ($econnect_temp_array as $econnect_value_pair) {
                  $econnect_value_pair_array = explode("=",$econnect_value_pair,2);
                  $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
               }

			         // Add note to the order for reference
			         $success_text = 'Order successfully transmitted to ManageMore. Transction Id = '.$econnect_packet_array['TID'];
			         $order->add_order_note( $success_text);
			         
			         //Set econnect reponse array
			         $this->econnect_response_array = $econnect_packet_array;
               return; //Success
               
            }
            
          }
          else {
            //Invalid response
            //Void CC Transaction
            if ($this->autovoid=='yes'){
               $submit_type='Void';
               $void_response = $this->_sendCCRequest($submit_type,$order);
   
               //Check if Void was approved, add note to Order
               if ($void_response->ApprovalStatus == 'APPROVED'){
                  $void_text .= ' Credit Card payment was successfully voided.';
               } else {
                  $void_text .= ' WARNING: Credit Card could not be voided.';
               }

               //Mark Order as Failed
               $error_text = 'Error: Invalid Server Response / Timeout. '.$void_text;
               $order->update_status('failed', __($error_text , 'woothemes'));
  
      			   // Add notice to the cart
               $response_msg_to_customer = 'There was a Server Error Invalid Response / Timeout - Please Try Again Later' ;
               wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
               $this->eConnectError=true;
            }
            else {
      	      // Autovoid off: Just add note of error to the order for reference
               $error_text = 'Invalid Server Response / Timeout.';
	            $order->add_order_note( 'eConnect Error: '. $error_text );
            }
            return ;
          }
      }
  }

  /**
   * Send payment packet to eConnect Server for CRM.
   */

  function _econnect_sendpacket_crm_payment($payment) {

      global $woocommerce,$econnect_payment_packet_array;
     

      $econnect_disable = esc_attr( get_option('econnect_disable') );
      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );
      $connect_timeout = get_option('econnect_connect_timeout');
      $packet_timeout = get_option('econnect_packet_timeout');
      //make sure timeouts not less than minimum
      if ($connect_timeout < 5) $connect_timeout = 5;
      if ($packet_timeout < 10) $packet_timeout = 10;

      //Check if ManageMore processing should be skipped via
      //econnect_disable flag
      if ($econnect_disable=='1') {
         return;
      }

      if (esc_attr( get_option('econnect_ssl') ) == '1') {
        $socket = @fsockopen("ssl://".$host, $port, $errno, $errstr, $connect_timeout);
      }
      else {
        $socket = @fsockopen($host, $port, $errno, $errstr, $connect_timeout);
      }
      
      if (!$socket) {

         //Always attempt to Void Transaction on Gateway
         $submit_type='Void';
         $void_response = $this->_sendCCRequest_crm($submit_type,$payment);
   
         //Check if Void was approved, add note to Order
         if ($void_response->ApprovalStatus == 'APPROVED'){
            $void_text .= ' Credit Card payment was successfully voided.';
         } else {
            $void_text .= ' WARNING: Credit Card could not be voided.';
         }

         //Send Error to Admin
         $error_text = 'Error - Server not Responding. ' .$void_text.'Customer: '.$payment['customer_name'].'('.$payment['cust_id'].') Amount: '.number_format($payment['amount'],2);
         econnect_send_admin_email($error_text,'Customer Payment');

			   // Add notice
         $response_msg_to_customer = 'Your payment could not be posted because the Server is not Responding. '.$void_text;
         $this->eConnectError=true;
         wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
         return ;

      
      } else {

          //Socket connection established with eConnect Server.
          //Create Packet using eConnect Pipe-Delimited protocol and send
          
          $t=time(); //for web id


          //Set Payment Info -------------------------------------------------------------------
          if ($this->token_value!= ''){
          	 //card info from token
             $token_id = $this->token_id;
             $token    = WC_Payment_Tokens::get( $token_id );
             $cardnum = $token->get_last4();
             $this->card_type= $token->get_card_type();
             switch ($this->card_type){
                 case "amex":
                    $cardtype = "American Express";
                   break;
                 case "visa":
                    $cardtype = "Visa";
                    break;
                 case "mastercard":
                    $cardtype = "MasterCard";
                    break;
                 case "discover":
                    $cardtype = "Discover";
                    break;
                 default:
                    $cardtype = "Credit Card";
                    break;
             }
             $expdate = $token->get_expiry_month() . substr($token->get_expiry_year(),-2);
         	
          }
          else {
          	 //card info entered
             $cardnum = $_POST[$this->id.'-card-number'];
             $firstdigit = substr($cardnum, 0, 1);
             switch ($firstdigit){
                 case "3":
                    $cardtype = "American Express";
                    $this->card_type = 'amex';
                    break;
                 case "4":
                    $cardtype = "Visa";
                    $this->card_type = 'visa';
                    break;
                 case "5":
                    $cardtype = "MasterCard";
                    $this->card_type = 'mastercard';
                    break;
                 case "6":
                    $cardtype = "Discover";
                    $this->card_type = 'discover';
                    break;
                 default:
                    $cardtype = "Credit Card";
                    break;
             }
             $expdate = str_replace( array( '/', ' '), '', $_POST[$this->id.'-card-expiry'] );
          	
          }


          //Create Payment Packet Array ---------------------------------------------------------------------------

          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];


        	$lid = $payment['cust_id'];
        	$ltp = "1";
          $econnect_payment_packet_array = array(
             'WID' => $t ,
             'LID' => $lid,
             'LTP' => $ltp,
             'UTP' => '3',
             'TOT' => $payment['amount'],
             'CN' => substr($payment['company_name'], 0, 40) ,
             'FN' => substr($payment['first_name'], 0, 40) ,
             'LN' => substr($payment['last_name'], 0, 40) ,
             'BA1' => substr($payment['bill_addr_1'], 0, 40) ,
             'BA2' => substr($payment['bill_addr_2'], 0, 40) ,
             'BC' => substr($payment['bill_city'], 0, 25) ,
             'BS' => substr($payment['bill_state'],0,3) ,
             'BZ' => substr($payment['bill_zip'], 0, 13) ,
             'EM' => substr($payment['bill_email'], 0, 80) ,
             'CCT' => $cardtype ,
             'CCN' => substr($cardnum,-4) ,
             'CCX' => substr($expdate,0,2) . '/' . substr($expdate,2,2) ,
             'AC' => $this->auth_code ,
             'GR' =>  $this->gateway_refid,
             'PPW' => esc_attr( get_option('econnect_password') ) ,
             'BUA' => $user_agent,
             'RIP' => $ip_address,
             'D' => date("m/d/y",$t) ,
             'T' => date("h:iA",$t)
          );

                   
          /*
          Add custom hook "econnect_before_sending_payment"
          Use this hook to do something custom to the econnect_payment_packet_array that will be used
          to create the packet to send to eConnect. (e.g, update Status Request, etc.)
          */
          //add hook
          do_action('econnect_before_sending_payment',$payment);


          //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
          $in = '';
          foreach ($econnect_payment_packet_array as $array_key => $array_value ) {
           	 $in .= '|'. $array_key . '=' . $array_value ;
          } 
          $in = '1004' . $in . chr(4);  //add packet type, end with chr(4)



          fwrite($socket, $in);
          stream_set_timeout($socket, $packet_timeout);
            
      
          // Get Response
          $response = "";
          $i=0;
          do
          { 
            $out = fgets($socket,2);
            $info = stream_get_meta_data($socket);
            $response .= $out;
            $i++;
          } 
          while($out != chr(4) && $i<1000000 && $info['timed_out']==false);

          fclose($socket);

          if ($response != "") {

            if (strpos($response,"|S=1") === false ) {
               //Success Flag not returned
               //Get Error Message returned
               $errpos = strpos($response,"|E=");
               $errend = strpos($response,"|",$errpos+1);
               $errmsg = substr($response,$errpos+3,$errend-$errpos-3);

               //Void CC Transaction
               $submit_type='Void';
               $void_response = $this->_sendCCRequest_crm($submit_type,$payment);

               //Check if Void was approved, add note to Order
               if ($void_response->ApprovalStatus == 'APPROVED'){
                  $void_text .= ' Credit Card payment was successfully voided.';
               } else {
                  $void_text .= ' WARNING: Credit Card could not be voided.';
               }
   
               //Send Error to Admin
               $error_text = 'Error - '.$errmsg.'. ' .$void_text.'Customer: '.$payment['customer_name'].'('.$payment['cust_id'].') Amount: '.number_format($payment['amount'],2).' Card Info: '.$cardtype . '(****'.substr($cardnum,-4).')';
               econnect_send_admin_email($error_text,'Customer Payment');
   
               $response_msg_to_customer = 'Your payment could not be posted due to a Server Error.'.$void_text ;
               wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
               $this->eConnectError=true;
               return ;
               
               
            }
            else {
               //Parse Response
               $econnect_response_packet = $response;
               //a) Explode response by pipe delimiter
               $econnect_temp_array = explode("|",$econnect_response_packet);
               //b) For each item, add to packet array with Key, Value
               foreach ($econnect_temp_array as $econnect_value_pair) {
                  $econnect_value_pair_array = explode("=",$econnect_value_pair,2);
                  $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
               }

			         
			         //Set econnect reponse array
			         $this->econnect_response_array = $econnect_packet_array;
               return; //Success
               
            }
            
          }
          else {
            //Invalid response
            //Void CC Transaction
            $submit_type='Void';
            $void_response = $this->_sendCCRequest_crm($submit_type,$payment);
   
            //Check if Void was approved, add note to Order
            if ($void_response->ApprovalStatus == 'APPROVED'){
               $void_text .= ' Credit Card payment was successfully voided.';
            } else {
               $void_text .= ' WARNING: Credit Card could not be voided.';
            }

            //Send email to admin
            $error_text = 'Error: Invalid Server Response / Timeout. '.$void_text.'Customer: '.$payment['customer_name'].'('.$payment['cust_id'].') Amount: '.number_format($payment['amount'],2).' Card Info: '.$cardtype . '(****'.substr($cardnum,-4).')';
            econnect_send_admin_email($error_text,'Customer Payment');

   			    // Add notice to the cart
            $response_msg_to_customer = 'Your payment could not be posted due to an Invalid Response / Timeout.'.$void_text ;
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
            return ;
          }
      }
  }
	

  /**
   * Send Quantity Check packet to eConnect Server.
   */
  function _econnect_checkstock($order) {

      global $woocommerce;
     

      $econnect_disable = esc_attr( get_option('econnect_disable') );
      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );
      $connect_timeout = get_option('econnect_connect_timeout');
      $packet_timeout = get_option('econnect_packet_timeout');
      //make sure timeouts not less than minimum
      if ($connect_timeout < 5) $connect_timeout = 5;
      if ($packet_timeout < 10) $packet_timeout = 10;

      //Check if ManageMore processing should be skipped via
      //econnect_disable flag
      if ($econnect_disable=='1') {
         return;
      }

      if (esc_attr( get_option('econnect_ssl') ) == '1') {
        $socket = @fsockopen("ssl://".$host, $port, $errno, $errstr, $connect_timeout);
      }
      else {
        $socket = @fsockopen($host, $port, $errno, $errstr, $connect_timeout);
      }
      
      if (!$socket) {
            //Mark Order as Failed
            $error_text = 'Error: eConnect Server not Responding.';
            $order->update_status('failed', __($error_text , 'woothemes'));

   			// Add notice to the cart
            $response_msg_to_customer = 'There was a Server Communication Error - Please Try Again Later ';
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
      }
      else {

          //Socket connection established with eConnect Server.
          //Create Packet using eConnect Pipe-Delimited protocol and send
          
          $t=time();
          $oID = str_replace( "#", "", $order->get_order_number() );
          
          
          //Begin Packet -----------------------------------------------------------------------------
          $alllocations = (get_option('econnect_checkstock')=='all' ) ? '1' : '0' ;
          $econnect_order_packet_array = Array(
                    'WID' => $t ,
                    'ALOC' => $alllocations,
                    'LOC' => (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' 
                    );

                            
          //Order Item Details ------------------------------------------------------------------------
          $this->_econnect_orderdetails($order,$econnect_order_packet_array);

          //Finish Packet -----------------------------------------------------------------------------
          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];
          $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                    'PPW' => esc_attr( get_option('econnect_password') ) ,
                    'BUA' => $user_agent,
                    'RIP' => $ip_address,
                    'D' => date("m/d/y",$t) ,
                    'T' => date("h:iA",$t).chr(4)
                    ));


          //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
          $in = '';
          foreach ($econnect_order_packet_array as $array_key => $array_value ) {
           	 $in .= '|'. $array_key . '=' . $array_value ;
          } 
          $in = '1021' . $in . chr(4);  //add packet type, end with chr(4)


          fwrite($socket, $in);
          stream_set_timeout($socket, $packet_timeout);
            
      
          // Get Response
          $response = "";
          $i=0;
          do
          { 
            $out = fgets($socket,2);
            $info = stream_get_meta_data($socket);
            $response .= $out;
            $i++;
          } 
          while($out != chr(4) && $i<10000 && $info['timed_out']==false);

          fclose($socket);

          if ($response != "") {

            if (strpos($response,"|S=1") === false ) {
               //Success Flag not returned
               //Get Error Message returned
               $errpos = strpos($response,"|E=");
               $errend = strpos($response,"|",$errpos+1);
               $errmsg = substr($response,$errpos+3,$errend-$errpos-3);

               //Note: Error responses are expected on this packet, since error means out of stock, so we do not notify the Admin
               $error_text = 'We are sorry, but one or more items are not available at this time: '.$errmsg;
   
         		   // Add notice to the cart
               wc_add_notice( __('', 'woothemes') . $error_text, 'error' );
               $this->eConnectError=true;
               return ;
               
               
            }
            else {
               return; //Success
            }
            
          }
          else {
            //Invalid response

            //Mark Order as Failed
            $error_text = 'Error: Invalid Server Response.';
            $order->update_status('failed', __($error_text , 'woothemes'));
  
   			// Add notice to the cart
            $response_msg_to_customer = 'There was a Server Error Invalid Response - Please Try Again Later' ;
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
            return ;
          }

      }
      

  }
  
  /**
   * Build Order Details Part of Packet.
   */
  function _econnect_orderdetails($order,&$econnect_order_packet_array,$item_total) {
         
      global $woocommerce;


      $item_total = 0;
      $i=0;
      foreach( $order->get_items() as $item ){
             if ($i < 9) {
               $Zeros="00";
             }
             elseif ($i < 99) { 
               $Zeros="0";
             }
             else{
               $Zeros="";
             }
             $num=$i+1;
             $item_total += $order->get_item_total($item,false,true);
             
             

             // Check if product has variation.
             $product_variation_id = $item['variation_id'];
             
             if ($product_variation_id) { 
                $_product = new WC_Product($item['product_id']);
                $_product_variation = new WC_Product_Variation($item['variation_id']);
                $_parent_product = new WC_Product($item['product_id']);
                $skucode = $_product_variation->get_sku();
                if($skucode==''){
                  $skucode = $_parent_product->get_sku();
                }
             } else {
                $_product = new WC_Product($item['product_id']);
                $skucode = $_product->get_sku();
             }


             $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                  "SKU".$Zeros.$num  =>  substr($skucode, 0, 16) ,
                  "SKUQ".$Zeros.$num =>  $item['quantity'] ,
                  "SKUA".$Zeros.$num  =>   $item['subtotal']
                ));
                
             //If Variation, send attribute types/values, and add their names to description   
             if($product_variation_id) { 
                $variation_data = $_product_variation->get_variation_attributes();
                $attributes     = $_product_variation->parent->get_attributes();
                $description    = array();
                $anum=0;
                foreach($attributes as $attribute){
                    // Only deal with attributes that are variations 
                    if ( ! $attribute[ 'is_variation' ]  ) {
                       continue;
                    }
                    
                    $variation_selected_value = isset( $variation_data[ 'attribute_' . sanitize_title( $attribute[ 'name' ] ) ] ) ? $variation_data[ 'attribute_' . sanitize_title( $attribute[ 'name' ] ) ] : '';
                    $description_name         = esc_html( wc_attribute_label( $attribute[ 'name' ] ) );
                    $description_value        = __( 'Any', 'woocommerce' );

                    $anum++;
                    if ($anum>3){
                      break;
                    }

                    // Get terms for attribute taxonomy or value if its a custom attribute
                    if ( $attribute[ 'is_taxonomy' ] ) {
    
                        $post_terms = wp_get_post_terms( $_product->id, $attribute[ 'name' ] );
    
                        foreach ( $post_terms as $term ) {
                            if ( $variation_selected_value === $term->slug ) {
                                $description_value = esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) );
                            }
                        }
    
                    } else {
    
                        $options = wc_get_text_attributes( $attribute[ 'value' ] );
    
                        foreach ( $options as $option ) {
    
                            if ( sanitize_title( $variation_selected_value ) === $variation_selected_value ) {
                                if ( $variation_selected_value !== sanitize_title( $option ) ) {
                                    continue;
                                }
                            } else {
                                if ( $variation_selected_value !== $option ) {
                                    continue;
                                }
                            }
    
                            $description_value = esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) );
                        }
                    }

                    $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                        "SKAT".$anum.$Zeros.$num =>  $description_name ,
                        "SKAV".$anum.$Zeros.$num =>  $description_value 
                       ));
                    
                    $description[] = rawurldecode( $description_value );
                    
                }
                $variation_detail = ' ('. implode(', ',$description).')';
             }
             else {
                $variation_detail='';
             }

             $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                "SKUD".$Zeros.$num =>  $item['name'].$variation_detail ));

             $i++;
             
      }
      return;

  }
}
