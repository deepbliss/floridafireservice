<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * ManageMore Payment Gateway
 *
 * Provides a ManageMore Payment Gateway
 *
 * @class 		WC_Gateway_ManageMore_Cayan
 * @extends		WC_Payment_Gateway_CC
 * @version		2.17
 * @package		WooCommerce/Classes/Payment
 * @author 		Intellisoft Solutions, Inc.
 */
class WC_Gateway_ManageMore_Onaccount extends WC_Payment_Gateway_CC {

    /**
     * Constructor for the gateway.
     */
	public function __construct() {
		$this->id                 = 'managemore_onaccount';
		$this->icon               = null;
		$this->has_fields         = false;
		// Supports the default credit card form
		$this->supports = array( 'product' );
		$this->method_title       = __( 'ManageMore eConnect On Account', 'woocommerce' );
		$this->method_description = __( '<img src="' . plugins_url( 'images/mmpill.png', __FILE__ ) . '" ><br>Allows automatic transmission of On Account orders to ManageMore via eConnect.<br><br>
		                                 <a href="' . admin_url( 'admin.php?page=econnect-settings' ) . '">Plug-In Settings</a>', 'woocommerce' );

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

      // Turn these settings into variables we can use
		foreach ( $this->settings as $setting_key => $value ) {
			$this->$setting_key = $value;
		}
		// Define user set variables
		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option( 'instructions', $this->description );


    //Save Settings
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
    
  }

    /**
     * Initialise Gateway Settings Form Fields
     */
    public function init_form_fields() {
      
    	$this->form_fields = array(
			'enabled' => array(
				'title'   => __( 'Enable/Disable', 'woocommerce' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable ManageMore eConnect On Account', 'woocommerce' ),
				'default' => 'no'
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
				'default'     => __( 'On Account', 'woocommerce' ),
				'desc_tip'    => true,
			),
		);
    }


 



  /**
  * Process the payment and return the result
  *
  * @param int $order_id
  * @return array
  */
	public function process_payment( $order_id ) {
      global $woocommerce;
      


      //Get Order
		  $order = new WC_Order( $order_id );
		
		  //See if we should send a Check Stock Packet
		  if (get_option('econnect_checkstock')=='yes' || get_option('econnect_checkstock')=='all'){
		    $this->_econnect_checkstock($order);
          if ($this->eConnectError ==true){
            return;
          }
      }


      //Call Gateway
      unset($response);
      
      
												 
     //Send Order to eConnect
     $this->_econnect_sendpacket($order);
     if ($this->eConnectError ==true){
        return;
     }

     //All OK, approved and sent to eConnect

     //Update Post Meta with Gateway Ref Id (for refunds)
     update_post_meta( $order->id, '_econnect_gateway_refid', $this->gateway_refid, true );

     //Save Account Number on Customer Record
     $user = wp_get_current_user();
     $user_id = $user->ID ;
     $custid = $this->custid;
     if ($user_id) {
       update_user_meta( $user_id, '_econnect_custid' , $custid);
     }


		 // Empty the cart (Very important step)
		 $woocommerce->cart->empty_cart();


		 // Redirect to thank you page
   	 return array(
 			'result'   => 'success',
	   	'redirect' => $this->get_return_url( $order ),
	    );
			    
			    
	}



  /**
   * Send transaction packet to eConnect Server.
   */

  function _econnect_sendpacket($order) {

      global $woocommerce,$econnect_order_packet_array;
     

      $econnect_disable = esc_attr( get_option('econnect_disable') );
      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );

      //Check if ManageMore processing should be skipped via
      //econnect_disable flag
      if ($econnect_disable=='1') {
         $note_text = "eConnect Processing Disabled.  Order not transmitted.";
         $order->add_order_note( $note_text);
         return;
      }

      if (esc_attr( get_option('econnect_ssl') ) == '1') {
        $socket = fsockopen("ssl://".$host, $port, $errno, $errstr, 10);
      }
      else {
        $socket = fsockopen($host, $port, $errno, $errstr, 10);
      }
      
      if (!$socket) {
   	     // Just add note of error to the order for reference
         $error_text = 'eConnect Server not Responding.';
	       $order->add_order_note( 'eConnect Error: '. $error_text );
            
         return ;

      
      } else {

          //Socket connection established with eConnect Server.
          //Create Packet using eConnect Pipe-Delimited protocol and send
          
          $t=time(); //for web id
          $oID = str_replace( "#", "", $order->get_order_number() );

          //Set Tran Type
          if (get_option('econnect_transtype') == 'invoice'){
             $trantype = 1 ; //invoice
          }
          else {
             $trantype = 7 ;  //assume sales order
          }



          //Create Order Packet Array ---------------------------------------------------------------------------

          //Check Account Number on Customer Record, Set Lookup Id, Lookup Type
          $user = wp_get_current_user();
          $user_id = $user->ID ;
          if ($user_id) {
            $custid= get_user_meta( $user_id, '_econnect_custid' , true);
          }
          if ($custid!=''){
          	 $lid = $custid;
          	 $ltp = "1";
          }
          else {
          	 $lid = substr($order->billing_email, 0, 80);
          	 $ltp = "2";
          }

          //check if user id should be passed to eConnect to auto-register the user
          $user_info = get_userdata($user_id);
	        $user_login = $user_info->user_login;
          if (get_option('econnect_auto_register') =='1' && get_option('econnect_crm_enabled') == '1'){
          	 $auto_create_login = $user_login;
          }

          $econnect_order_packet_array = array(
             'WID' => $t ,
             'LID' => $lid,
             'LTP' => $ltp,
             'UTP' => '3',
             'TTP' => $trantype,
             'LANG' => '',
             'ALID' => $auto_create_login,
             'OID' => $oID,
             'CN' => substr($order->billing_company, 0, 40) ,
             'FN' => substr($order->billing_first_name, 0, 40) ,
             'LN' => substr($order->billing_last_name, 0, 40) ,
             'BA1' => substr($order->billing_address_1, 0, 40) ,
             'BA2' => substr($order->billing_address_2, 0, 40) ,
             'BC' => substr($order->billing_city, 0, 25) ,
             'BS' => substr($order->billing_state,0,3) ,
             'BZ' => substr($order->billing_postcode, 0, 13) ,
             'BCN' => $order->billing_country ,
             'P1' => substr($order->billing_phone, 0, 25) ,
             'EM' => substr($order->billing_email, 0, 80) ,
             'AST' => '1' ,
             'SCT' => substr($order->shipping_first_name, 0, 20) . ' ' . substr($order->shipping_last_name, 0, 20) ,
             'SCM' => substr($order->shipping_company, 0, 50) ,
             'SA1' => substr($order->shipping_address_1, 0, 40) ,
             'SA2' => substr($order->shipping_address_2, 0, 40) ,
             'SC' => substr($order->shipping_city, 0, 25) ,
             'SS' => substr($order->shipping_state,0,3) ,
             'SZ' => substr($order->shipping_postcode, 0, 13) ,
             'SCN' => $order->shipping_country ,
             'SP' => substr($order->billing_phone, 0, 25) ,
             'LOC' => (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' 
          );

          //Add Order Item Details -----------------------------------------------------------
          $this->_econnect_orderdetails($order,$econnect_order_packet_array,$item_total);


          //Final Order Info -----------------------------------------------------------------
          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];
          $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                'FRT' => $order->get_total_shipping() ,
                'DISC' => $order->get_total_discount(true) ,
                'SVIA' => $order->get_shipping_method( ) , 
                'TXC' => '' ,
                'TAX1' => $order->get_total_tax() ,
                'NOTE' => $order->customer_note ,
                'PPW' => esc_attr( get_option('econnect_password') ) ,
                'BUA' => $user_agent,
                'RIP' => $ip_address,
                'D' => date("m/d/y",$t) ,
                'T' => date("h:iA",$t)
                ));

          /*
          Add custom hook "econnect_before_sending_order"
          Use this hook to do something custom to the econnect_order_packet_array that will be used
          to create the packet to send to eConnect. (e.g, update Tax Code, etc.)
          */
          //add hook
          do_action('econnect_before_sending_order',$order,$econnect_order_packet_array);

                   

          //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
          $in = '';
          foreach ($econnect_order_packet_array as $array_key => $array_value ) {
           	 $in .= '|'. $array_key . '=' . $array_value ;
          } 
          $in = '1007' . $in . chr(4);  //add packet type, end with chr(4)



          fwrite($socket, $in);
            
      
          // Get Response
          $response = "";
          $i=0;
          do
          { 
            $out = fgets($socket,2);
            $response .= $out;
            $i++;
          } 
          while($out != chr(4) && $i<10000);

          fclose($socket);

          if ($response != "") {

            if (strpos($response,"|S=1") === false ) {
               //Success Flag not returned
               //Get Error Message returned
               $errpos = strpos($response,"|E=");
               $errend = strpos($response,"|",$errpos+1);
               $errmsg = substr($response,$errpos+3,$errend-$errpos-3);

               // Just add note of error to the order for reference
               $error_text = $errmsg.'.';
      	       $order->add_order_note( 'eConnect Error: '. $error_text );
               return ;
               
               
            }
            else {
               //Parse Response
               $econnect_response_packet = $response;
               //a) Explode response by pipe delimiter
               $econnect_temp_array = explode("|",$econnect_response_packet);
               //b) For each item, add to packet array with Key, Value
               foreach ($econnect_temp_array as $econnect_value_pair) {
                  $econnect_value_pair_array = explode("=",$econnect_value_pair,2);
                  $econnect_packet_array[$econnect_value_pair_array[0]] = $econnect_value_pair_array[1];
               }

			         // Add note to the order for reference
			         $success_text = 'Order successfully transmitted to ManageMore. Transction Id = '.$econnect_packet_array['TID'];
			         $order->add_order_note( $success_text);
			         
			         //Set CustId
			         $this->custid = $econnect_packet_array['CID'];
               return; //Success
               
            }
            
          }
          else {
            //Invalid response
      	    // Just add note of error to the order for reference
            $error_text = 'Invalid Server Response.';
	          $order->add_order_note( 'eConnect Error: '. $error_text );
            return ;
          }
      }
  }
	

  /**
   * Send Quantity Check packet to eConnect Server.
   */
  function _econnect_checkstock($order) {

      global $woocommerce;
     

      $econnect_disable = esc_attr( get_option('econnect_disable') );
      $host = esc_attr( get_option('econnect_ip_address') );
      $port = esc_attr( get_option('econnect_port') );

      //Check if ManageMore processing should be skipped via
      //econnect_disable flag
      if ($econnect_disable=='1') {
         return;
      }

      if (esc_attr( get_option('econnect_ssl') ) == '1') {
        $socket = fsockopen("ssl://".$host, $port, $errno, $errstr, 10);
      }
      else {
        $socket = fsockopen($host, $port, $errno, $errstr, 10);
      }
      
      if (!$socket) {
            //Mark Order as Failed
            $error_text = 'Error: eConnect Server not Responding.';
            $order->update_status('failed', __($error_text , 'woothemes'));

   			// Add notice to the cart
            $response_msg_to_customer = 'There was a Server Communication Error - Please Try Again Later ';
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
      }
      else {

          //Socket connection established with eConnect Server.
          //Create Packet using eConnect Pipe-Delimited protocol and send
          
          $t=time();
          $oID = str_replace( "#", "", $order->get_order_number() );
          
          
          //Begin Packet -----------------------------------------------------------------------------
          $alllocations = (get_option('econnect_checkstock')=='all' ) ? '1' : '0' ;
          $econnect_order_packet_array = Array(
                    'WID' => $t ,
                    'ALOC' => $alllocations,
                    'LOC' => (esc_attr( get_option('econnect_location') ) != '' && esc_attr( get_option('econnect_location') ) != '0') ? get_option('econnect_location') : '' 
                    );

                            
          //Order Item Details ------------------------------------------------------------------------
          $this->_econnect_orderdetails($order,$econnect_order_packet_array);

          //Finish Packet -----------------------------------------------------------------------------
          $geo = new WC_Geolocation();
          $ip_address = $geo->get_ip_address();
          $user_agent = $_SERVER['HTTP_USER_AGENT'];
          $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                    'PPW' => esc_attr( get_option('econnect_password') ) ,
                    'BUA' => $user_agent,
                    'RIP' => $ip_address,
                    'D' => date("m/d/y",$t) ,
                    'T' => date("h:iA",$t).chr(4)
                    ));


          //Create Pipe-delimited eConnect packet from array |CODE=VALUE|CODE=VALUE  etc.
          $in = '';
          foreach ($econnect_order_packet_array as $array_key => $array_value ) {
           	 $in .= '|'. $array_key . '=' . $array_value ;
          } 
          $in = '1021' . $in . chr(4);  //add packet type, end with chr(4)


          fwrite($socket, $in);
            
      
          // Get Response
          $response = "";
          $i=0;
          do
          { 
            $out = fgets($socket,2);
            $response .= $out;
            $i++;
          } 
          while($out != chr(4) && $i<10000);

          fclose($socket);

          if ($response != "") {

            if (strpos($response,"|S=1") === false ) {
               //Success Flag not returned
               //Get Error Message returned
               $errpos = strpos($response,"|E=");
               $errend = strpos($response,"|",$errpos+1);
               $errmsg = substr($response,$errpos+3,$errend-$errpos-3);

               //Note: Error responses are expected on this packet, since error means out of stock, so we do not notify the Admin
               $error_text = 'We are sorry, but one or more items are not available at this time: '.$errmsg;
   
         		   // Add notice to the cart
               wc_add_notice( __('', 'woothemes') . $error_text, 'error' );
               $this->eConnectError=true;
               return ;
               
               
            }
            else {
               return; //Success
            }
            
          }
          else {
            //Invalid response

            //Mark Order as Failed
            $error_text = 'Error: Invalid Server Response.';
            $order->update_status('failed', __($error_text , 'woothemes'));
  
   			// Add notice to the cart
            $response_msg_to_customer = 'There was a Server Error Invalid Response - Please Try Again Later' ;
            wc_add_notice( __('Payment error: ', 'woothemes') . $response_msg_to_customer, 'error' );
            $this->eConnectError=true;
            return ;
          }

      }
      

  }
  
  /**
   * Build Order Details Part of Packet.
   */
  function _econnect_orderdetails($order,&$econnect_order_packet_array,$item_total) {
         
      global $woocommerce;

      $item_total = 0;
      $i=0;
      foreach( $order->get_items() as $item ){
             if ($i < 9) {
               $Zeros="00";
             }
             elseif ($i < 99) { 
               $Zeros="0";
             }
             else{
               $Zeros="";
             }
             $num=$i+1;
             $item_total += $order->get_item_total($item,false,true);
             
             // Check if product has variation.
             $product_variation_id = $item['variation_id'];
             if ($product_variation_id) { 
                $_product = new WC_Product($item['product_id']);
                $_product_variation = new WC_Product_Variation($item['variation_id']);
                $_parent_product = new WC_Product($item['product_id']);
                $skucode = $_product_variation->get_sku();
                if($skucode==''){
                  $skucode = $_parent_product->get_sku();
                }
             } else {
                $_product = new WC_Product($item['product_id']);
                $skucode = $_product->get_sku();
             }
             

             $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                  "SKU".$Zeros.$num  =>  substr($skucode, 0, 16) ,
                  "SKUQ".$Zeros.$num =>  $item['quantity'] ,
                  "SKUA".$Zeros.$num  =>   $item['line_subtotal']
                ));
                
             //If Variation, send attribute types/values, and add their names to description   
             if($product_variation_id) { 
                $variation_data = $_product_variation->get_variation_attributes();
                $attributes     = $_product_variation->parent->get_attributes();
                $description    = array();
                $anum=0;
                foreach($attributes as $attribute){
                    // Only deal with attributes that are variations 
                    if ( ! $attribute[ 'is_variation' ]  ) {
                       continue;
                    }
                    
                    $variation_selected_value = isset( $variation_data[ 'attribute_' . sanitize_title( $attribute[ 'name' ] ) ] ) ? $variation_data[ 'attribute_' . sanitize_title( $attribute[ 'name' ] ) ] : '';
                    $description_name         = esc_html( wc_attribute_label( $attribute[ 'name' ] ) );
                    $description_value        = __( 'Any', 'woocommerce' );

                    $anum++;
                    if ($anum>3){
                      break;
                    }

                    // Get terms for attribute taxonomy or value if its a custom attribute
                    if ( $attribute[ 'is_taxonomy' ] ) {
    
                        $post_terms = wp_get_post_terms( $_product->id, $attribute[ 'name' ] );
    
                        foreach ( $post_terms as $term ) {
                            if ( $variation_selected_value === $term->slug ) {
                                $description_value = esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name ) );
                            }
                        }
    
                    } else {
    
                        $options = wc_get_text_attributes( $attribute[ 'value' ] );
    
                        foreach ( $options as $option ) {
    
                            if ( sanitize_title( $variation_selected_value ) === $variation_selected_value ) {
                                if ( $variation_selected_value !== sanitize_title( $option ) ) {
                                    continue;
                                }
                            } else {
                                if ( $variation_selected_value !== $option ) {
                                    continue;
                                }
                            }
    
                            $description_value = esc_html( apply_filters( 'woocommerce_variation_option_name', $option ) );
                        }
                    }

                    $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                        "SKAT".$anum.$Zeros.$num =>  $description_name ,
                        "SKAV".$anum.$Zeros.$num =>  $description_value 
                       ));
                    
                    $description[] = rawurldecode( $description_value );
                    
                }
                $variation_detail = ' ('. implode(', ',$description).')';
             }
             else {
                $variation_detail='';
             }

             $econnect_order_packet_array = array_merge ( $econnect_order_packet_array , Array (
                "SKUD".$Zeros.$num =>  $item['name'].$variation_detail ));

             $i++;
             
      }
      return;

  }
}
