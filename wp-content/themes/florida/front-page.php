<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!--slider-section-->
<section class="home-video">      
<div class="container">
     <div class="desktop-wrapper">       
        <div class="desktop-inner-wrapper"> 
       <iframe allow="autoplay" id="vp1HlhMu" title="Video Player" width="640" height="360" frameborder="0" 
        src="https://s3.amazonaws.com/embed.animoto.com/play.html?w=swf/production/vp1&e=1541090388&f=1fngUndBNzi01B1ePQZ70g&d=0&m=p&r=360p+480p&volume=100&start_res=0p&i=m&asset_domain=s3-p.animoto.com&animoto_domain=animoto.com&options=autostart/loop?rel=0&autoplay=1&muted=1" allowfullscreen></iframe>                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
        </div>
      </div>
     
     <div class="video-text">
       <h2>Total Fire Protection Service Provider</h2>
       <span>Professional, Affordable, Family Owned and Operated</span>
     </div>
</div>              
</section>

<!--featured-products-->
<section class="featured">
    <h2>Featured Products</h2>
    <div class="container">
        <ul class="product-carousel owl-carousel wow fadeInUp animated"> 
                           <?php
                           $args = array(
                           'post_type'   =>  'product',
                           'stock'       =>  1,
                           'showposts'   =>  10,
                           'orderby'     =>  'date',
                           'order'       =>  'ASC',
                           'meta_key' => 'featured_product',
                           'meta_value' => '1', 
           
                           );    
                           $loop = new WP_Query( $args );
                           while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
                           <li>
                                <figure><a href="<?php the_permalink(); ?>"><?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="" />'; ?></a></figure>
                                <div class="product-detail">
                                   <h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
                                  <div class="product-price"><span>Price: </span><?php echo $product->get_price_html(); ?></div>
                                 </div>                                    
                                <div class="look-btn">
                                    <form class="cart" method="post" enctype='multipart/form-data'><?php 
                                        //    do_action( 'woocommerce_before_add_to_cart_button' );
                                         do_action( 'woocommerce_after_shop_loop_item' ); ?>
                                    </form>
                                </div>
                           </li>
                           <?php endwhile; ?>
                           <?php wp_reset_query(); ?>
       </ul>
    </div>  
</section>

<!--safety starts-->
<section class="safety">
    <div class="container">
        <div class="safety-left wow fadeInLeft animated" style="visibility: visible;">   
            <h2><?php the_field("safety_heading"); ?></h2>                  
            <?php the_field("safety_content"); ?>   
        </div>
        
        <div class="safety-right wow fadeInRight animated" style="visibility: visible;">
        <div class="safety-right-inner">
            <h3>CONTACT US</h3>  
            <?php echo do_shortcode('[contact-form-7 id="20" title="Contact form 1"]');?>             
            <!-- <ul class="">
                <?php
                    $recent_posts = wp_get_recent_posts(array('numberposts'=>3, 'sattus'=>"publish")); 
                    $i=3;
                    foreach($recent_posts as $recent){ ?>
                    <li>   
                        <div class="post-text">
                        <h6><a href="<?php echo get_permalink($recent["ID"]); ?>"><?php echo $recent["post_title"] ?></a></h6>
                        </div>
                    </li>
                <?php  $i++; $i = $i+1;}       ?>  
            </ul>  -->  
             
         </div>
         <div class="safety-img">
                 <div class="inner-img headline-logo">              
                     <img src="<?php the_field('news_headline_image'); ?>" />   
                 </div>  
                 <div class="inner-img">                        
                   <img src="<?php the_field('news_qualify_image'); ?>" />
                 </div>  
            </div>    
            <div class="safety-right-bottom">
                <h3><?php the_field("title"); ?></h3>                  
               <?php the_field("address"); ?>               
           </div>  
        </div>
    </div> 
    
    
</section>

           

<?php get_footer();
