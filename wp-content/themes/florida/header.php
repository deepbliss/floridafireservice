<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content = "telephone=no">
<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1.0">
<meta name="description" content="Florida Fire Service">                                                              <!--
<link rel="icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.ico" type="images/ico">  -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/woocommerce.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/mediaquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.mmenu.all.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/animate.css">
<!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/css3-mediaqueries.js"  type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5shiv.js"  type="text/javascript"></script>
<![endif]-->



<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
    <!--Header Start Here-->
    <header class="header <?php if (!is_front_page()){?>inner-header<?php }?>">    
     <?php $image = get_field('header-bg', 'option'); ?>
        <div class="header-top" style="background-image:url('<?php echo $image ?>'); ">
            <div class="container">
                <div class="header-part">              
                <!--mobile-nav-->
                <div class="mobile-nav">
                             <a class="menu-btn" href="#menu">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/menu-icon.png" alt="Menu Icon" title="Menu Icon" 
                                    class="menu-icon" >
                            </a>
                            
                            <nav id="menu">
                                  <ul>
                                     <?php wp_nav_menu( array('menu' => 'main-menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
                                  </ul>
                            </nav>    
                </div>    
                
                <div class="header-left">
                <!--logo-->
                    <div class="logo">
                        <a href="<?php echo site_url(); ?>/">                           
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo.png" alt="Florida Fire Service" title="Florida Fire Service">                                  </a>
                    </div>                   
                </div>
                <div class="header-right">
                    <div class="top-part">
                        <div class="cart">
                            <a href="<?php echo site_url(); ?>/cart">shopping cart</a>
                        </div>
                        <ul>
                          <?php wp_nav_menu( array('menu' => 'Top-Menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
                        </ul>                        
                    </div>
                    
                    <div class="bottom-part">
                        <div class="call">
                            <?php $variable = get_field('call');?>
                            <p>Call us at<br/><span><?php the_field('call','option'); ?></span></p>
                        </div>
                        <div class="mobile-call">
                            <?php $variable = get_field('call');?>
                            <p><a href="tel: +1<?php $phone = get_field('call','option'); $output = str_replace(array('-', ")","(",' '), '', $phone); echo $output; ?>"><?php the_field('call','option'); ?></a></p>
                        </div>
                        <div class="header-img">
                          <?php $image = get_field('header-img', 'option'); ?>
                          <img src="<?php echo $image ?>" />
                          <?php $image2 = get_field('header-img2', 'option'); ?>
                          <img src="<?php echo $image2 ?>" />
                          <?php $image3 = get_field('header-img3', 'option'); ?>
                          <img src="<?php echo $image3 ?>" />                          
                        </div>           
                    </div>
                </div>  
            </div>
        </div>
        <!--Desktop Menu-->
        <div class="header-bottom desktop-menu">
            <div class="header-bottom desktop-menu">
              <nav>
                 <ul>                                                                      
                   <?php wp_nav_menu( array('menu' => 'main-menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
                 </ul>
              </nav>
           </div>   
       </div>
       </div>       
    </header>

