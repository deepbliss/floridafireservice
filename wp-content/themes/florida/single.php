<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="inner-con blog-detail">  
      <section class="inner-sec">
<div class="container">
<div class="breadcrumb">
        <ul>
               <li><a href="<?php echo site_url(); ?>">Home</a></li>
<?php if ( is_page() && $post->post_parent ){ ?>
<li><?php 
                          $top_page_url = get_permalink( end( get_ancestors( get_the_ID(), 'page' ) ) );
                          $parents = get_post_ancestors( $post->ID );
?> <a href="<?php echo $top_page_url; ?>"><?php echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); ?></a></li>
<?php } ?>
               <li class="current"><?php the_title(); ?></li>

           </ul>
 </div>
<div class="content-main">
<div class="inner-lft">
<div class="blog-inner">

    <?php while ( have_posts() ) : the_post(); ?>
        <div class="blog-block" id="post-<?php the_ID(); ?>">
        
        <div class="blog-txt-wrap blog_description">
        <h2><?php the_title( ); ?></h2>
                          
				<div class="blog-top">
				   
				   <span class="post-date"><?php the_time('d M, Y'); ?></span>
				  <div class="blog_border">
				   <span class="postedby"> <?php the_author_meta( 'display_name', 1 ); ?></span>
				   </div>
				   <div class="blog_border">
				   <?php //the_tags( '<span class="tags">Tags: ' , ', ', '</span>' ); ?>
				   <span class="num-of-commemts"><a href="<?php echo get_comments_link( $recent["ID"] );; ?>"><?php echo get_comments_number( $recent['ID'] ); ?></a></span>
				</div>
				</div>

				
                                <?php $img= get_the_post_thumbnail($post_id, 'full'); ?>
        
        <div class="blog-content"><?php the_content(); ?></div>
        </div>
        </div>
        
<?php 
if ( comments_open() || get_comments_number() ) {
comments_template();
} else {?><p class="no-comments"><?php  echo "Comments are closed.";?></p><?php }?>
<?php endwhile;?>
</div>
</div>
<div class="inner-rgt">
   
<?php dynamic_sidebar('sidebar'); ?>
</div>
    
</div>
</div>

</section>
</div>


<?php get_footer();
