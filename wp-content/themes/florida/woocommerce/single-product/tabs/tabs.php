<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>
<div class="detail-tabs">

<div id="parentHorizontalTab">
<ul>
<li><a href="#tab-1"><span>Description</span></a></li>
<!--<li><a href="#tab-2"><span>Additional Information</span></a></li>-->
<?php             
    $args = array ('post_type' => 'product','post_id' =>$product->id);
    $comments = get_comments( $args );
    $totalreviews = count($comments);
?>
<li><a href="#tab-2"><span>Additional Information</span></a></li>
<li><a href="#tab-3" id="tab2"><span>Reviews (<?php echo $totalreviews; ?>)</span></a></li>
</ul>
    
<div id="tab-1">
<div class="main-tabs-box">
<h4>Product Description</h4>
<?php the_content(); ?>
</div>
</div>

<!--<div id="tab-2">
<div class="main-tabs-box">
<h4>Additional Information</h4>
<?php //global $product; ?>
<?php //$product->list_attributes(); ?></div>
</div>-->

<div id="tab-2">
<div class="main-tabs-box">
<h4>Additional Information</h4>
<?php the_content(); ?>
</div>
</div>


<div id="tab-3">
<div class="main-tabs-box">
<h4>Reviews</h4>
<?php comments_template(); ?>
</div>
</div>


</div>

</div>
    

<?php endif; ?>
