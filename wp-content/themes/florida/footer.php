<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
</div>
 <footer class="footer" style="background-image:url('<?php the_field('header-bg', 'option'); ?>'); ">
      <div class="container">
                       <ul>
                          <?php wp_nav_menu( array('menu' => 'bottom-menu',  'items_wrap' => '%3$s', 'container' => 'false', )); ?>
                        </ul>     
                        
                        <div class="copyright">
                               <?php the_field('copyright','option'); ?>                        
                        </div>                   
     </div>                                      
</footer> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.min.all.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.responsiveTabs.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    
jQuery(".review-link a").click(function(){
   jQuery("#tab2").click();
});    
jQuery('#parentHorizontalTab').responsiveTabs({
rotate: false,
startCollapsed: 'accordion',
collapsible: 'accordion',
setHash: false,
//disabled: [3,4],
activate: function(e, tab) {
$('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
}
});
});
</script>

<!--Featured Products Script-->
<script>
$(document).ready(function(){
$('.product-carousel').owlCarousel({
loop: false,
items: 5,       
autoplay: true,
smartSpeed: 2500,
rewindNav:false,
autoHeight: true,
nav: false,
responsive: { 
1280: {    
loop: true,  
items: 5,
},
1024: {        
items: 3,
},  
640: {
margin: 20,
items: 2,
}, 
567: {
margin: 10,
items: 2,
}, 
0: {
items: 1,
margin: 0,
},
}
});
});
</script>   

<!--sticky header-->
<script type="text/javascript">
$(window).scroll(function() {
if ($(this).scrollTop() > 1){  
    $('.header-bottom').addClass("sticky");
  }
  else{
    $('.header-bottom').removeClass("sticky");
  }
});
</script> 

<!--<script type="text/javascript">
if($(window).width() <= 1023){
  // do your stuff
  $(window).scroll(function() {
if ($(this).scrollTop() > 1){  
    $('header').addClass("sticky");
  }
  else{
    $('header').removeClass("sticky");
  }
});
}
</script>    -->

<!--mobile menu-->
<script>
$(function() {
$('nav#menu').mmenu({
extensions : [ 'effect-slide-menu', 'pageshadow' ],
searchfield : false,
counters : false,
navbar : {
title : 'Menu'
},
navbars : [
{
position : 'top',
<!--content : [ 'searchfield' ]-->               
}, {
position : 'top',
content : [
'prev',
'title',
'close'
]
}
]
});
});
</script>                           
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<script type="text/javascript">  
  $(document).ready(function($) {
     
      $('form.register').validate({
          rules: {  
              sr_firstname: {
                  required: true
              },
              sr_lastname: {
                  required: true
              },
              email: {
                  required: true,
                  email: true
              },
              password: {
                  required: true,
                  minlength: 12
              },
          },
         
          messages: {
              sr_firstname: "Please enter first name.",
              sr_lastname: "Please enter last name.",
              email: "Please enter a valid email address.",
              password: "Password required minimum 12 characters.",               
          },
          errorElement: "div",
          errorPlacement: function(error, element) {
              element.after(error);
          }
      });
 });
</script>                    
<script type="text/javascript">
$(document).ready(function () {
    
jQuery(".review-link a").click(function(){
   jQuery("#tab2").click();
});    
jQuery('#parentHorizontalTab').responsiveTabs({
rotate: false,
startCollapsed: 'accordion',
collapsible: 'accordion',
setHash: false,
//disabled: [3,4],
activate: function(e, tab) {
$('.info').html('Tab <strong>' + tab.id + '</strong> activated!');
}
});
});
</script>        

<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = '<?php echo site_url();?>/thank-you';
}, false );
</script>
<script>
        $window = $(window);
        $window.scroll(function() {
          $scroll_position = $window.scrollTop();
          if ($(window).width() <= 1023 ){
            if ($scroll_position > 1) { // if body is scrolled down by 300 pixels
                $('header').addClass('sticky');  
                // to get rid of jerk
                header_height = $('header').innerHeight();
                
            } else {
                $('body').css('padding-top' , '0');
                $('header').removeClass('sticky');
            }
          }
         });
                   
         </script>
         
<?php wp_footer(); ?>


</body>
</html>
