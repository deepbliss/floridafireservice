<?php
/**
 * Twenty Fifteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since Twenty Fifteen 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * Twenty Fifteen only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfifteen_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since Twenty Fifteen 1.0
 */
function twentyfifteen_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 * to change 'twentyfifteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Middle Menu', 'twentyfifteen' ),
		'social'  => __( 'Top Menu', 'twentyfifteen' ),
		'footer'  => __( 'Footer Links Menu', 'twentyfifteen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */

}
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );


function mytheme_add_admin()
{
	
	global $themename, $shortname, $options;
	
	 if ( isset( $_GET['page'] ) && $_GET['page'] == basename(__FILE__) ) {
		
		if ( !empty( $_REQUEST['action'] ) && 'save' == $_REQUEST['action'] ) {
			
			foreach ($options as $value) {
				if (isset($_REQUEST[$value['id']])) {
					update_option($value['id'], $_REQUEST[$value['id']]);
				} else {
					delete_option($value['id']);
				}
			}
			header("Location: admin.php?page=functions.php&saved=true");
			die;
			
		      } else if( !empty( $_REQUEST['action'] ) && 'reset' == $_REQUEST['action'] ) {
			
			foreach ($options as $value) {
				delete_option($value['id']);
			}
			
			header("Location: admin.php?page=functions.php&reset=true");
			die;
			
		}
	}
	
	add_theme_page($themename, $themename, 'administrator', basename(__FILE__), 'mytheme_admin');
}
function my_admin_scripts() {
	$screen = get_current_screen();
	if ( isset( $screen->base ) && $screen->base == 'appearance_page_functions' && is_admin()) {
		$file_dir = get_bloginfo('template_directory');
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_register_script('my-upload', $file_dir."/functions/my-script.js", array('jquery','media-upload','thickbox'));
		wp_enqueue_script('my-upload');
	}
}

function my_admin_styles() {
	if(is_admin()) {
		wp_enqueue_style('thickbox');
	}
}
add_action('admin_print_scripts', 'my_admin_scripts');
add_action('admin_print_styles', 'my_admin_styles');
function mytheme_add_init()
{
	$file_dir = get_bloginfo('template_directory');
	wp_enqueue_style("functions", $file_dir."/functions/functions.css", false, "1.0", "all");
	
	$file_dir = get_bloginfo('template_directory');
	wp_enqueue_style("functions", $file_dir."/functions/functions.css", false, "1.0", "all");
	wp_enqueue_script("rm_script", $file_dir."/functions/rm_script.js", false, "1.0");
	add_theme_support('post-thumbnails');
}

function mytheme_admin() {
	global $themename, $shortname, $options;
	$i=0;
  
	 if ( !empty( $_REQUEST['saved'] ) && $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' options have been saved.</strong></p></div>';
    if ( !empty( $_REQUEST['reset'] ) && $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' options have been reset to their default settings.</strong></p></div>';
?>
		<div class="wrap rm_wrap">
		<h2><?php echo $themename; ?> Settings</h2>
		<div class="rm_opts">
		<form method="post">
			<?php 
				foreach ($options as $value) {
					switch ( $value['type'] ) {
						case "open":
						break;
  
						case "close":
			?>
								</div>
							</div>
							<br />  
					<?php 
						break;
						case "title":
					?>
					<p>To easily use the <?php echo $themename;?> theme, you can use the menu below.</p>
					<?php 
						break;
						case 'text':
					?>
						<div class="rm_input rm_text">
							<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
							<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id'])  ); } else { echo $value['std']; } ?>" />
							<small><?php echo $value['desc']; ?></small>
							<div class="clearfix"></div>
						</div>
					<?php
						break;
						case 'textarea':
					?> 
							<div class="rm_input rm_textarea">
								<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
								<textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="" rows=""><?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id']) ); } else { echo $value['std']; } ?></textarea>
								<small><?php echo $value['desc']; ?></small>
								<div class="clearfix"></div>
							</div>
					<?php
						break;						  
						case 'select':
					?>
 
							<div class="rm_input rm_select">
								<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
								<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
									<?php 
										foreach ($value['options'] as $option) { ?>
											<option <?php if (get_option( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option>
									<?php } ?>
								</select>							 
								<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
							</div>
					<?php
						break;
						case "checkbox":
					?>
 
						<div class="rm_input rm_checkbox">
							<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
							<?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
							<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
						 	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
						 </div>
						 <?php
						 break;
						 case 'upload':
						 	$ar = get_site_url('', '/' ,'');
							?>
							<div class="rm_input rm_upload">
							<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
							<input id="<?php echo $value['id']; ?>" type="text" size="36" value="<?php if ( get_option( $value['id'] ) != "") { echo str_replace($ar,'',(get_option( $value['id']) ) ); } else { echo $value['std']; } ?>" name="<?php echo $value['id']; ?>" />
							<input id="upload_logo_button" type="button" value="Upload Image" class="button button-primary"/>
							</div>
							
							<?php 

						break;
						case "section":
							$i++;
					?>
 
						<div class="rm_section">
							<div class="rm_title"><h3><input type='button' value='+' class="close button button-primary" ><?php echo $value['name']; ?></h3>
								<span class="submit"><input name="save<?php echo $i; ?>" type="submit" value="Save changes" class="button button-primary"/></span>
								<div class="clearfix"></div>
							</div>
							<div class="rm_options">
					<?php 
						break;  
						case "":
					?>
						<div class="rm_input title"><h3><?php echo $value['name']; ?></h3><div class="clearfix"></div></div>
					<?php
					}
				}
			?>
			<input type="hidden" name="action" value="save" />
		</form>
		<form method="post">
			<p class="submit reset" style="float:right;">
				<input name="reset" type="submit" value="Reset" class="button button-primary" />
			</p>
		</form>
	</div> 
<?php 
}

if (function_exists('register_sidebar')) {
    
    register_sidebar(array(
    'name' => 'Sidebar',
    'id' => 'sidebar',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => ''
    ));
    
   
}

add_action('admin_init', 'mytheme_add_init');
add_action('admin_menu', 'mytheme_add_admin');


//regster multiple sidebar
/*if (function_exists('register_sidebar')) {
	register_sidebar(array(
	'name' => 'Search',
	'id' => 'search',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => ''
	));
	
	register_sidebar(array(
	'name' => 'Brochures Left',
	'id' => 'brochures_left',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => ''
	));
	
	register_sidebar(array(
	'name' => 'Brochures Right',
	'id' => 'brochures_right',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => ''
	));
	
	register_sidebar(array(
	'name' => 'Address',
	'id' => 'address',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => ''
			));
	
	register_sidebar(array(
	'name' => 'Current project address',
	'id' => 'current-project-address',
	'before_widget' => '',
	'after_widget' => '',
	'before_title' => '',
	'after_title' => ''
			));
	
}*/

add_action('init', 'register_my_custom_post_type_slider_post');
function register_my_custom_post_type_slider_post()
{
	register_post_type('slider', array(
	'label' => 'Slider',
	'description' => 'This will allow you to add slides.',
	'public' => false,
	'show_ui' => true,
	'show_in_menu' => true,
	'capability_type' => 'post',
	'map_meta_cap' => true,
	'hierarchical' => false,
	'rewrite' => false,
	'query_var' => true,
	'supports' => array(
	'title',
	'editor',
	'thumbnail',
	),
	'labels' => array(
	'name' => 'Slider',
	'singular_name' => 'Slide',
	'menu_name' => 'Slide',
	'add_new' => 'Add Slide',
	'add_new_item' => 'Add New Slide',
	'edit' => 'Edit',
	'edit_item' => 'Edit Slide',
	'new_item' => 'New Slide',
	'view' => 'View Slide',
	'view_item' => 'View Slide',
	'search_items' => 'Search Slide',
	'not_found' => 'No Slide Found',
	'not_found_in_trash' => 'No Slide Found in Trash',
	'parent' => 'Parent Slide'
			)
	));
}
function home_slider() {
	$args = array( 'post_type' => 'slider', 'posts_per_page' => 100, 'order' => 'ASC' );
	$the_query = new WP_Query($args);
	if($the_query -> have_posts())
	{ ?>
		 <ul class="home-slider">
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full', true, '' ); ?>
				<li style="background:url(<?php echo $src[0] ?>) no-repeat top center">
					<?php $slidetxt = get_the_content(); 
					if($slidetxt != "") { ?>
					 <div class="slider-text">
						<div class="container">
							<?php echo the_content(); ?>
						 </div>
						</div> 
     				<?php } ?>
				</li>
			<?php endwhile; ?>
		</ul>
	<?php }
	wp_reset_query();
	return;
}
add_shortcode( 'slider', 'home_slider' );

add_action('init', 'register_my_custom_post_type_team_post');
function register_my_custom_post_type_team_post()
{
	register_post_type('team', array(
	'label' => 'Team',
	'description' => 'This will allow you to add Team.',
	'public' => false,
	'show_ui' => true,
	'show_in_menu' => true,
	'capability_type' => 'post',
	'map_meta_cap' => true,
	'hierarchical' => false,
	'rewrite' => false,
	'query_var' => true,
	'supports' => array(
	'title',
	'editor',
	'thumbnail',
	),
	'labels' => array(
	'name' => 'Team',
	'singular_name' => 'Team',
	'menu_name' => 'Team',
	'add_new' => 'Add Team',
	'add_new_item' => 'Add New Team',
	'edit' => 'Edit',
	'edit_item' => 'Edit Team',
	'new_item' => 'New Team',
	'view' => 'View Team',
	'view_item' => 'View Team',
	'search_items' => 'Search Team',
	'not_found' => 'No Team Found',
	'not_found_in_trash' => 'No Team Found in Trash',
	'parent' => 'Parent Team'
			)

	));
}
function team_members() {
	$args = array( 'post_type' => 'team', 'posts_per_page' => 100, 'order' => 'ASC' );
	$the_query = new WP_Query($args);
	if($the_query -> have_posts())
	{
		echo '<ul>';
		while ( $the_query->have_posts() ) : $the_query->the_post();
		?>
		<li>
<div class="member-img wow delay5 animated fadeInLeft"><?php the_post_thumbnail( "full" ); ?></div>
<div class="member-detail wow delay5 animated fadeInRight">
<h4><?php the_title(); ?></h4>
<span><?php the_field('team_designation') ?></span>
<?php the_content(); ?>
<a href="<?php the_field('team_fb_link') ?>" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/facebook2.png"></a>
</div>
</li>
		<?php 
		endwhile;
		echo '</ul>';
	}
	wp_reset_query();
	return;
}
add_shortcode( 'teams', 'team_members' );


add_action('init', 'register_my_custom_post_type_projects_post');
function register_my_custom_post_type_projects_post()
{
	register_post_type('projects_post', array(
	'label' => 'Projects',
	//'taxonomies' => array('category'),
	'description' => 'This will allow you to add projects.',
	'public' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'capability_type' => 'post',
	'map_meta_cap' => true,
	'hierarchical' => false,
	'rewrite' => array(
	'slug' => 'projects_post',
	'with_front' => true
	),
	'query_var' => true,
	'supports' => array(
	'title',
	'editor',
	'thumbnail',
	'excerpt',
	'custom-fields'
			),
			'labels' => array(
			'name' => 'Projects',
			'singular_name' => 'Project',
			'menu_name' => 'Projects',
			'add_new' => 'Add Projects',
			'add_new_item' => 'Add New Projects',
			'edit' => 'Edit',
			'edit_item' => 'Edit Projects',
			'new_item' => 'New Projects',
			'view' => 'View Projects',
			'view_item' => 'View Projects',
			'search_items' => 'Search Projects',
			'not_found' => 'No Projects Found',
			'not_found_in_trash' => 'No Projects Found in Trash',
			'parent' => 'Parent Projects'
					)

	));
}
function projects_taxonomy() {
	register_taxonomy(
	'projects_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
	'projects_post',   		 //post type name
	array(
	'hierarchical' 		=> true,
	'label' 			=> 'Projects Category',  //Display name
	'query_var' 		=> true,
	'rewrite'			=> array(
	'slug' 			=> 'projects_categories', // This controls the base slug that will display before each term
	'with_front' 	=> false // Don't display the category base before
	)
	)
	);
}
add_action( 'init', 'projects_taxonomy');

add_filter( 'manage_taxonomies_for_projects_post_columns', 'projects_post_type_columns' );
function projects_post_type_columns( $taxonomies ) {
	$taxonomies[] = 'projects_categories';
	return $taxonomies;
}
function featured_projects()
{
	//for a given post type, return given category posts.
	 $post_type = 'projects_post';
	 $tax = 'projects_categories';
	 $tax_terms = get_terms($tax);
	 if ($tax_terms) {
	 	$args=array(
	 			'post_type' => $post_type,
	 			"$tax" => 'Featured Projects',
	 			'post_status' => 'publish',
	 			'posts_per_page' => 15,
	 			'ignore_sticky_posts'=> 1
	 	);
		$my_query = null;
		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) { ?>
		<div class="wraper">
			<div class="section-title"><h2>Featured Projects</h2></div>
			<div class="project-slider">
				<ul class="bxslider">
					<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
						<li>
							<div class="project-slider-lft">
								<?php the_post_thumbnail( array(600, 351) ); ?>
							</div>
							<div class="project-slider-rgt">
								<h3><?php the_title(); ?></h3>
								<p><?php  the_excerpt(); ?></p>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			</div>
		</div>	
		<?php } ?>		
<?php }
 }
wp_reset_query();	
add_shortcode('featuredprojects', 'featured_projects');


function past_upcomming_projects($atts, $content = null)
{
	$a = shortcode_atts(array(
			'category' => '',
	), $atts);
	
	//for a given post type, return given category posts.
	$post_type = 'projects_post';
	$tax = 'projects_categories';
	$tax_terms = get_terms($tax);
	if ($tax_terms) {
		$args=array(
				'post_type' => $post_type,
				"$tax" => $a['category'],
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'ignore_sticky_posts'=> 1,
				'orderby' => 'date',
				'order' => 'ASC'
				
		);
		$my_query = null;
		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) { ?>
		<ul>
			<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
			<?php $detaillink = get_field('show_project_detail_link'); ?>
				<li class="wow delay5 animated fadeInDown">
					<div class="projects2-img">
						<?php //$image = get_the_post_thumbnail( array(355, 237) );
						 $src = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), array( 355,237 ), false, '' ); ?>
						 <?php if( in_array('Yes', $detaillink) )  { ?><a href="<?php the_permalink(); ?>" ><?php } else {  ?>
						<a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo $src[0]; ?>" title="<?php the_title(); ?>"><?php } ?>
							<img src="<?php echo $src[0]; ?>" title="<?php the_title(); ?>">
							<div class="mask wow animated fadeIn">
								<div class="mask-content">
									<span><?php  the_title();  ?></span>
								</div>
							</div>
						</a>
					</div>
					<div class="project-name"><?php if( in_array('Yes', $detaillink) )  { ?><a href="<?php the_permalink(); ?>" ><?php  the_title();  ?></a><?php } else { the_title(); } ?></div>
				</li>
			<?php endwhile; ?>
		</ul>
		<?php } ?>		
<?php }
 }
wp_reset_query();	
add_shortcode('pastupcommingprojects', 'past_upcomming_projects');
add_filter('the_content', 'do_shortcode');

function homeurl_shortcode($atts, $content = null)
{
	$siteurl = get_template_directory_uri();
	return $siteurl;
}
add_shortcode('theme_url', 'homeurl_shortcode');
add_filter('widget_text', 'do_shortcode');

function baseurl_shortcode($atts, $content = null)
{
	$siteurl = get_site_url();
	return $siteurl;
}
add_shortcode('site_url', 'baseurl_shortcode');
add_filter('widget_text', 'do_shortcode');


function my_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo() { ?>
    <style type="text/css">
         body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
            padding-bottom: 0px;
            margin-bottom: 0px;
            background-size: 257px;
            width: 100%;
            height: 85px;
            margin-top:50px;
        }                                                                        
        body.login #login {padding-top: 1%;}
        li#toplevel_page_functions .wp-menu-image.dashicons-before.dashicons-admin-generic{
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.jpg);
        }
        body, html{height: auto;}
    
       /* body.login div#login h1 a {
        	background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
			padding-bottom: 5px;
			margin-bottom: 0px;
			background-size: 95px;
			width: 100%;
			height: 97px;
        }
		body.login.login-action-login.wp-core-ui.locale-en-us{background: #ccc}
		body.login #login {padding-top: 1%;}
		li#toplevel_page_functions .wp-menu-image.dashicons-before.dashicons-admin-generic{
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.jpg);
		}
		body, html{height: auto;}*/
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url_title() {
     return get_option('blogname'); 
} 
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

//remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');
//remove_filter( 'the_content', 'wpautop' );

function wpse_excerpt_length( $length ) {
	return 165;
}
add_filter( 'excerpt_length', 'wpse_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '... <a class="read-more" href="' . get_permalink( get_the_ID() ) . '">' . __( 'Read More', 'your-text-domain' ) . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );



function comment_form_new( $args = array(), $post_id = null ) {
	if ( null === $post_id )
		$post_id = get_the_ID();

	$commenter = wp_get_current_commenter();
	$user = wp_get_current_user();
	$user_identity = $user->exists() ? $user->display_name : '';

	$args = wp_parse_args( $args );
	if ( ! isset( $args['format'] ) )
		$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

	$req      = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$html5    = 'html5' === $args['format'];
	$fields   =  array(
			'author' => '<div class="comment-form-top-lft"><div class="blog-row"><label><p>Name </p><span>*</span></label>' .
			'<input id="author" class="name-field" name="author" placeholder="Name*" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
			'email'  => '<div class="comment-form-email"><div class="blog-row"><label><p>Email </p><span>*</span></label>' .
			'<input id="email" placeholder="Email*" class="email-field" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
			'url' => '<div class="blog-row"><label>Website</label>' .
    		'<input id="url" placeholder="Website" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .'"  /></div></div></div>',
	);

	$required_text = sprintf( ' ' . __('Required fields are marked %s'), '<span class="required">*</span>' );

	/**
	 * Filter the default comment form fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $fields The default comment fields.
	*/
	$fields = apply_filters( 'comment_form_default_fields', $fields );
	$defaults = array(
			'fields'               => $fields,
			'comment_field'        => '<div class="comment-form-top-rgt"><div class="blog-row"><label>Your Comments</label><textarea id="comment" name="comment" class="form-control" rows="3"  aria-required="true" required="required">Your Comments</textarea></div></div>',
			/** This filter is documented in wp-includes/link-template.php */
			'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
			/** This filter is documented in wp-includes/link-template.php */
			'logged_in_as'         => '<div class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</div>',
			'comment_notes_before' => '<div class="comment-notes">' . __( 'Your email address will not be published.' ) . '</div>',
			'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
			'id_form'              => 'commentform',
			'id_submit'            => 'submit',
			'name_submit'          => 'submit',
			'title_reply'          => __( 'Add a comment' ),
			'title_reply_to'       => __( 'Leave a Reply to %s' ),
			'cancel_reply_link'    => __( 'Cancel reply' ),
			'label_submit'         => __( 'Post' ),
			'format'               => 'xhtml',
	);

	/**
	 * Filter the comment form default arguments.
	 *
	 * Use 'comment_form_default_fields' to filter the comment fields.
	 *
	 * @since 3.0.0
	 *
	 * @param array $defaults The default comment form arguments.
	*/
	$args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

	?>
		<?php if ( comments_open( $post_id ) ) : ?>
			<?php
			/**
			 * Fires before the comment form.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_before' );
			?>
			<div id="respond" class="comment-respond">
				<span><h3 id="reply-title" class="comment-reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'], false ); ?> <small><?php cancel_comment_reply_link( $args['cancel_reply_link'] ); ?></small></h3></span>
				<?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
					<?php echo $args['must_log_in']; ?>
					<?php
					/**
					 * Fires after the HTML-formatted 'must log in after' message in the comment form.
					 *
					 * @since 3.0.0
					 */
					do_action( 'comment_form_must_log_in_after' );
					?>
				<?php else : ?>
				<div class="add-comment-form">
					<form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="comment-form"<?php echo $html5 ? ' novalidate' : ''; ?>>
						<div class="comment-form-top"><?php
						/**
						 * Fires at the top of the comment form, inside the <form> tag.
						 *
						 * @since 3.0.0
						 */
						do_action( 'comment_form_top' );
						?>
						<?php if ( is_user_logged_in() ) : ?>
							<?php
							/**
							 * Filter the 'logged in' message for the comment form for display.
							 *
							 * @since 3.0.0
							 *
							 * @param string $args_logged_in The logged-in-as HTML-formatted message.
							 * @param array  $commenter      An array containing the comment author's
							 *                               username, email, and URL.
							 * @param string $user_identity  If the commenter is a registered user,
							 *                               the display name, blank otherwise.
							 */
							echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );
							?>
							<?php
							/**
							 * Fires after the is_user_logged_in() check in the comment form.
							 *
							 * @since 3.0.0
							 *
							 * @param array  $commenter     An array containing the comment author's
							 *                              username, email, and URL.
							 * @param string $user_identity If the commenter is a registered user,
							 *                              the display name, blank otherwise.
							 */
							do_action( 'comment_form_logged_in_after', $commenter, $user_identity );
							?>
						<?php else : ?>
							<?php //echo $args['comment_notes_before']; ?>
							<?php
							/**
							 * Fires before the comment fields in the comment form.
							 *
							 * @since 3.0.0
							 */
							do_action( 'comment_form_before_fields' );
							foreach ( (array) $args['fields'] as $name => $field ) {
								/**
								 * Filter a comment form field for display.
								 *
								 * The dynamic portion of the filter hook, $name, refers to the name
								 * of the comment form field. Such as 'author', 'email', or 'url'.
								 *
								 * @since 3.0.0
								 *
								 * @param string $field The HTML-formatted output of the comment form field.
								 */
								echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
							}
							/**
							 * Fires after the comment fields in the comment form.
							 *
							 * @since 3.0.0
							 */
							do_action( 'comment_form_after_fields' );
							?>
						<?php endif; ?>
						<?php if ( is_user_logged_in() ) {?><div class="user-loged-commnt"><?php }
						/**
						 * Filter the content of the comment textarea field for display.
						 *
						 * @since 3.0.0
						 *
						 * @param string $args_comment_field The content of the comment textarea field.
						 */
						echo apply_filters( 'comment_form_field_comment', $args['comment_field'] );
						?>
						<?php if ( is_user_logged_in() ) {?></div><?php }?>
						</div><?php //echo $args['comment_notes_after']; ?>
						<div class="comment-form-btm">
							<input class="btn btn-sm  btn-primary submit-button" name="<?php echo esc_attr( $args['name_submit'] ); ?>" type="submit" id="<?php echo esc_attr( $args['id_submit'] ); ?>" value="Add Comments" />
							<?php comment_id_fields( $post_id ); ?>
						</div>
						<?php
						/**
						 * Fires at the bottom of the comment form, inside the closing </form> tag.
						 *
						 * @since 1.5.0
						 *
						 * @param int $post_id The post ID.
						 */
						do_action( 'comment_form', $post_id );
						?>
					</form>
					</div>
				<?php endif; ?>
			</div><!-- #respond -->
			<?php
			/**
			 * Fires after the comment form.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_after' ); 
		else :
			/**
			 * Fires after the comment form if comments are closed.
			 *
			 * @since 3.0.0
			 */
			do_action( 'comment_form_comments_closed' ); 
		endif;
		
}


function comment_validation_init() {
    if(is_single() && comments_open() ) { ?>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
    $('#commentform').validate({

    rules: {
      author: {
        required: true,
        minlength: 2
      },

      email: {
        required: true,
        email: true
      },

      comment: {
        required: true,
        minlength: 20
      },
      captcha_code: {
          required: true,
        }
    },

    messages: {
      author: "Please fill the required field",
      email: "Please enter a valid email address.",
      comment: "Comments required minimum 20 characters.",
      captcha_code: "Please enter Captcha."
    },

    errorElement: "div",
    errorPlacement: function(error, element) {
      element.after(error);
    }

    });
    });
    </script>
    <?php
    }
    }
    add_action('wp_footer', 'comment_validation_init');
function proui_comments($comment, $args, $depth) {
 $GLOBALS['comment'] = $comment; ?>
<li class="commnt">
	<div class="commnt-img">
		<a href="#" class="pull-left">
	        <?php echo get_avatar($comment,$size='64',$default='' ); ?>
	    </a>
	</div>    
    <div class="commnt-body">
        <?php if ($comment->comment_approved == '0') : ?>
			<p><em><?php _e('Your comment is awaiting moderation.') ?></em></p>
		<?php endif; ?>
        <span class="posted-time"><small><em><?php echo human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ago'; ?></em></small></span>
        <span class="commnt-author-name"><strong><?php comment_author( $comment_ID ); ?></strong><?php edit_comment_link(__('Edit'),'&nbsp; ','') ?></span>
                <?php comment_text() ?>
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
</li>
<?php
 }
 

 
 
 
 if( function_exists('acf_add_options_page') ) {
   
   acf_add_options_page(array(
       'page_title'     => 'Theme General Settings',
       'menu_title'    => 'Theme Settings',
       'menu_slug'     => 'theme-general-settings',
       'capability'    => 'edit_posts',
       'redirect'        => false
   ));
   
   acf_add_options_sub_page(array(
       'page_title'     => 'Theme Header Settings',
       'menu_title'    => 'Header',
       'parent_slug'    => 'theme-general-settings',
   ));
   
   acf_add_options_sub_page(array(
       'page_title'     => 'Theme Footer Settings',
       'menu_title'    => 'Footer',
       'parent_slug'    => 'theme-general-settings',
   ));
}

add_action( 'after_setup_theme','woocommerce_support' );

function woocommerce_support() {

add_theme_support( 'woocommerce' );

} 


add_theme_support( 'wc-product-gallery-slider' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );          
add_filter( 'woocommerce_enqueue_styles', '__return_false' );   


//This function prints the JavaScript to the footer
function cf7_footer_script(){ ?>
  

  
<?php } 
  
add_action('wp_footer', 'cf7_footer_script'); 

 add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' ',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}
add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count',1);
add_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering',1);

add_filter( 'woocommerce_sale_flash', 'wc_custom_replace_sale_text' );
function wc_custom_replace_sale_text( $html ) {
    return str_replace( __( 'Sale!', 'woocommerce' ), __( 'Sale', 'woocommerce' ), $html );
}
add_action( 'woocommerce_before_add_to_cart_quantity', 'bbloomer_echo_qty_front_add_cart' );
 
function bbloomer_echo_qty_front_add_cart() {
 echo '<div class="qty">Quantity: </div>'; 
}

function woo_related_products_limit() {
  global $product;
	
	$args['posts_per_page'] = 5;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 5; // 4 related products
	$args['columns'] = 1; // arranged in 2 columns
	return $args;
}

/**
 * Change the default state and country on the checkout page
 */
add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );
add_filter( 'default_checkout_billing_state', 'change_default_checkout_state' );

function change_default_checkout_country() {
  return 'US'; // country code
}

function change_default_checkout_state() {
  return 'XX'; // state code
}


