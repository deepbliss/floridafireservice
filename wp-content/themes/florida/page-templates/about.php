

<?php
/**
* Template Name: About Page Template
* Template Name:  Page
*
**/
get_header(); ?>

<div class="cms black-bg common-div">
	<div class="container">
	<div class="breadcrumb">
        <ul>
               <li><a href="<?php echo site_url(); ?>">Home</a></li>
<?php if ( is_page() && $post->post_parent ){ ?>
<li><?php 
                          $top_page_url = get_permalink( end( get_ancestors( get_the_ID(), 'page' ) ) );
                          $parents = get_post_ancestors( $post->ID );
?> <a href="<?php echo $top_page_url; ?>"><?php echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); ?></a></li>
<?php } ?>
               <li class="current"><?php the_title(); ?></li>

           </ul>
 </div>	
	<div class="main-div">
	<div class="left-content">	
		<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} // end while
} // end if
?>			
</div>
<div class="right-content">
<div class="sidebar-contact">
<h2>Get In Touch</h2>
<?php echo do_shortcode('[contact-form-7 id="230" title="Sidebar Contact"]') ?>
</div>
</div>
</div>
	</div>
</div>

<?php get_footer('inner'); ?>

