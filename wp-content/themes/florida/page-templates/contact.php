

<?php
/**
* Template Name: Contact Page Template
* Template Name:  Page
*
**/
get_header(); ?>

<div class="cms black-bg common-div">
	<div class="container">	
		<div class="breadcrumb">
        <ul>
               <li><a href="<?php echo site_url(); ?>">Home</a></li>
<?php if ( is_page() && $post->post_parent ){ ?>
<li><?php 
                          $top_page_url = get_permalink( end( get_ancestors( get_the_ID(), 'page' ) ) );
                          $parents = get_post_ancestors( $post->ID );
?> <a href="<?php echo $top_page_url; ?>"><?php echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); ?></a></li>
<?php } ?>
               <li class="current"><?php the_title(); ?></li>

           </ul>
 </div>	
	<h1><?php the_title(); ?></h1>
		<div class="main-div">
		<p>If you'd like to share your needs with our staff, please fill out the information below and we'll be in touch within 24 hours!</p>
        <p>If you prefer to call, feel free to reach us at (877) 662-3473 today!</p>
			<div class="contact-left">
				<?php echo do_shortcode('[contact-form-7 id="265" title="Contact Page Form"]') ?>
			</div>
			<div class="contact-right">
				<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		the_content();
	} // end while
} // end if
?>			
			</div>
		</div>
	</div>
</div>

<?php get_footer('inner'); ?>

