<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="blog">
    <div class="container">
      <div class="breadcrumb">
        <ul>
               <li><a href="<?php echo site_url(); ?>">Home</a></li>
<?php global $wp_query;
if ( isset( $wp_query ) && (bool) $wp_query->is_posts_page ) {?>
<li>Blog</li>
<?php

}else if ( is_page() && $post->post_parent ){ ?>
<li><?php 
                          $top_page_url = get_permalink( end( get_ancestors( get_the_ID(), 'page' ) ) );
                          $parents = get_post_ancestors( $post->ID );
?> <a href="<?php echo $top_page_url; ?>"><?php echo apply_filters( "the_title", get_the_title( end ( $parents ) ) ); ?></a></li><li class="current"><?php the_title(); ?></li>
<?php } ?>
               

           </ul>
 </div>
        <div class="my-title"> <h1>Blog</h1></div>
      <div class="content-main" id="main">
        <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
        <article class="blog-main-sec post" >  
          <div class="blog_imgbox"><figure><a href="<?php echo get_permalink($recent["ID"]); ?>"> <?php $img = get_the_post_thumbnail($recent['ID'], 'full'); if($img != ''){echo $img;  } else{?> <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/blog1.png"> <?php } ?></a></figure></div>
          <div class="blog_description">
            <div class="blog-top">
              <span class="post-date"><?php the_time('d M, Y'); ?></span>
              <div class="blog_border">
                <span class="postedby"> <?php the_author_meta( 'display_name', 1 ); ?></span>
              </div>
              <div class="blog_border">
                <?php //the_tags( '<span class="tags">Tags: ' , ', ', '</span>' ); ?>
                <span class="num-of-commemts"><a href="<?php echo get_comments_link( $recent["ID"] );; ?>"> <?php echo get_comments_number( $recent['ID'] ); ?></a></span>
              </div>
            </div>
            <h4><a href="<?php the_permalink() ?>"><?php the_title( ); ?></a></h4>
            <div class="blog-content">  <?php echo wp_trim_words( get_the_content(), 25, '...' ); ?></div>
            <a href="<?php echo get_permalink($recent["ID"]); ?>" class="read_more">Read More</a>
          </div>
        </article>
        <?php endwhile; ?>
                       <?php //the_posts_pagination( array( 'mid_size' => 2 ) ); ?>
                <?php else :
                            //    get_template_part( 'content', 'none' );
            endif; ?>
      </div>
    </div>
  </div>


<?php get_footer();?>
