<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

     <section class="error-404 not-found">                 
                <header class="page-header">
                    <h1 class="page-title"><?php _e( '404 PAGE NOT FOUND', 'twentyseventeen' ); ?></h1>
                </header>
                <!-- .page-header -->
                <div class="page-content">
               

                </div><!-- .page-content -->
            </section><!-- .error-404 -->


<?php get_footer();
